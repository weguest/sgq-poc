**SGQ - Sistema de Gerenciamento da Qualidade.**

Softwares utilizados:
* Visual Studio 2017
* Net Core 2.2 instalado na maquina.
* Management Studio
* Docker
* SQLServer
* Redis
* RabbitMQ

Para rodar o sistema, primeiro é necessário rodar as dependencias. Utilizei o docker para criar instancias dos serviços necessários.

**RabbitMQ**

`docker run -d --hostname my-rabbit --name some-rabbit -p 15672:15672 -p 5672:5672  -e RABBITMQ_DEFAULT_USER=mqadmin -e RABBITMQ_DEFAULT_PASS=Admin123XX_ rabbitmq:3-management`


**Redis**

`docker run -d -p 6379:6379 -i -t redis:3.2.5-alpine`


**SQLServer**

`docker run -e "ACCEPT_EULA=Y" -e "SA_PASSWORD=mspassword@123" -p 1433:1433 --name sql -d microsoft/mssql-server-linux:2017-latest`

Usuario: **sa** e senha: **mspassword@123**

**SCRIPT**


Feito isso, rodar os scripts no servidor criado acima 
`src/DATABASE/SGQ-DATABASE.sql`
`src/DATABASE/SGQ-IDENTITYDB.sql`

**Executar as APIS**

Modificar o arquivo `src/appsettings.shared.json` com o IP, servidor e porta dos servidores criados acima.

Executar `dotnet run ` dentro de cada api listada:

*  src/SGQ.Incidentes.Presentation.API
*  src/SGQ.Processos.Presentation.API
*  src/SGQ.Transparencia.Presentation.API
*  src/Servico.Externo.Normas.API
*  src/SGQ.Presentation.Site
