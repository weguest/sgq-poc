﻿using System;
using System.Reflection;
using MediatR;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace SGQ.Crosscutting.MediatR
{
    public static class MediatorBootstrap
    {
        private static IConfiguration _configuration { get; set; }

        public static IServiceCollection RegisterMediatorServices(this IServiceCollection services, IConfiguration configuration, Assembly assembly)
        {

            services.AddMediatR(assembly);
            return services;
        }
    }
}
