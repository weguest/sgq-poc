﻿using System;
using System.Collections.Generic;
using System.Text;
using SGQ.Core.Domain.Event;

namespace SGQ.Incidentes.Domain.NaoConformidade.Events
{
    public class RespostaChecklistRespondidaEvent : Event
    {
        public string Organismo { get; set; }
        public string Norma { get; set; }
        public string Titulo { get; set; }
        public string Descricao { get; set; }

        public string Processo { get; set; }
        public string Grupo { get; set; }
        public string Turno { get; set; }
        public string Pergunta { get; set; }
        public string Resposta_Esperada { get; set; }
        public string Resposta { get; set; }

        public RespostaChecklistRespondidaEvent(
            string codigo,
            string modificadoPor,
            string organismo,
            string norma,
            string titulo,
            string descricao,
            string processo,
            string grupo,
            string turno,
            string pergunta,
            string resposta,
            string respostaEsperada
        )
        {
            Codigo = codigo;
            ModificadoPor = modificadoPor;
            Organismo = organismo;
            Norma = norma;
            Titulo = titulo;
            Descricao = descricao;
            Processo = processo;
            Grupo = grupo;
            Turno = turno;
            Pergunta = pergunta;
            Resposta = resposta;
            Resposta_Esperada = respostaEsperada;
        }
    }
}
