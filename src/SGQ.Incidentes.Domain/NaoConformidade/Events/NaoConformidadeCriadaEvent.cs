﻿using SGQ.Core.Domain.Event;

namespace SGQ.Incidentes.Domain.NaoConformidade.Events
{
    public class NaoConformidadeCriadaEvent : Event
    {
        public string Titulo { get; set; }
        public string Descricao { get; set; }
        public string Criadopor { get; set; }
        public string Norma { get; set; }
        public string Processo { get; set; }

        public NaoConformidadeCriadaEvent()
        {
            
        }

        public NaoConformidadeCriadaEvent( string codigo, string norma, string processo, string titulo, string descricao, string criadopor)
        {
            Norma = norma;
            Processo = processo;
            Titulo = titulo;
            Descricao = descricao;
            Criadopor = criadopor;
        }
    }
}
