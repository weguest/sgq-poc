﻿using SGQ.Core.Domain.Entity.Enum;
using SGQ.Core.Domain.Query;

namespace SGQ.Incidentes.Domain.NaoConformidade.Query
{
    public class NaoConformidadeListarQueryFilter : IQueryFilter<NaoConformidadeListarQueryDTO>
    {
        public string Codigo { get; set; }
        public string Titulo { get; set; }
        public string Descricao { get; set; }
        public string Norma { get; set; }
        public string Processo { get; set; }
        public EnumEstadoRegistro? EstadoRegistro { get; set; } = EnumEstadoRegistro.Ativo;

    }
}
