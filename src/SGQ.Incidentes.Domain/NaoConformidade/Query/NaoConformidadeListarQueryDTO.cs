﻿using System;
using SGQ.Core.Domain.Query;

namespace SGQ.Incidentes.Domain.NaoConformidade.Query
{
    public class NaoConformidadeListarQueryDTO : IQueryDTO
    {
        public string codigo { get; set; }

        public string titulo { get; set; }

        public string descricao { get; set; }

        public string norma { get; set; }

        public string processo { get; set; }

        public string Status { get; set; }

        public DateTime? FechadoEm { get; set; }

        public string FechadoPor { get; set; }

        public string AcoesCorretivas { get; set; }

        public string AcoesPreventivas { get; set; }

    }
}
