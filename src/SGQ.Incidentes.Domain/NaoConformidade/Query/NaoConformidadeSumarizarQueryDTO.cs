﻿using System;
using System.Collections.Generic;
using System.Text;
using SGQ.Core.Domain.Query;

namespace SGQ.Incidentes.Domain.NaoConformidade.Query
{
    public class NaoConformidadeSumarizarQueryDTO : IQueryDTO
    {
        public string status { get; set; }
        public int count { get; set; }
    }
}
