﻿using System;
using SGQ.Core.Domain.Entity;
using SGQ.Core.Domain.Entity.Enum;

namespace SGQ.Incidentes.Domain.NaoConformidade.Entity
{
    public class NaoConformidadeEntity : EntityBase
    {
        public string Titulo { get; set; }
        public string Descricao { get; set; }
        public string Norma { get; set; }
        public string Processo { get; set; }
        public string Status { get; set; } = "Aberto";
        public DateTime? FechadoEm { get; set; }
        public string FechadoPor { get; set; }
        public string AcoesCorretivas { get; set; }
        public string AcoesPreventivas { get; set; }


        public NaoConformidadeEntity()
        {
            
        }

        public NaoConformidadeEntity(string titulo, string descricao, string norma, string processo, string criadoPor) : base(criadoPor)
        {
            Codigo = GerarNumeroRandomico("NCD", 12);
            Titulo = titulo;
            Descricao = descricao;
            Norma = norma;
            Processo = processo;
            CriadoPor = criadoPor;

            if (string.IsNullOrWhiteSpace(Codigo))
                this.Erros.Add(new Erro("Codigo", "Você deve informar o Código."));

            if (string.IsNullOrWhiteSpace(titulo))
                this.Erros.Add(new Erro("titulo", "Você deve informar o titulo."));

            if (string.IsNullOrWhiteSpace(descricao))
                this.Erros.Add(new Erro("descricao", "Você deve informar uma descrição."));

            if (string.IsNullOrWhiteSpace(norma))
                this.Erros.Add(new Erro("norma", "Você deve informar uma norma."));

            if (string.IsNullOrWhiteSpace(processo))
                this.Erros.Add(new Erro("processo", "Você deve informar um processo."));

        }

        public void Alterar(string codigo, string titulo, string descricao, string norma, string processo, string alteradopor, EnumEstadoRegistro estado)
        {
            base.Alterar(codigo, alteradopor, estado);

            Titulo = titulo;
            Descricao = descricao;
            Norma = norma;
            Processo = processo;

            if (Status == "Fechado")
                this.Erros.Add(new Erro("status", "A Não Conformidade já foi fechada."));

            if (string.IsNullOrWhiteSpace(codigo))
                this.Erros.Add(new Erro("Codigo", "Você deve informar o Código."));

            if (string.IsNullOrWhiteSpace(titulo))
                this.Erros.Add(new Erro("titulo", "Você deve informar o titulo."));

            if (string.IsNullOrWhiteSpace(descricao))
                this.Erros.Add(new Erro("descricao", "Você deve informar uma descrição."));

            if (string.IsNullOrWhiteSpace(norma))
                this.Erros.Add(new Erro("norma", "Você deve informar uma norma."));

            if (string.IsNullOrWhiteSpace(processo))
                this.Erros.Add(new Erro("processo", "Você deve informar um processo."));

        }

        public void Fechar(string fechadopor, string acoesCorretivas, string acoesPreventivas)
        {

            if (Status == "Fechado")
                this.Erros.Add(new Erro("status", "A Não Conformidade já foi fechada."));

            Status = "Fechado";
            FechadoPor = fechadopor;
            FechadoEm = DateTime.UtcNow;
            AcoesCorretivas = acoesCorretivas;
            AcoesPreventivas = acoesPreventivas;

            if (string.IsNullOrWhiteSpace(fechadopor))
                this.Erros.Add(new Erro("fechadopor", "Você deve informar o campo fechadopor."));

            if (string.IsNullOrWhiteSpace(acoesCorretivas))
                this.Erros.Add(new Erro("acoesCorretivas", "Você deve informar o  campo Acoes Corretivas."));

            if (string.IsNullOrWhiteSpace(acoesPreventivas))
                this.Erros.Add(new Erro("acoesPreventivas", "Você deve informar o campo Acoes Preventivas."));

        }

    }
}
