﻿using MediatR;
using SGQ.Core.Domain.Command;
using SGQ.Incidentes.Domain.NaoConformidade.Entity;

namespace SGQ.Incidentes.Domain.NaoConformidade.Command
{
    public class NaoConformidadeCriarDeRespostaCommand : IRequest<CommandResult<NaoConformidadeEntity>>
    {
        public string Codigo { get; set; }
        public string ModificadoPor { get; set; }
        public string Organismo { get; set; }
        public string Norma { get; set; }
        public string Titulo { get; set; }
        public string Descricao { get; set; }
        public string Processo { get; set; }
        public string Grupo { get; set; }
        public string Turno { get; set; }
        public string Pergunta { get; set; }
        public string Resposta_Esperada { get; set; }
        public string Resposta { get; set; }
    }
}
