﻿using MediatR;
using SGQ.Core.Domain.Command;
using SGQ.Core.Domain.Entity.Enum;
using SGQ.Incidentes.Domain.NaoConformidade.Entity;

namespace SGQ.Incidentes.Domain.NaoConformidade.Command
{
    public class NaoConformidadeAtualizarCommand : IRequest<CommandResult<NaoConformidadeEntity>>
    {
        public string Codigo { get; set; }

        public EnumEstadoRegistro EstadoRegistro { get; set; }

        public string ModificadoPor { get; set; }

        public string Titulo { get; set; }

        public string Descricao { get; set; }

        public string Norma { get; set; }

        public string Processo { get; set; }
    }
}