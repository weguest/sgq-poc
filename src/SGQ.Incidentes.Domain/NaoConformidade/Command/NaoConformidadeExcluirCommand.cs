﻿using MediatR;
using SGQ.Core.Domain.Command;

namespace SGQ.Incidentes.Domain.NC.Command
{
    public class NaoConformidadeExcluirCommand : IRequest<CommandResult<bool>>
    {
        public string Codigo { get; set; }
    }
}