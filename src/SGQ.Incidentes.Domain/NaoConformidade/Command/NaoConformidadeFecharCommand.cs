﻿using System;
using System.Collections.Generic;
using System.Text;
using MediatR;
using SGQ.Core.Domain.Command;
using SGQ.Core.Domain.Entity.Enum;
using SGQ.Incidentes.Domain.NaoConformidade.Entity;

namespace SGQ.Incidentes.Domain.NaoConformidade.Command
{
    public class NaoConformidadeFecharCommand : IRequest<CommandResult<NaoConformidadeEntity>>
    {
        public string Codigo { get; set; }
        public string ModificadoPor { get; set; }
        public string AcoesPreventivas { get; set; }
        public string AcoesCorretivas { get; set; }
    }
}
