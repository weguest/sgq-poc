﻿using System;
using System.Collections.Generic;
using System.Text;
using MediatR;
using SGQ.Core.Domain.Command;
using SGQ.Incidentes.Domain.NaoConformidade.Entity;

namespace SGQ.Incidentes.Domain.NC.Command
{
    public class NaoConformidadeCriarCommand : IRequest<CommandResult<NaoConformidadeEntity>>
    {
        public string CriadoPor { get; set; }

        public string Titulo { get; set; }

        public string Descricao { get; set; }

        public string Norma { get; set; }

        public string Processo { get; set; }

    }
}
