﻿using System;
using System.Collections.Generic;
using System.Text;
using SGQ.Core.Domain.Query;

namespace SGQ.Incidentes.Domain.Incidente.Query
{
    public class IncidenteListarBaseQueryDTO : IQueryDTO
    {
        public IEnumerable<IncidenteListarQueryDTO> Lista { get; set; }
        public IEnumerable<IncidenteSumarizarQueryDTO> Sumarizado { get; set; }
    }
}
