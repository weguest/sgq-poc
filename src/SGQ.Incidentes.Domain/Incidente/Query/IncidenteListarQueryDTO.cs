﻿using System;
using System.Collections.Generic;
using System.Text;
using SGQ.Core.Domain.Entity.Enum;
using SGQ.Core.Domain.Query;

namespace SGQ.Incidentes.Domain.Incidente.Query
{
    public class IncidenteListarQueryDTO : IQueryDTO
    {
        public string codigo { get;set; }
        public string titulo { get;set; }
        public string descricao { get; set; }
        public string status { get; set; }
        public string solucao { get;set; }
        public EnumEstadoRegistro EstadoRegistro { get;set; }
        public string criadoEm { get;set; }
        public string modificadoEm { get;set; }
        public string criadoPor { get;set; }
        public string criadoPor_Nome { get;set; }
        public string modificadoPor { get;set; }
        public string modificadoPor_Nome { get;set; }
    }
}
