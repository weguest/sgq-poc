﻿using SGQ.Core.Domain.Event;

namespace SGQ.Incidentes.Domain.Incidente.Events
{
    public class IncidenteCriadoEvent : Event
    {
        public string Titulo { get; set; }
        public string Descricao { get; set; }

        public IncidenteCriadoEvent( string codigo, string titulo, string descricao, string criadoPor)
        {
            Codigo = codigo;
            Titulo = titulo;
            Descricao = descricao;
            CriadoPor = criadoPor;
        }
    }
}