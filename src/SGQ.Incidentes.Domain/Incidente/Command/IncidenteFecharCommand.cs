﻿using System;
using System.Collections.Generic;
using System.Text;
using MediatR;
using SGQ.Core.Domain.Command;

namespace SGQ.Incidentes.Domain.Incidente.Command
{
    public class IncidenteFecharCommand : IRequest<CommandResult<bool>>
    {
        public string Codigo { get; set; }
        public string ModificadoPor { get; set; }
        public string Solucao { get; set; }
    }
}
