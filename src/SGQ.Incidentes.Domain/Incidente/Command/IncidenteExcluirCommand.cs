﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using SGQ.Core.Domain.Command;
using SGQ.Core.Domain.Entity.Enum;

namespace SGQ.Incidentes.Domain.Incidente.Command
{
    public class IncidenteExcluirCommand : IRequest<CommandResult<bool>>
    {
        public string Codigo { get; set; }
        public EnumEstadoRegistro EstadoRegistro { get; set; } = EnumEstadoRegistro.Excluido;
        public string ModificadoPor { get; set; }
    }
}
