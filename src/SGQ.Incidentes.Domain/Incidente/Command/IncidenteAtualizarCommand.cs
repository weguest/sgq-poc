﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SGQ.Core.Domain.Command;
using SGQ.Core.Domain.Entity.Enum;
using SGQ.Incidentes.Domain.Incidente.Entity;

namespace SGQ.Incidentes.Domain.Incidente.Command
{
    public class IncidenteAtualizarCommand : IRequest<CommandResult<IncidenteEntity>>
    {
        public string Codigo { get; set; }
        public EnumEstadoRegistro EstadoRegistro { get; set; }
        public DateTime ModificadoEm { get; set; } = DateTime.Now;
        public string ModificadoPor { get; set; }
        public string Titulo { get; set; }
        public string Descricao { get; set; }
    }
}
