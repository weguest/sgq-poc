﻿using MediatR;
using System;
using SGQ.Core.Domain.Command;
using SGQ.Incidentes.Domain.Incidente.Entity;

namespace SGQ.Incidentes.Domain.Incidente.Command
{
    public class IncidenteCriarCommand : IRequest<CommandResult<IncidenteEntity>>
    {
        public string CriadoPor { get; set; }
        public string Titulo { get; set; }
        public string Descricao { get; set; }
        public string Status { get; set; }
    }
}
