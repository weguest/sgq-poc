﻿using System;
using SGQ.Core.Domain.Entity;
using SGQ.Core.Domain.Entity.Enum;

namespace SGQ.Incidentes.Domain.Incidente.Entity
{
    public class IncidenteEntity : EntityBase
    {
        public string Titulo { get; set; }
        public string Descricao { get; set; }
        public string Status { get; set; } = "Aberto";
        public string Solucao { get; set; }

        public override string ToString()
        {
            return $"{Titulo} - {Descricao} ";
        }

        public IncidenteEntity() : base()
        {

        }

        public IncidenteEntity(string usuarioCriacao, string titulo, string descricao ) : base(usuarioCriacao)
        {
            Codigo = this.GerarNumeroRandomico("INC", 12);
            Titulo = titulo;
            Descricao = descricao;

            if (string.IsNullOrWhiteSpace(Codigo))
                this.Erros.Add(new Erro("Codigo", "Você deve informar o Código."));

            if (string.IsNullOrWhiteSpace(titulo))
                this.Erros.Add(new Erro("titulo", "Você deve informar o titulo."));

            if (string.IsNullOrWhiteSpace(descricao))
                this.Erros.Add(new Erro("descricao", "Você deve informar uma descrição."));
        }

        public void Alterar(string codigo, string usuarioAlteracao, EnumEstadoRegistro estado, string titulo, string descricao)
        {
           base.Alterar(codigo, usuarioAlteracao, estado);
           Titulo = titulo;
           Descricao = descricao;

           if (Status == "Fechado")
               Erros.Add(new Erro("status", "Este incidente já esta fechado."));

            if (string.IsNullOrWhiteSpace(Codigo))
                this.Erros.Add(new Erro("Codigo", "Você deve informar o Código."));

            if (string.IsNullOrWhiteSpace(titulo))
                this.Erros.Add(new Erro("titulo", "Você deve informar o titulo."));

            if (string.IsNullOrWhiteSpace(descricao))
                this.Erros.Add(new Erro("descricao", "Você deve informar uma descrição."));

        }

        public void Fechar(string solucao)
        {

            if (Status == "Fechado")
                Erros.Add(new Erro("status", "Este incidente já esta fechado."));

            if (string.IsNullOrWhiteSpace(solucao))
                this.Erros.Add(new Erro("solucao", "Você deve informar uma solucao."));

            Solucao = solucao;
            Status = "Fechado";
        }
    }
}
