﻿using SGQ.Core.Domain.Query;

namespace SGQ.Incidentes.Domain.Problema.Query
{
    public class ProblemaSumarizarQueryDTO : IQueryDTO
    {
        public string status { get; set; }
        public int count { get; set; }
    }
}