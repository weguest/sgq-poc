﻿using System;
using System.Collections.Generic;
using System.Text;
using SGQ.Core.Domain.Entity.Enum;
using SGQ.Core.Domain.Query;

namespace SGQ.Incidentes.Domain.Problema.Query
{
    public class ProblemaListarQueryFilter : IQueryFilter<ProblemaListarQueryDTO>
    {
        public string codigo { get; set; }
        public string titulo { get; set; }
        public string descricao { get; set; }
        public EnumEstadoRegistro? EstadoRegistro { get; set; } = EnumEstadoRegistro.Ativo;
    }
}
