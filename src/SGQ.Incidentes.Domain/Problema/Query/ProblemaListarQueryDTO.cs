﻿using System;
using System.Collections.Generic;
using System.Text;
using SGQ.Core.Domain.Query;

namespace SGQ.Incidentes.Domain.Problema.Query
{
    public class ProblemaListarQueryDTO : IQueryDTO
    {
        public string codigo { get; set; }
        public string titulo { get; set; }
        public string descricao { get; set; }
        public string estadoRegistro { get; set; }
        public string status { get; set; }
        public string solucao { get; set; }
    }
}
