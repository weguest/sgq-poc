﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SGQ.Incidentes.Domain.Problema.Events
{
    public class ProblemaCriadoEvent
    {
        public string Codigo { get; set; }
        public string Titulo { get; set; }
        public string Descricao { get; set; }

        public ProblemaCriadoEvent(string codigo, string titulo, string descricao)
        {
            Codigo = codigo;
            Titulo = titulo;
            Descricao = descricao;
        }
    }
}
