﻿using System;
using System.Collections.Generic;
using System.Text;
using MediatR;
using SGQ.Core.Domain.Command;
using SGQ.Core.Domain.Entity.Enum;

namespace SGQ.Incidentes.Domain.Problema.Command
{
    public class ProblemaAtualizarCommand : IRequest<CommandResult<Entity.ProblemaEntity>>
    {
        public string Codigo { get; set; }
        public EnumEstadoRegistro EstadoRegistro { get; set; }
        public DateTime ModificadoEm { get; set; } = DateTime.Now;
        public string ModificadoPor { get; set; }
        public string Titulo { get; set; }
        public string Descricao { get; set; }
        public List<string> Incidentes { get; set; } = new List<string>();
    }
}
