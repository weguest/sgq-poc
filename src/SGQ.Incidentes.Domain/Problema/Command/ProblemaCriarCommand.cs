﻿using System;
using System.Collections.Generic;
using System.Text;
using MediatR;
using SGQ.Core.Domain.Command;

namespace SGQ.Incidentes.Domain.Problema.Command
{
    public class ProblemaCriarCommand : IRequest<CommandResult<Entity.ProblemaEntity>>
    {
        public string CriadoPor { get; set; }
        public string Titulo { get; set; }
        public string Descricao { get; set; }
        public List<string> Incidentes { get; set; } = new List<string>();
    }
}
