﻿using SGQ.Core.Domain.Entity;
using SGQ.Core.Domain.Entity.Enum;
using System.Collections.Generic;

namespace SGQ.Incidentes.Domain.Problema.Entity
{
    public class ProblemaEntity : EntityBase
    {
        public string Titulo { get; set; }
        public string Descricao { get; set; }
        public string Status { get; set; } = "Aberto";
        public string Solucao { get; set; }
        public List<Incidente.Entity.IncidenteEntity> Incidentes { get; private set; } = new List<Incidente.Entity.IncidenteEntity>();


        public ProblemaEntity() { }

        public ProblemaEntity(string titulo, string descricao, List<Incidente.Entity.IncidenteEntity> incidentes, string criadoPor) : base(criadoPor)
        {
            Codigo = this.GerarNumeroRandomico("PBL", 12);
            Titulo = titulo;
            Descricao = descricao;
            Incidentes = incidentes;

            if (string.IsNullOrWhiteSpace(Codigo))
                this.Erros.Add(new Erro("Codigo", "Você deve informar o Código."));

            if (string.IsNullOrWhiteSpace(titulo))
                this.Erros.Add(new Erro("titulo", "Você deve informar o titulo."));

            if (string.IsNullOrWhiteSpace(descricao))
                this.Erros.Add(new Erro("descricao", "Você deve informar uma descrição."));

        }
        public void Alterar(string codigo, string usuarioAlteracao, EnumEstadoRegistro estado, string titulo, string descricao, List<Incidente.Entity.IncidenteEntity> incidentes)
        {
            base.Alterar(codigo, usuarioAlteracao, estado);

            Titulo = titulo;
            Descricao = descricao;

            if (Status == "Resolvido")
                Erros.Add(new Erro("status", "Este problema já foi resolvido."));

            if (string.IsNullOrWhiteSpace(Codigo))
                this.Erros.Add(new Erro("Codigo", "Você deve informar o Código."));

            if (string.IsNullOrWhiteSpace(titulo))
                this.Erros.Add(new Erro("titulo", "Você deve informar o titulo."));

            if (string.IsNullOrWhiteSpace(descricao))
                this.Erros.Add(new Erro("descricao", "Você deve informar uma descrição."));

            if (Incidentes is null)
                this.Erros.Add(new Erro("Incidentes", "Lista de Incidentes vazia."));

        }
        public void Excluir(string modificadoPor)
        {
            if (string.IsNullOrWhiteSpace(modificadoPor))
                this.Erros.Add(new Erro("modificadoPor", "Você deve informar o usuário de modificação."));

            EstadoRegistro = EnumEstadoRegistro.Excluido.GetHashCode();

        }
        public void Resolver(string solucao)
        {
            if (Status == "Resolvido")
                Erros.Add(new Erro("status", "Este problema já foi resolvido."));
           
            if (string.IsNullOrWhiteSpace(solucao))
                this.Erros.Add(new Erro("solucao", "Você deve informar uma solucao."));

            Solucao = solucao;
            Status = "Resolvido";
        }
    }
}
