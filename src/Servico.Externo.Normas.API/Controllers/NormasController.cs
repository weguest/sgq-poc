﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Servico.Externo.Normas.API.Models;

namespace Servico.Externo.Normas.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class NormasController : ControllerBase
    {
        private List<NormaViewModel> _lista = new List<NormaViewModel>();

        public NormasController()
        {
            _lista.Add(new NormaViewModel() { codigo = "14842",  arquivo = "NBR 14842.pdf", nome = "Critérios para a qualificação e certificação de inspetores de soldagem", descricao = "Esta Norma estabelece os critérios e a sistemática para a qualificação e certificação de inspetores de soldagem, e descreve as atribuições e responsabilidades para os níveis de qualificação estabelecidos.", organismo = "ABNT" });
            _lista.Add(new NormaViewModel() { codigo = "1609.2", arquivo = "IEEE_Std_1609_2a-2017.pdf", nome = "IEEE Standard for Wireless Access in Vehicular Environments— Security Services for Applications and Management Messages", descricao = "Secure message formats and processing for use by Wireless Access in Vehicular Environments(WAVE) devices, including methods to secure WAVE management messages and methods to secure application messages are defined.Administrative functions necessary to support the core security functions are described", organismo = "IEEE" });
            _lista.Add(new NormaViewModel() { codigo = "0003",   arquivo = "file3.pdf", nome = "Norma xpto", descricao = "Descrição xpto", organismo = "ASTM" });

        }

        // GET api/normas
        [HttpGet]
        public ActionResult<IEnumerable<NormaViewModel>> Get([FromQuery] string nome, string descricao, string organismo)
        {
            var items = _lista.Where(x =>
                (nome is null || x.nome.Contains(nome, StringComparison.CurrentCultureIgnoreCase)) &&
                (descricao is null || x.descricao.Contains(descricao, StringComparison.CurrentCultureIgnoreCase)) &&
                (organismo is null || x.organismo.Contains(organismo, StringComparison.CurrentCultureIgnoreCase))
            ).ToList();

            if (items.Count == 0)
                return NotFound();
            
           
            return items;
        }

        // GET api/normas/5
        [HttpGet("{codigo}")]
        public ActionResult<NormaViewModel> Get(string codigo)
        {

            var norma = _lista.FirstOrDefault(x => x.codigo == codigo);
            if (norma is null)
                return NotFound();
            else
                return Ok(norma);
        }

    }
}
