﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Servico.Externo.Normas.API.Models
{
    public class NormaViewModel
    {
        public string codigo { get; set; }
        public string nome { get; set; }
        public string descricao { get; set; }
        public string organismo { get; set; }
        public string arquivo { get; set; }
    }
}
