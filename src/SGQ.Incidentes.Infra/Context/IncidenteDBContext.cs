﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using SGQ.Incidentes.Domain.Incidente.Entity;

namespace SGQ.Incidentes.Infra.Context
{
    public class IncidenteDBContext : DbContext
    {
        public IncidenteDBContext(DbContextOptions<IncidenteDBContext> options) : base(options)
        {
            Database.EnsureCreated();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
        }
        
        //entities
        public DbSet<IncidenteEntity> Incidentes { get; set; }
    }
}
