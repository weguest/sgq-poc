﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using SGQ.Core.Infra.Repositories;
using SGQ.Incidentes.Domain.Incidente.Entity;
using SGQ.Incidentes.Domain.Incidente.Query;
using SGQ.Incidentes.Infra.Context;

namespace SGQ.Incidentes.Infra.Repositories
{
    public interface IIncidentesRepository: IBaseRepository<IncidenteEntity>
    {
        Task<IEnumerable<IncidenteListarQueryDTO>> Listar(IncidentesListarQueryFilter filtro);
        Task<IEnumerable<IncidenteSumarizarQueryDTO>> SumarizarIncidentes();

    }
}
