﻿using System.Collections.Generic;
using System.Threading.Tasks;
using SGQ.Core.Infra.Repositories;
using SGQ.Incidentes.Domain.Problema.Entity;
using SGQ.Incidentes.Domain.Problema.Query;

namespace SGQ.Incidentes.Infra.Repositories
{
    public interface IProblemaRepository : IBaseRepository<ProblemaEntity>
    {
        Task<IEnumerable<ProblemaListarQueryDTO>> Listar(ProblemaListarQueryFilter filtro);
        Task<IEnumerable<ProblemaSumarizarQueryDTO>> SumarizarProblemas();
    }
}