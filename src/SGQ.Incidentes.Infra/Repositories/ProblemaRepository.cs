﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Configuration;
using SGQ.Core.Infra.Repositories;
using SGQ.Incidentes.Domain.Incidente.Entity;
using SGQ.Incidentes.Domain.Problema.Entity;
using SGQ.Incidentes.Domain.Problema.Query;

namespace SGQ.Incidentes.Infra.Repositories
{
    public class ProblemaRepository : IProblemaRepository, IBaseRepository<ProblemaEntity>
    {

        private readonly IUnitOfWork _unitOfWork;
        private readonly IIncidentesRepository _incidentesRepository;
        private readonly CacheRepository<ProblemaEntity> _cache;

        public ProblemaRepository(IUnitOfWork unitOfWork, IIncidentesRepository incidentesRepository, IDistributedCache distributedCache)
        {
            _unitOfWork = unitOfWork;
            _incidentesRepository = incidentesRepository;
            _cache = new CacheRepository<ProblemaEntity>(distributedCache);
        }

        public async Task<ProblemaEntity> ObterPorCodigo(string codigo)
        {

            var cached = await _cache.ObterPorCodigo(codigo);
            if (cached != null)
                return cached;

            var problemaQuery = await _unitOfWork.Connection.QueryAsync<ProblemaEntity>($@"
            SELECT 
                 [Codigo]
                ,[CriadoPor]
                ,[CriadoEm]
                ,[modificadoPor]
                ,[modificadoEm]
                ,[EstadoRegistro]
                ,[Titulo]
                ,[Descricao]
                ,[Status]
                ,[Solucao]
            FROM Problemas
            WHERE 
            [Codigo] = @Codigo", new { Codigo = codigo }, _unitOfWork.Transaction);

            var problema = problemaQuery.SingleOrDefault();

            var IncidentesProblemas = await _unitOfWork.Connection.QueryAsync(
                $"SELECT [Incidente] ,[Problema] FROM [dbo].[IncidenteProblema] WHERE Problema = @Codigo", new { Codigo = codigo},
                _unitOfWork.Transaction);

            foreach (var incidentesProblema in IncidentesProblemas)
            {
                var incidente = await _incidentesRepository.ObterPorCodigo(incidentesProblema.Incidente);
                problema.Incidentes.Add(incidente);
            }

            await _cache.AdicionarAsync(problema);

            return problema;
        }

        public async Task<int> AdicionarAsync(ProblemaEntity entity)
        {
                var queryResult = await _unitOfWork.Connection.ExecuteAsync($@"
                INSERT INTO Problemas ( [Codigo], [CriadoPor], [CriadoEm], [modificadoPor], [modificadoEm], [EstadoRegistro], [Titulo], [Descricao], [Status], [Solucao] )	            VALUES ( @Codigo, @CriadoPor, @CriadoEm, @modificadoPor, @modificadoEm, @EstadoRegistro, @Titulo, @Descricao, @Status, @Solucao )",
                entity,
                _unitOfWork.Transaction,
                10 * 60,
                CommandType.Text);


                foreach (var item in entity.Incidentes)
                {
                    var queryResultInsertIncidente = await _unitOfWork.Connection.ExecuteAsync(
                        $@"
                        INSERT INTO IncidenteProblema (	                        [Incidente],	                        [Problema]                        )                        VALUES (	                        @Incidente,	                        @Problema                        )", new {Incidente = item.Codigo, Problema = entity.Codigo},
                        _unitOfWork.Transaction);
                }

            await _cache.AdicionarAsync(entity);

            return queryResult;
        }

        public async Task<int> AtualizarAsync(ProblemaEntity entity)
        {

            var queryResult = await _unitOfWork.Connection.ExecuteAsync($@"
                UPDATE Problemas SET 	                [CriadoPor] = @CriadoPor,	                [CriadoEm] = @CriadoEm,	                [modificadoPor] = @modificadoPor,	                [modificadoEm] = @modificadoEm,	                [EstadoRegistro] = @EstadoRegistro,	                [Titulo] = @Titulo,	                [Descricao] = @Descricao,	                [Status] = @Status,	                [Solucao] = @Solucao                WHERE [Codigo] = @Codigo",
                entity,
                _unitOfWork.Transaction,
                10 * 60,
                CommandType.Text);

            var queryResultDelete = await _unitOfWork.Connection.ExecuteAsync(
                $@"DELETE FROM [dbo].[IncidenteProblema] WHERE Problema = @codigo", new { codigo = entity.Codigo },
                _unitOfWork.Transaction);

            foreach (var item in entity.Incidentes)
            {
                var queryResultInsertIncidente = await _unitOfWork.Connection.ExecuteAsync(
                    $@"
                        INSERT INTO IncidenteProblema (	                        [Incidente],	                        [Problema]                        )                        VALUES (	                        @Incidente,	                        @Problema                        )", new { Incidente = item.Codigo, Problema = entity.Codigo },
                    _unitOfWork.Transaction);
            }

            await _cache.AtualizarAsync(entity);

            return queryResult;

        }

        public async Task<int> ExcluirAsync(string codigo)
        {
            var queryResultDelete = await _unitOfWork.Connection.ExecuteAsync(
                $@"DELETE FROM [IncidenteProblema] WHERE [Problema] = @codigo", new { codigo = codigo },
                _unitOfWork.Transaction);

            queryResultDelete = await _unitOfWork.Connection.ExecuteAsync(
                $@"DELETE FROM Problemas WHERE codigo = @codigo", new { codigo = codigo },
                _unitOfWork.Transaction);

            await _cache.ExcluirAsync(codigo);

            return queryResultDelete;
        }

        public async Task<IEnumerable<ProblemaListarQueryDTO>> Listar(ProblemaListarQueryFilter filtro)
        {

            var sqlQuery = $@"SELECT 
             [Codigo]
            ,[CriadoPor]
            ,[CriadoEm]
            ,[modificadoPor]
            ,[modificadoEm]
            ,[EstadoRegistro]
            ,[Titulo]
            ,[Descricao]
            ,[Status]
            ,[Solucao]
            FROM Problemas
            WHERE
                1 = 1";

            if (!string.IsNullOrWhiteSpace(filtro.codigo))
                sqlQuery += "AND (codigo = @codigo)";

            if (!string.IsNullOrWhiteSpace(filtro.titulo))
                sqlQuery += "AND (titulo LIKE '%' + @titulo + '%')";

            if (!string.IsNullOrWhiteSpace(filtro.descricao))
                sqlQuery += "AND (descricao LIKE '%' + @descricao + '%')";

            if (filtro.EstadoRegistro != null)
                sqlQuery += "AND (EstadoRegistro = @EstadoRegistro)";

            var problemaQuery = await _unitOfWork.Connection.QueryAsync<ProblemaListarQueryDTO>(sqlQuery, filtro, _unitOfWork.Transaction);

            //foreach (var item in problemaQuery)
            //{
            //    item.incidentes =
            //        await _unitOfWork.Connection.QueryAsync<string>(@"
            //        SELECT [Codigo]
            //    FROM[dbo].[Incidentes] I
            //        INNER JOIN[dbo].[IncidenteProblema] IPR ON IPR.Incidente = I.Codigo
            //    WHERE
            //        IPR.Problema = @Codigo", new { Codigo = item.codigo }, _unitOfWork.Transaction);
            //}

            return problemaQuery;
        }

        public async Task<IEnumerable<ProblemaSumarizarQueryDTO>> SumarizarProblemas()
        {
            var problemaQuery =
            await _unitOfWork.Connection.QueryAsync<ProblemaSumarizarQueryDTO>(@"
            SELECT 'Aberto' as status, count(*) as count FROM( SELECT CODIGO FROM Problemas WHERE [status] = 'Aberto') AS A
            UNION ALL
            SELECT 'Resolvido' as status, count(*) as count FROM( SELECT CODIGO FROM Problemas WHERE [status] = 'Resolvido') AS F
            ", null, _unitOfWork.Transaction);
        

            return problemaQuery;
        }
    }
}
