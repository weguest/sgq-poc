﻿using Dapper;
using Microsoft.Extensions.Configuration;
using SGQ.Core.Infra.Repositories;
using SGQ.Incidentes.Domain.NaoConformidade.Entity;
using SGQ.Incidentes.Domain.NaoConformidade.Query;
using SGQ.Incidentes.Domain.Problema.Query;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Caching.Distributed;

namespace SGQ.Incidentes.Infra.Repositories
{
    public class NaoConformidadeRepository : INaoConformidadeRepository
    {
        private readonly IConfiguration _configuration;
        private readonly IUnitOfWork _unitOfWork;
        private readonly CacheRepository<NaoConformidadeEntity> _cache;

        #region sql
        private static readonly string sqlObterPorCodigo = $@"SELECT [Codigo]
      ,[CriadoPor]
      ,[CriadoEm]
      ,[modificadoPor]
      ,[modificadoEm]
      ,[EstadoRegistro]
      ,[Titulo]
      ,[Descricao]
      ,[Norma]
      ,[Processo]
      ,[Status]
      ,[FechadoEm]
      ,[FechadoPor]
      ,[AcoesCorretivas]
      ,[AcoesPreventivas]        FROM Ncs
        WHERE Codigo = @Codigo";

        private static readonly string sqlAdicionar = $@"INSERT INTO Ncs (             [Codigo]
            ,[CriadoPor]
            ,[CriadoEm]
            ,[modificadoPor]
            ,[modificadoEm]
            ,[EstadoRegistro]
            ,[Titulo]
            ,[Descricao]
            ,[Norma]
            ,[Processo]
            ,[Status]
            ,[FechadoEm]
            ,[FechadoPor]
            ,[AcoesCorretivas]
            ,[AcoesPreventivas]	        )	        VALUES (		        @Codigo,		        @CriadoPor,		        @CriadoEm,		        @modificadoPor,		        @modificadoEm,		        @EstadoRegistro,		        @Titulo,		        @Descricao,		        @Norma,		        @Processo,		        @Status,		        @FechadoEm,		        @FechadoPor,		        @AcoesCorretivas,		        @AcoesPreventivas	        )";

        private static readonly string sqlAlterar = $@"UPDATE Ncs SET 	        [CriadoPor] = @CriadoPor,	        [CriadoEm] = @CriadoEm,	        [modificadoPor] = @modificadoPor,	        [modificadoEm] = @modificadoEm,	        [EstadoRegistro] = @EstadoRegistro,	        [Titulo] = @Titulo,	        [Descricao] = @Descricao,	        [Norma] = @Norma,	        [Processo] = @Processo,	        [Status] = @Status,	        [FechadoEm] = @FechadoEm,	        [FechadoPor] = @FechadoPor,	        [AcoesCorretivas] = @AcoesCorretivas,	        [AcoesPreventivas] = @AcoesPreventivas        WHERE [Codigo] = @Codigo";

        private static readonly string sqlExcluir = $@"DELETE FROM Ncs WHERE Codigo = @Codigo";


        private readonly string sqlListarBase = $@"SELECT              [Codigo]
            ,[CriadoPor]
            ,[CriadoEm]
            ,[modificadoPor]
            ,[modificadoEm]
            ,[EstadoRegistro]
            ,[Titulo]
            ,[Descricao]
            ,[Norma]
            ,[Processo]
            ,[Status]
            ,[FechadoEm]
            ,[FechadoPor]
            ,[AcoesCorretivas]
            ,[AcoesPreventivas]        FROM Ncs 
        WHERE 1 = 1";

        #endregion sql

        public NaoConformidadeRepository(IConfiguration configuration, IUnitOfWork unitOfWork, IDistributedCache distributedCache)
        {
            _configuration = configuration;
            _unitOfWork = unitOfWork;
            _cache = new CacheRepository<NaoConformidadeEntity>(distributedCache);
        }

        public async Task<NaoConformidadeEntity> ObterPorCodigo(string codigo)
        {
            var cached = await _cache.ObterPorCodigo(codigo);
            if (cached != null)
                return cached;

            var queryResult = await _unitOfWork.Connection.QueryAsync<NaoConformidadeEntity>(sqlObterPorCodigo, new { Codigo = codigo }, _unitOfWork.Transaction, _unitOfWork.Timeout, CommandType.Text);

            await _cache.AdicionarAsync(queryResult.FirstOrDefault());

            return queryResult.FirstOrDefault();
        }

        public async Task<int> AdicionarAsync(NaoConformidadeEntity entity)
        {
            var queryResult = await _unitOfWork.Connection.ExecuteAsync(sqlAdicionar, entity,
                _unitOfWork.Transaction, _unitOfWork.Timeout, CommandType.Text);

            await _cache.AdicionarAsync(entity);

            return queryResult;
        }

        public async Task<int> AtualizarAsync(NaoConformidadeEntity entity)
        {
            var queryResult = await _unitOfWork.Connection.ExecuteAsync(sqlAlterar, entity,
                _unitOfWork.Transaction, _unitOfWork.Timeout, CommandType.Text);

            await _cache.AtualizarAsync(entity);

            return queryResult;
        }

        public async Task<int> ExcluirAsync(string codigo)
        {
            var queryResult = await _unitOfWork.Connection.ExecuteAsync(sqlExcluir, new { Codigo = codigo },
                _unitOfWork.Transaction, _unitOfWork.Timeout, CommandType.Text);

            await _cache.ExcluirAsync(codigo);

            return queryResult;
        }

        public async Task<IEnumerable<NaoConformidadeListarQueryDTO>> Listar(NaoConformidadeListarQueryFilter filtro)
        {

            var sqlQuery = string.Empty;

            if (!string.IsNullOrWhiteSpace(filtro.Codigo))
                sqlQuery += "AND (codigo = @Codigo)";

            if (!string.IsNullOrWhiteSpace(filtro.Titulo))
                sqlQuery += "AND (titulo LIKE '%' + @Titulo + '%')";

            if (!string.IsNullOrWhiteSpace(filtro.Descricao))
                sqlQuery += "AND (descricao LIKE '%' + @Descricao + '%')";

            if (!string.IsNullOrWhiteSpace(filtro.Norma))
                sqlQuery += "AND (Norma LIKE '%' + @Norma +'%')";

            if (!string.IsNullOrWhiteSpace(filtro.Processo))
                sqlQuery += "AND (Processo LIKE '%' + @Processo + '%')";

            if (filtro.EstadoRegistro != null)
                sqlQuery += "AND (EstadoRegistro = @EstadoRegistro)";

            sqlQuery += " ORDER BY CriadoEm DESC";

            var queryResult = await _unitOfWork.Connection.QueryAsync<NaoConformidadeListarQueryDTO>(sqlListarBase + sqlQuery, filtro,
                _unitOfWork.Transaction, _unitOfWork.Timeout, CommandType.Text);

            return queryResult;
        }

        public async Task<IEnumerable<ProblemaSumarizarQueryDTO>> SumarizarNaoConformidades()
        {
            var problemaQuery =
                await _unitOfWork.Connection.QueryAsync<ProblemaSumarizarQueryDTO>(@"
                SELECT 'Aberto' as status, count(*) as count FROM( SELECT CODIGO FROM Ncs WHERE [status] = 'Aberto') AS A
                UNION ALL
                SELECT 'Fechado' as status, count(*) as count FROM( SELECT CODIGO FROM Ncs WHERE [status] = 'Fechado') AS F
                ", null, _unitOfWork.Transaction);


            return problemaQuery;
        }
    }
}
