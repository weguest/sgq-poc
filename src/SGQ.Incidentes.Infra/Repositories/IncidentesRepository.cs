﻿using System;
using Dapper;
using Microsoft.Extensions.Configuration;
using SGQ.Core.Infra.Repositories;
using SGQ.Incidentes.Domain.Incidente.Entity;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Caching.Distributed;
using SGQ.Incidentes.Domain.Incidente.Query;

namespace SGQ.Incidentes.Infra.Repositories
{
    public class IncidentesRepository : BaseRepository, IIncidentesRepository
    {
        private readonly IConfiguration _configuration;
        private readonly IUnitOfWork _unitOfWork;
        private readonly CacheRepository<IncidenteEntity> _cache;

        public IncidentesRepository(IConfiguration configuration, IUnitOfWork unitOfWork, IDistributedCache distributedCache)
        {
            _configuration = configuration;
            _unitOfWork = unitOfWork;
            _cache = new CacheRepository<IncidenteEntity>(distributedCache);
        }

        public async Task<IEnumerable<IncidenteListarQueryDTO>> Listar(IncidentesListarQueryFilter parametros)
        {

            try
            {
                var sqlQuery = $@"SELECT 
                                TOP 100
	                             codigo 
	                            ,titulo 
	                            ,descricao 
                                ,EstadoRegistro
	                            ,criadoEm 
	                            ,modificadoEm 
	                            ,criadoPor 
                                ,status
                                ,solucao
	                            ,UC.UserName AS criadoPor_Nome 
	                            ,modificadoPor 
	                            ,UM.UserName AS modificadoPor_Nome 
                            FROM [dbo].[Incidentes] I
                                INNER JOIN [SGQ-IdentityDB].[dbo].[AspNetUsers] UC ON UC.ID = I.CRIADOPOR
                                LEFT JOIN [SGQ-IdentityDB].[dbo].[AspNetUsers] UM ON UM.ID = I.MODIFICADOPOR 
                            WHERE 
                                I.EstadoRegistro = @EstadoRegistro
                            ";

                if (!string.IsNullOrWhiteSpace(parametros.status))
                    sqlQuery += " AND (I.status LIKE '%' + @status +  '%')";

                if (!string.IsNullOrWhiteSpace(parametros.solucao))
                    sqlQuery += " AND (I.solucao LIKE '%' + @solucao +  '%')";


                if (!string.IsNullOrWhiteSpace(parametros.codigo))
                    sqlQuery += " AND (I.codigo = @codigo)";

                if (!string.IsNullOrWhiteSpace(parametros.titulo))
                    sqlQuery += " AND (I.titulo LIKE '%' + @titulo +  '%')";

                if (!string.IsNullOrWhiteSpace(parametros.descricao))
                    sqlQuery += " AND (I.descricao LIKE '%' + @descricao +  '%')";

                sqlQuery += " ORDER BY CriadoEm DESC ";

                var queryResult = await _unitOfWork.Connection.QueryAsync<IncidenteListarQueryDTO>(sqlQuery, parametros, _unitOfWork.Transaction);

                return queryResult;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public async Task<IEnumerable<IncidenteSumarizarQueryDTO>> SumarizarIncidentes()
        {
            var sql = @"SELECT 'Aberto' as status, count(*) as count FROM( SELECT CODIGO FROM Incidentes WHERE [status] = 'Aberto')AS A
            UNION ALL
            SELECT 'Fechado' as status, count(*) as count FROM( SELECT CODIGO FROM Incidentes WHERE [status] = 'Fechado') AS F";

            var queryResult = await _unitOfWork.Connection.QueryAsync<IncidenteSumarizarQueryDTO>(sql, null,
                _unitOfWork.Transaction,
                _unitOfWork.Timeout,
                CommandType.Text);

            return queryResult;

        }

        public async Task<IncidenteEntity> ObterPorCodigo(string codigo)
        {
            var cached = await _cache.ObterPorCodigo(codigo);
            if (cached != null)
                return cached;

            var queryResult = await _unitOfWork.Connection.QueryAsync<IncidenteEntity>($@"SELECT 
                     TOP 100
                     [Codigo]
                    ,[CriadoPor]
                    ,[CriadoEm]
                    ,[modificadoPor]
                    ,[modificadoEm]
                    ,[EstadoRegistro]
                    ,[Titulo]
                    ,[Descricao]
                    ,[Status]
                    ,[Solucao]
                  FROM [dbo].[Incidentes]
                  WHERE 
	                [Codigo] = @Codigo", new { Codigo = codigo },
                _unitOfWork.Transaction,
                _unitOfWork.Timeout,
                CommandType.Text);

            await _cache.AdicionarAsync(queryResult?.FirstOrDefault());
            
            return queryResult.FirstOrDefault();

        }

        public async Task<int> AdicionarAsync(IncidenteEntity entity)
        {
            var queryResult = await _unitOfWork.Connection.ExecuteAsync($@"
            INSERT INTO [dbo].[Incidentes]
                (Codigo, CriadoPor, CriadoEm, modificadoPor, modificadoEm, EstadoRegistro, Titulo, Descricao, Status, Solucao)            VALUES(
                @Codigo,
                @CriadoPor,
                @CriadoEm,
                @modificadoPor,
                @modificadoEm,
                @EstadoRegistro,
                @Titulo,
                @Descricao,
                @Status,
                @Solucao)",
                entity,
                _unitOfWork.Transaction,
                _unitOfWork.Timeout,
                CommandType.Text);

            await _cache.AdicionarAsync(entity);

            return queryResult;
        }

        public async Task<int> AtualizarAsync(IncidenteEntity entity)
        {
            var queryResult = await _unitOfWork.Connection.ExecuteAsync($@"
                    UPDATE [dbo].[Incidentes] SET                        CriadoPor          = @CriadoPor,                       CriadoEm           = @CriadoEm,                       modificadoPor      = @modificadoPor,                       modificadoEm       = @modificadoEm,                       EstadoRegistro     = @EstadoRegistro,                       Titulo             = @Titulo,                       Descricao          = @Descricao,                       Status             = @Status,                       Solucao            = @Solucao                     WHERE 
                        Codigo = @Codigo",
                entity,
                _unitOfWork.Transaction,
                _unitOfWork.Timeout,
                CommandType.Text);

            await _cache.AtualizarAsync(entity);

            return queryResult;
        }

        public async Task<int> ExcluirAsync(string codigo)
        {

            var queryResult = await _unitOfWork.Connection.ExecuteAsync($@"
                DELETE FROM [dbo].[Incidentes] WHERE Codigo = @codigo",
                new { codigo = codigo },
                _unitOfWork.Transaction,
                _unitOfWork.Timeout,
                CommandType.Text);

            await _cache.ExcluirAsync(codigo);

            return queryResult;
        }
    }
}
