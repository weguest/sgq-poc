﻿using SGQ.Core.Infra.Repositories;
using SGQ.Incidentes.Domain.NaoConformidade.Entity;
using SGQ.Incidentes.Domain.NaoConformidade.Query;
using System.Collections.Generic;
using System.Threading.Tasks;
using SGQ.Incidentes.Domain.Problema.Query;

namespace SGQ.Incidentes.Infra.Repositories
{
    public interface INaoConformidadeRepository : IBaseRepository<NaoConformidadeEntity>
    {
        Task<IEnumerable<NaoConformidadeListarQueryDTO>> Listar(NaoConformidadeListarQueryFilter filtro);
        Task<IEnumerable<ProblemaSumarizarQueryDTO>> SumarizarNaoConformidades();
    }
}
