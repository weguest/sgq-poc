﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SGQ.Core.Domain.Entity;
using SGQ.Crosscutting.Identity.Context;

namespace SGQ.Core.Application.Handler
{
    public class BaseCommandHandler : Notificavel
    {
        protected readonly IUserService _userService;

        public BaseCommandHandler(IUserService userService)
        {
            _userService = userService;
        }

        public void UsuarioExiste(string user)
        {
            var response = _userService.PorId(user);
            if (response.Succeeded == false)
            {
                Erros.Add(new Erro("Usuario", response.Errors.FirstOrDefault()));
            }
        }

    }
}
