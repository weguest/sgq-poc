﻿using System.Collections.Generic;using System.Data;using System.Linq;using System.Threading.Tasks;using Dapper;using SGQ.Core.Infra.Repositories;using SGQ.Processos.Domain.RespostaChecklist.Entity;using SGQ.Processos.Domain.RespostaChecklist.Query;namespace SGQ.Processos.Infra.Repositories
{
    public class RespostaChecklistRepository : IRespostaChecklistRepository
    {
        #region QuerySQL

        private string queryObterPorCodigo = $@"SELECT [Codigo], 	        [CriadoPor], 	        [CriadoEm], 	        [modificadoPor], 	        [modificadoEm], 	        [EstadoRegistro],             [Grupo],            [Turno],	        [Processo], 	        [Pergunta], 	        [Resposta_Esperada], 	        [Resposta]        FROM RespostasChecklists        WHERE [Codigo] = @Codigo";        private string queryFiltrarRespostas = $@"SELECT [Codigo], 	        [CriadoPor], 	        [CriadoEm], 	        [modificadoPor], 	        [modificadoEm], 	        [EstadoRegistro],             [Grupo],            [Turno],	        [Processo], 	        [Pergunta], 	        [Resposta_Esperada], 	        [Resposta]        FROM RespostasChecklists        WHERE 
            (@Codigo IS NULL OR [Codigo] = @Codigo) AND
            (@Grupo IS NULL OR [Grupo] = @Grupo) AND
            (@Turno IS NULL OR [Turno] = @Turno) AND
            (@Processo IS NULL OR [Processo] = @Processo) AND
            (@Pergunta IS NULL OR [Pergunta] = @Pergunta) AND
            (@Resposta_Esperada IS NULL OR [Resposta_Esperada] = @Resposta_Esperada) AND
            (@Resposta IS NULL OR [Resposta] = @Resposta) AND
            (@EstadoRegistro IS NULL OR [EstadoRegistro] = @EstadoRegistro)";

        private string queryAdicionar = $@"INSERT INTO RespostasChecklists (		    [Codigo],		    [CriadoPor],		    [CriadoEm],		    [modificadoPor],		    [modificadoEm],		    [EstadoRegistro],            [Grupo],            [Turno],		    [Processo],		    [Pergunta],		    [Resposta_Esperada],		    [Resposta]	    )	    VALUES (		    @Codigo,		    @CriadoPor,		    @CriadoEm,		    @modificadoPor,		    @modificadoEm,		    @EstadoRegistro,		    @Grupo,            @Turno,            @Processo,		    @Pergunta,		    @Resposta_Esperada,		    @Resposta	    )";

        private string queryAtualizar = $@"UPDATE RespostasChecklists SET 	        [CriadoPor] = @CriadoPor,	        [CriadoEm] = @CriadoEm,	        [modificadoPor] = @modificadoPor,	        [modificadoEm] = @modificadoEm,	        [EstadoRegistro] = @EstadoRegistro,	        [Grupo] = @Grupo,	        [Turno] = @Turno,	        [Processo] = @Processo,	        [Pergunta] = @Pergunta,	        [Resposta_Esperada] = @Resposta_Esperada,	        [Resposta] = @Resposta        WHERE [Codigo] = @Codigo";

        private string queryExcluir = $@"DELETE FROM RespostasChecklists WHERE codigo = @codigo";

        #endregion

        private readonly IUnitOfWork _uow;

        public RespostaChecklistRepository(IUnitOfWork uow)        {            _uow = uow;        }        public async Task<RespostaChecklistEntity> ObterPorCodigo(string codigo)        {            var query = await _uow.Connection.QueryAsync<RespostaChecklistEntity>(queryObterPorCodigo, new { codigo = codigo }, _uow.Transaction, _uow.Timeout, CommandType.Text);            return query.SingleOrDefault();        }        public async Task<int> AdicionarAsync(RespostaChecklistEntity entity)        {            var queryResult = await _uow.Connection.ExecuteAsync(queryAdicionar, entity, _uow.Transaction, _uow.Timeout, CommandType.Text);            return queryResult;        }        public async Task<int> AtualizarAsync(RespostaChecklistEntity entity)        {            var queryResult = await _uow.Connection.ExecuteAsync(queryAtualizar, entity, _uow.Transaction, _uow.Timeout, CommandType.Text);            return queryResult;        }        public async Task<int> ExcluirAsync(string codigo)        {            var queryResult = await _uow.Connection.ExecuteAsync(queryExcluir, new { codigo = codigo }, _uow.Transaction);            return queryResult;        }        public async Task<IEnumerable<RespostaChecklistListarQueryDTO>> Listar(RespostaChecklistListarQueryFilter filtro)        {            var query = await _uow.Connection.QueryAsync<RespostaChecklistListarQueryDTO>(queryFiltrarRespostas, filtro, _uow.Transaction, _uow.Timeout, CommandType.Text);            return query;        }    }
}