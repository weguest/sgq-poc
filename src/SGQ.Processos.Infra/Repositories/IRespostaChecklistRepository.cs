﻿using System.Collections.Generic;
using System.Threading.Tasks;
using SGQ.Core.Infra.Repositories;
using SGQ.Processos.Domain.RespostaChecklist.Entity;
using SGQ.Processos.Domain.RespostaChecklist.Query;

namespace SGQ.Processos.Infra.Repositories
{
    public interface IRespostaChecklistRepository : IBaseRepository<RespostaChecklistEntity>
    {
        Task<IEnumerable<RespostaChecklistListarQueryDTO>> Listar(RespostaChecklistListarQueryFilter filtro);
    }
}