﻿using SGQ.Core.Infra.Repositories;
using SGQ.Processos.Domain.Atividade.Entity;

namespace SGQ.Processos.Infra.Repositories
{
    public interface IAtividadeRepository : IBaseRepository<AtividadeEntity>
    {
        
    }
}