﻿using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using SGQ.Core.Infra.Repositories;
using SGQ.Processos.Domain.Atividade.Entity;

namespace SGQ.Processos.Infra.Repositories
{
    public class AtividadeRepository : IAtividadeRepository
    {

        private string queryObterPorCodigo = $@"SELECT             [Codigo], 	        [CriadoPor], 	        [CriadoEm], 	        [modificadoPor], 	        [modificadoEm], 	        [EstadoRegistro], 	        [Titulo], 	        [Descricao], 	        [Processo], 	        [Responsavel]        FROM Atividades        WHERE [Codigo] = @Codigo";

        private string queryAdicionar = $@"INSERT INTO Atividades (		    [Codigo],		    [CriadoPor],		    [CriadoEm],		    [modificadoPor],		    [modificadoEm],		    [EstadoRegistro],		    [Titulo],		    [Descricao],		    [Processo],		    [Responsavel]	    )	    VALUES (		    @Codigo,		    @CriadoPor,		    @CriadoEm,		    @modificadoPor,		    @modificadoEm,		    @EstadoRegistro,		    @Titulo,		    @Descricao,		    @Processo,		    @Responsavel	    )";

        private string queryAtualizar = $@"UPDATE Atividades SET 		    [CriadoPor] = @CriadoPor,		    [CriadoEm] = @CriadoEm,		    [modificadoPor] = @modificadoPor,		    [modificadoEm] = @modificadoEm,		    [EstadoRegistro] = @EstadoRegistro,		    [Titulo] = @Titulo,		    [Descricao] = @Descricao,		    [Processo] = @Processo,		    [Responsavel] = @Responsavel	    WHERE [Codigo] = @Codigo";

        private string queryExcluir = $@"DELETE FROM Atividades WHERE codigo = @codigo";

        private readonly IUnitOfWork _uow;

        public AtividadeRepository(IUnitOfWork uow)
        {
            _uow = uow;
        }

        public async Task<AtividadeEntity> ObterPorCodigo(string codigo)
        {
            var query = await _uow.Connection.QueryAsync<AtividadeEntity>(queryObterPorCodigo, new { Codigo = codigo }, _uow.Transaction, _uow.Timeout, CommandType.Text);
            return query.SingleOrDefault();
        }

        public async Task<int> AdicionarAsync(AtividadeEntity entity)
        {
            var queryResult = await _uow.Connection.ExecuteAsync(queryAdicionar, entity, _uow.Transaction, _uow.Timeout, CommandType.Text);
            return queryResult;
        }

        public async Task<int> AtualizarAsync(AtividadeEntity entity)
        {
            var queryResult = await _uow.Connection.ExecuteAsync(queryAtualizar, entity, _uow.Transaction, _uow.Timeout, CommandType.Text);
            return queryResult;
        }

        public async Task<int> ExcluirAsync(string codigo)
        {
            var queryResult = await _uow.Connection.ExecuteAsync(queryExcluir, new { codigo = codigo }, _uow.Transaction);
            return queryResult;
        }
    }
}