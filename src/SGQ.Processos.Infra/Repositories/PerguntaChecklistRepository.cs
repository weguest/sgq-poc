﻿using System.Collections.Generic;using System.Data;using System.Linq;using System.Threading.Tasks;using Dapper;using SGQ.Core.Infra.Repositories;using SGQ.Processos.Domain.PerguntaChecklist.Entity;using SGQ.Processos.Domain.PerguntaChecklist.Query;namespace SGQ.Processos.Infra.Repositories
{
    public class PerguntaChecklistRepository : IPerguntaChecklistRepository
    {

        #region QuerySQL

        private string queryObterPorCodigo = $@"SELECT [Codigo], 	        [CriadoPor], 	        [CriadoEm], 	        [modificadoPor], 	        [modificadoEm], 	        [EstadoRegistro], 	        [Processo], 	        [Pergunta], 	        [Resposta_Esperada]        FROM PerguntasChecklists        WHERE [Codigo] = @Codigo";        private string queryFiltro = $@"SELECT [Codigo], 	        [CriadoPor], 	        [CriadoEm], 	        [modificadoPor], 	        [modificadoEm], 	        [EstadoRegistro], 	        [Processo], 	        [Pergunta], 	        [Resposta_Esperada]        FROM PerguntasChecklists        WHERE 
            (@codigo is null OR [Codigo] = @Codigo) AND
            (@processo is null OR [Processo] = @processo) AND
            (@pergunta is null OR [Pergunta] = @pergunta) AND
            (@resposta_esperada is null OR [Resposta_Esperada] = @resposta_esperada)
";

        private string queryAdicionar = $@"INSERT INTO PerguntasChecklists (	        [Codigo],	        [CriadoPor],	        [CriadoEm],	        [modificadoPor],	        [modificadoEm],	        [EstadoRegistro],	        [Processo],	        [Pergunta],	        [Resposta_Esperada]        )        VALUES (	        @Codigo,	        @CriadoPor,	        @CriadoEm,	        @modificadoPor,	        @modificadoEm,	        @EstadoRegistro,	        @Processo,	        @Pergunta,	        @Resposta_Esperada        )";

        private string queryAtualizar = $@"	UPDATE PerguntasChecklists SET 		    [CriadoPor] = @CriadoPor,		    [CriadoEm] = @CriadoEm,		    [modificadoPor] = @modificadoPor,		    [modificadoEm] = @modificadoEm,		    [EstadoRegistro] = @EstadoRegistro,		    [Processo] = @Processo,		    [Pergunta] = @Pergunta,		    [Resposta_Esperada] = @Resposta_Esperada	    WHERE [Codigo] = @Codigo";

        private string queryExcluir = $@"UPDATE PerguntasChecklists SET EstadoRegistro = 3 WHERE codigo = @codigo";

        #endregion

        private readonly IUnitOfWork _uow;

        public PerguntaChecklistRepository(IUnitOfWork uow)        {            _uow = uow;        }        public async Task<PerguntaChecklistEntity> ObterPorCodigo(string codigo)        {            var query = await _uow.Connection.QueryAsync<PerguntaChecklistEntity>(queryObterPorCodigo, new { codigo = codigo }, _uow.Transaction, _uow.Timeout, CommandType.Text);            return query.SingleOrDefault();        }        public async Task<int> AdicionarAsync(PerguntaChecklistEntity entity)        {            var queryResult = await _uow.Connection.ExecuteAsync(queryAdicionar, entity, _uow.Transaction, _uow.Timeout, CommandType.Text);            return queryResult;        }        public async Task<int> AtualizarAsync(PerguntaChecklistEntity entity)        {            var queryResult = await _uow.Connection.ExecuteAsync(queryAtualizar, entity, _uow.Transaction, _uow.Timeout, CommandType.Text);            return queryResult;        }        public async Task<int> ExcluirAsync(string codigo)        {            var queryResult = await _uow.Connection.ExecuteAsync(queryExcluir, new { codigo = codigo }, _uow.Transaction);            return queryResult;        }        public async Task<IEnumerable<PerguntaChecklistListarQueryDTO>> Listar(PerguntaChecklistListarQueryFilter filtro)        {            var query = await _uow.Connection.QueryAsync<PerguntaChecklistListarQueryDTO>(queryFiltro, filtro, _uow.Transaction, _uow.Timeout, CommandType.Text);            return query;        }    }
}