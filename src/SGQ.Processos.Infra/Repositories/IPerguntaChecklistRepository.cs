﻿using System.Collections.Generic;
using System.Threading.Tasks;
using SGQ.Core.Infra.Repositories;
using SGQ.Processos.Domain.PerguntaChecklist.Entity;
using SGQ.Processos.Domain.PerguntaChecklist.Query;
using SGQ.Processos.Domain.RespostaChecklist.Query;

namespace SGQ.Processos.Infra.Repositories
{
    public interface IPerguntaChecklistRepository : IBaseRepository<PerguntaChecklistEntity>
    {
        Task<IEnumerable<PerguntaChecklistListarQueryDTO>> Listar(PerguntaChecklistListarQueryFilter filtro);
    }
}