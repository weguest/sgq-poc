﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using SGQ.Core.Infra.Repositories;
using SGQ.Processos.Domain.Atividade.Entity;
using SGQ.Processos.Domain.Atividade.Query;
using SGQ.Processos.Domain.PerguntaChecklist.Entity;
using SGQ.Processos.Domain.PerguntaChecklist.Query;
using SGQ.Processos.Domain.Processo.Entity;
using SGQ.Processos.Domain.Processo.Query;
using SGQ.Processos.Domain.RespostaChecklist.Entity;
using SGQ.Processos.Domain.RespostaChecklist.Query;

namespace SGQ.Processos.Infra.Repositories
{
    public class ProcessoRepository : IProcessoRepository
    {
        private readonly IUnitOfWork _uow;

        #region QuerySQL
        private readonly string queryProcessoListar = $@"SELECT
            [Codigo], 
            [CriadoPor], 
            [CriadoEm], 
            [modificadoPor], 
            [modificadoEm], 
            [EstadoRegistro], 
            [Titulo], 
            [Descricao], 
            [Organismo], 
            [norma]
        FROM Processos
        WHERE
            (@Codigo is null or codigo = @Codigo) AND
            (@Titulo is null or titulo LIKE @Titulo) AND
            (@Descricao is null or descricao LIKE @Descricao) AND
            (@Norma is null or norma = @Norma)";

        private readonly string queryProcessoAtividadesListar = $@"SELECT             [Codigo], 	        [CriadoPor], 	        [CriadoEm], 	        [modificadoPor], 	        [modificadoEm], 	        [EstadoRegistro], 	        [Titulo], 	        [Descricao], 	        [Processo], 	        [Responsavel]        FROM Atividades        WHERE [Processo] = @Codigo AND EstadoRegistro = 1 ORDER BY CriadoEm ASC";

        private readonly string queryProcessoPerguntasListar = $@"SELECT [Codigo], 	        [CriadoPor], 	        [CriadoEm], 	        [modificadoPor], 	        [modificadoEm], 	        [EstadoRegistro], 	        [Processo], 	        [Pergunta], 	        [Resposta_Esperada]        FROM PerguntasChecklists        WHERE [Processo] = @Codigo";

        private readonly string queryRespostasPerguntasListar = $@"SELECT [Codigo], 	        [CriadoPor], 	        [CriadoEm], 	        [modificadoPor], 	        [modificadoEm], 	        [EstadoRegistro],             [Grupo],            [Turno],	        [Processo], 	        [Pergunta], 	        [Resposta_Esperada], 	        [Resposta]        FROM RespostasChecklists        WHERE [Processo] = @Codigo";

        private string queryObterPorCodigo = $@"SELECT [Codigo], 	        [CriadoPor], 	        [CriadoEm], 	        [modificadoPor], 	        [modificadoEm], 	        [EstadoRegistro], 	        [Titulo], 	        [Descricao], 	        [Organismo], 	        [norma]        FROM Processos
        WHERE [codigo] = @codigo";

        private string queryAdicionar = $@"INSERT INTO Processos (		    [Codigo],		    [CriadoPor],		    [CriadoEm],		    [modificadoPor],		    [modificadoEm],		    [EstadoRegistro],		    [Titulo],		    [Descricao],		    [Organismo],		    [norma]	    )	    VALUES (		    @Codigo,		    @CriadoPor,		    @CriadoEm,		    @modificadoPor,		    @modificadoEm,		    @EstadoRegistro,		    @Titulo,		    @Descricao,		    @Organismo,		    @norma	    )";

        private string queryAtualizar = $@"UPDATE Processos SET 		    [CriadoPor] = @CriadoPor,		    [CriadoEm] = @CriadoEm,		    [modificadoPor] = @modificadoPor,		    [modificadoEm] = @modificadoEm,		    [EstadoRegistro] = @EstadoRegistro,		    [Titulo] = @Titulo,		    [Descricao] = @Descricao,		    [Organismo] = @Organismo,		    [norma] = @norma	    WHERE [Codigo] = @Codigo";

        private string queryExcluir = $@"DELETE FROM PROCESSOS WHERE CODIGO = @CODIGO";
        #endregion

        public ProcessoRepository(
            IUnitOfWork uow)
        {
            _uow = uow;
        }

        public async Task<ProcessoEntity> ObterPorCodigo(string codigo)
        {
            var query = await _uow.Connection.QueryAsync<ProcessoEntity>(queryObterPorCodigo, new { codigo = codigo }, _uow.Transaction, _uow.Timeout, CommandType.Text);
            return query.SingleOrDefault();
        }

        public async Task<int> AdicionarAsync(ProcessoEntity entity)
        {
            var queryResult = await _uow.Connection.ExecuteAsync(queryAdicionar, entity, _uow.Transaction, _uow.Timeout, CommandType.Text);
            return queryResult;
        }

        public async Task<int> AtualizarAsync(ProcessoEntity entity)
        {
            var queryResult = await _uow.Connection.ExecuteAsync(queryAtualizar, entity, _uow.Transaction, _uow.Timeout, CommandType.Text);
            return queryResult;
        }

        public async Task<int> ExcluirAsync(string codigo)
        {
            var queryResult = await _uow.Connection.ExecuteAsync(queryExcluir, new { codigo = codigo }, _uow.Transaction);
            return queryResult;
        }

        public async Task<IEnumerable<ProcessoListarQueryDTO>> Listar(ProcessoListarQueryFilter filter)
        {
            var queryResult = await _uow.Connection.QueryAsync<ProcessoListarQueryDTO>(queryProcessoListar, filter, _uow.Transaction, _uow.Timeout, CommandType.Text);
            foreach (var item in queryResult)
            {
                item.Atividades = await _uow.Connection.QueryAsync<AtividadeListarQueryDTO>(queryProcessoAtividadesListar, new { Codigo = item.Codigo }, _uow.Transaction, _uow.Timeout, CommandType.Text);
                item.PerguntasChecklist = await _uow.Connection.QueryAsync<PerguntaChecklistListarQueryDTO>(queryProcessoPerguntasListar, new { Codigo = item.Codigo }, _uow.Transaction, _uow.Timeout, CommandType.Text);
                item.RespostasChecklist = await _uow.Connection.QueryAsync<RespostaChecklistListarQueryDTO>(queryRespostasPerguntasListar, new { Codigo = item.Codigo }, _uow.Transaction, _uow.Timeout, CommandType.Text);
            }

            return queryResult;
        }
    }
}
