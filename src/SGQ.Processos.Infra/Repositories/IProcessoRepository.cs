﻿using System.Collections.Generic;
using System.Threading.Tasks;
using SGQ.Core.Infra.Repositories;
using SGQ.Processos.Domain.Processo.Entity;
using SGQ.Processos.Domain.Processo.Query;

namespace SGQ.Processos.Infra.Repositories
{
    public interface IProcessoRepository : IBaseRepository<ProcessoEntity>
    {
        Task<IEnumerable<ProcessoListarQueryDTO>> Listar(ProcessoListarQueryFilter filter);
    }
}