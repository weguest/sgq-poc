﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using SGQ.Core.Application.Handler;
using SGQ.Core.Domain.Command;
using SGQ.Core.Domain.Entity;
using SGQ.Core.Domain.Event;
using SGQ.Crosscutting.Identity.Context;
using SGQ.Processos.Domain.PerguntaChecklist.Query;
using SGQ.Processos.Domain.RespostaChecklist.Command;
using SGQ.Processos.Domain.RespostaChecklist.Entity;
using SGQ.Processos.Domain.RespostaChecklist.Events;
using SGQ.Processos.Domain.RespostaChecklist.Query;
using SGQ.Processos.Infra.Repositories;

namespace SGQ.Processos.Application.RespostaChecklist.Handler
{
    public class RespostaChecklistCommandHandler : BaseCommandHandler,
    IRequestHandler<RespostaChecklistCriarCommand, CommandResult<RespostaChecklistEntity>>,
    IRequestHandler<RespostaChecklistAtualizarCommand, CommandResult<bool>>,
    IRequestHandler<RespostaChecklistExcluirCommand, CommandResult<bool>>,
    IRequestHandler<RespostaFormularQuestionarioCommand, CommandResult<bool>>,
    IRequestHandler<RespostaChecklistResponderQuestionarioCommand, CommandResult<bool>>
    {
        private readonly IMediator _mediator;
        private readonly IEventBus _bus;
        private readonly IProcessoRepository _processoRepository;
        private readonly IRespostaChecklistRepository _RespostaChecklistRepository;
        private readonly IPerguntaChecklistRepository _perguntaChecklistRepository;

        public RespostaChecklistCommandHandler(
            IUserService userService,
            IMediator mediator,
            IEventBus bus,
            IProcessoRepository processoRepository,
            IRespostaChecklistRepository RespostaChecklistRepository,
            IPerguntaChecklistRepository perguntaChecklistRepository) : base(userService)
        {
            _mediator = mediator;
            _bus = bus;
            _processoRepository = processoRepository;
            _RespostaChecklistRepository = RespostaChecklistRepository;
            _perguntaChecklistRepository = perguntaChecklistRepository;
        }

        public async Task<CommandResult<RespostaChecklistEntity>> Handle(RespostaChecklistCriarCommand request, CancellationToken cancellationToken)
        {
            var RespostaChecklist = new RespostaChecklistEntity(request.CriadoPor, request.Processo, request.Grupo, request.Turno, request.Pergunta, request.Resposta_Esperada, request.Resposta);

            //Verifica se usuario existe.
            UsuarioExiste(request.CriadoPor);

            //Adiciona erros da entidade.
            Erros.AddRange(RespostaChecklist.Erros);

            var existe = await _RespostaChecklistRepository.Listar(
                new RespostaChecklistListarQueryFilter()
                {
                    grupo = request.Grupo,
                    turno = request.Turno,
                    processo = request.Processo,
                    pergunta = request.Pergunta
                });

            if (existe.Count() > 0)
                Erros.Add(new Erro("codigo", "A resposta informada já existe."));


            if (Invalido)
                return new CommandResult<RespostaChecklistEntity>()
                {
                    Erros = Erros,
                    Mensagem = "Não foi possivel criar a Resposta",
                    Valido = Valido,
                    Retorno = null
                };

            var retorno = await _RespostaChecklistRepository.AdicionarAsync(RespostaChecklist);

            return new CommandResult<RespostaChecklistEntity>()
            {
                Erros = Erros,
                Mensagem = "Resposta criado com sucesso.",
                Valido = Valido,
                Retorno = RespostaChecklist
            };

        }

        public async Task<CommandResult<bool>> Handle(RespostaChecklistAtualizarCommand request, CancellationToken cancellationToken)
        {
            var RespostaChecklist = await _RespostaChecklistRepository.ObterPorCodigo(request.Codigo);

            if (RespostaChecklist is null)
                Erros.Add(new Erro("Resposta", $"A Resposta informado não existe { request.Codigo }"));

            if (Invalido)
                return new CommandResult<bool>() { Valido = false, Erros = Erros, Mensagem = "Ocorreu um problema ao alterar o registro.", Retorno = false };

            RespostaChecklist.Alterar(
                request.Codigo,
                request.ModificadoPor,
                request.EstadoRegistro,
                request.Processo,
                request.Grupo,
                request.Turno,
                request.Pergunta, request.Resposta_Esperada, request.Resposta);

            //Verifica se usuario existe.
            UsuarioExiste(request.ModificadoPor);

            //Adiciona erros da entidade.
            Erros.AddRange(RespostaChecklist.Erros);

            if (Invalido)
                return new CommandResult<bool>()
                {
                    Erros = Erros,
                    Mensagem = "Não foi possivel alterar o RespostaChecklist",
                    Valido = Valido,
                    Retorno = false
                };

            var retorno = await _RespostaChecklistRepository.AtualizarAsync(RespostaChecklist);

            return new CommandResult<bool>()
            {
                Erros = Erros,
                Mensagem = "RespostaChecklist alterado com sucesso.",
                Valido = Valido,
                Retorno = retorno == 1
            };
        }

        public async Task<CommandResult<bool>> Handle(RespostaChecklistExcluirCommand request, CancellationToken cancellationToken)
        {
            var RespostaChecklist = await _RespostaChecklistRepository.ObterPorCodigo(request.Codigo);

            if (RespostaChecklist is null)
                Erros.Add(new Erro("RespostaChecklist", $"O RespostaChecklist informado não existe { request.Codigo }"));


            //Verifica se usuario existe.
            UsuarioExiste(request.ModificadoPor);

            if (Invalido)
                return new CommandResult<bool>() { Valido = false, Erros = Erros, Mensagem = "Ocorreu um problema ao alterar o registro.", Retorno = false };

            var retorno = await _RespostaChecklistRepository.ExcluirAsync(RespostaChecklist.Codigo);

            return new CommandResult<bool>()
            {
                Erros = Erros,
                Mensagem = "RespostaChecklist excluido com sucesso.",
                Valido = Valido,
                Retorno = retorno == 1
            };
        }

        public async Task<CommandResult<bool>> Handle(RespostaFormularQuestionarioCommand request, CancellationToken cancellationToken)
        {

            //Verifica se usuario existe.
            UsuarioExiste(request.criadoPor);

            if (Invalido)
                return new CommandResult<bool>()
                {
                    Retorno = false,
                    Valido = false,
                    Erros = Erros,
                    Mensagem = "Não foi possível gerar o questionário"
                };

            var perguntas = await _perguntaChecklistRepository.Listar(new PerguntaChecklistListarQueryFilter() { processo = request.Processo });
            foreach (var pergunta in perguntas)
            {
                var existe = await _RespostaChecklistRepository.Listar(new RespostaChecklistListarQueryFilter()
                {
                    processo = request.Processo,
                    pergunta = pergunta.pergunta,
                    grupo = request.Grupo,
                    turno = request.Turno,
                });

                if (existe.Any()) continue;

                var entity = new RespostaChecklistEntity(
                    request.criadoPor,
                    request.Processo,
                    request.Grupo,
                    request.Turno,
                    pergunta.pergunta,
                    pergunta.resposta_esperada,
                    ""
                );

                await _RespostaChecklistRepository.AdicionarAsync(entity);

            }

            return new CommandResult<bool>() { Valido = true, Retorno = true };

        }

        public async Task<CommandResult<bool>> Handle(RespostaChecklistResponderQuestionarioCommand request, CancellationToken cancellationToken)
        {
            //Verifica se usuario existe.
            UsuarioExiste(request.criadoPor);

            if (Invalido)
                return new CommandResult<bool>()
                {
                    Retorno = false,
                    Valido = false,
                    Erros = Erros,
                    Mensagem = "Não foi possível responder o questionário"
                };

            var turnoUnico = request.respostas.Select(r => r.turno).GroupBy(r => r).Count() == 1;
            var grupoUnico = request.respostas.Select(r => r.grupo).GroupBy(r => r).Count() == 1;

            if (grupoUnico == false || turnoUnico == false)
            {
                return new CommandResult<bool>()
                {
                    Retorno = false,
                    Valido = false,
                    Erros = Erros,
                    Mensagem = "O questionário deve ser do mesmo grupo e turno para ser respondido."
                };
            }

            foreach (var item in request.respostas)
            {
                var resposta = await _RespostaChecklistRepository.ObterPorCodigo(item.codigo);

                resposta.Resposta = null;

                //Somente as não respondidas.
                if(String.IsNullOrWhiteSpace(resposta.Resposta) == false) continue;
                
                resposta.Responder(item.resposta);

                await _RespostaChecklistRepository.AtualizarAsync(resposta);

                var processo = await _processoRepository.ObterPorCodigo(resposta.Processo);

                var evento = new RespostaChecklistRespondidaEvent(
                    codigo: resposta.Codigo,
                    modificadoPor: request.criadoPor,
                    organismo: processo.Organismo,
                    norma:processo.Norma,
                    titulo: processo.Titulo,
                    descricao:processo.Descricao,
                    processo: processo.Codigo,
                    grupo:resposta.Grupo,
                    turno:resposta.Turno,
                    pergunta: resposta.Pergunta,
                    resposta:resposta.Resposta,
                    respostaEsperada:resposta.Resposta_Esperada
                );

                _bus.Publish( evento );
            }

            return new CommandResult<bool>()
            {
                Retorno = false,
                Valido = true,
                Erros = Erros,
                Mensagem = "Questionário respondido com sucesso!"
            };
            ;

        }
    }
}