﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using SGQ.Core.Application.Handler;
using SGQ.Core.Domain.Command;
using SGQ.Core.Domain.Entity;
using SGQ.Crosscutting.Identity.Context;
using SGQ.Processos.Domain.PerguntaChecklist.Command;
using SGQ.Processos.Domain.PerguntaChecklist.Entity;
using SGQ.Processos.Infra.Repositories;

namespace SGQ.Processos.Application.PerguntaChecklist.Handler
{
    public class PerguntaChecklistCommandHandler : BaseCommandHandler,
    IRequestHandler<PerguntaChecklistCriarCommand, CommandResult<PerguntaChecklistEntity>>,
    IRequestHandler<PerguntaChecklistAtualizarCommand, CommandResult<bool>>,
    IRequestHandler<PerguntaChecklistExcluirCommand, CommandResult<bool>>
    {
        private readonly IMediator _mediator;
        private readonly IPerguntaChecklistRepository _PerguntaChecklistRepository;

        public PerguntaChecklistCommandHandler(IUserService userService, IMediator mediator, IPerguntaChecklistRepository PerguntaChecklistRepository) : base(userService)
        {
            _mediator = mediator;
            _PerguntaChecklistRepository = PerguntaChecklistRepository;
        }

        public async Task<CommandResult<PerguntaChecklistEntity>> Handle(PerguntaChecklistCriarCommand request, CancellationToken cancellationToken)
        {
            var PerguntaChecklist = new PerguntaChecklistEntity(request.CriadoPor, request.Processo, request.Pergunta,
                request.Resposta_Esperada);

            //Verifica se usuario existe.
            UsuarioExiste(request.CriadoPor);

            //Adiciona erros da entidade.
            Erros.AddRange(PerguntaChecklist.Erros);

            if (Invalido)
                return new CommandResult<PerguntaChecklistEntity>()
                {
                    Erros = Erros,
                    Mensagem = "Não foi possivel criar a Pergunta",
                    Valido = Valido,
                    Retorno = null
                };

            var retorno = await _PerguntaChecklistRepository.AdicionarAsync(PerguntaChecklist);

            return new CommandResult<PerguntaChecklistEntity>()
            {
                Erros = Erros,
                Mensagem = "Pergunta criado com sucesso.",
                Valido = Valido,
                Retorno = PerguntaChecklist
            };

        }

        public async Task<CommandResult<bool>> Handle(PerguntaChecklistAtualizarCommand request, CancellationToken cancellationToken)
        {
            var PerguntaChecklist = await _PerguntaChecklistRepository.ObterPorCodigo(request.Codigo);

            if (PerguntaChecklist is null)
                Erros.Add(new Erro("Pergunta", $"O PerguntaChecklist informado não existe { request.Codigo }"));

            if (Invalido)
                return new CommandResult<bool>() { Valido = false, Erros = Erros, Mensagem = "Ocorreu um problema ao alterar o registro.", Retorno = false };

            PerguntaChecklist.Alterar(request.Codigo, request.ModificadoPor, request.EstadoRegistro, request.Processo,
                request.Pergunta, request.Resposta_Esperada);

            //Verifica se usuario existe.
            UsuarioExiste(request.ModificadoPor);

            //Adiciona erros da entidade.
            Erros.AddRange(PerguntaChecklist.Erros);

            if (Invalido)
                return new CommandResult<bool>()
                {
                    Erros = Erros,
                    Mensagem = "Não foi possivel alterar a Pergunta",
                    Valido = Valido,
                    Retorno = false
                };

            var retorno = await _PerguntaChecklistRepository.AdicionarAsync(PerguntaChecklist);

            return new CommandResult<bool>()
            {
                Erros = Erros,
                Mensagem = "Pergunta alterado com sucesso.",
                Valido = Valido,
                Retorno = retorno == 1
            };
        }

        public async Task<CommandResult<bool>> Handle(PerguntaChecklistExcluirCommand request, CancellationToken cancellationToken)
        {
            var PerguntaChecklist = await _PerguntaChecklistRepository.ObterPorCodigo(request.Codigo);

            if (PerguntaChecklist is null)
                Erros.Add(new Erro("Pergunta", $"A Pergunta informado não existe { request.Codigo }"));


            //Verifica se usuario existe.
            UsuarioExiste(request.ModificadoPor);

            if (Invalido)
                return new CommandResult<bool>() { Valido = false, Erros = Erros, Mensagem = "Ocorreu um problema ao alterar o registro.", Retorno = false };

            var retorno = await _PerguntaChecklistRepository.ExcluirAsync(PerguntaChecklist.Codigo);

            return new CommandResult<bool>()
            {
                Erros = Erros,
                Mensagem = "Pergunta excluido com sucesso.",
                Valido = Valido,
                Retorno = retorno == 1
            };
        }
    }
}