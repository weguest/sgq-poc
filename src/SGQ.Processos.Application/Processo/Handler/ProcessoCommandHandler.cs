﻿using MediatR;
using Microsoft.EntityFrameworkCore.Internal;
using SGQ.Core.Application.Handler;
using SGQ.Core.Domain.Command;
using SGQ.Core.Domain.Entity;
using SGQ.Crosscutting.Identity.Context;
using SGQ.Processos.Domain.Processo.Command;
using SGQ.Processos.Domain.Processo.Entity;
using SGQ.Processos.Domain.Processo.Query;
using SGQ.Processos.Infra.Repositories;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace SGQ.Processos.Application.Processo.Handler
{
    public class ProcessoCommandHandler : BaseCommandHandler,
    IRequestHandler<ProcessoCriarCommand, CommandResult<ProcessoEntity>>,
    IRequestHandler<ProcessoAtualizarCommand, CommandResult<bool>>,
    IRequestHandler<ProcessoExcluirCommand, CommandResult<bool>>
    {
        private readonly IMediator _mediator;
        private readonly IProcessoRepository _processoRepository;
        private readonly IAtividadeRepository _atividadeRepository;
        private readonly IPerguntaChecklistRepository _perguntaChecklistRepository;
        private readonly IPerguntaChecklistRepository _respostaChecklistRepository;

        public ProcessoCommandHandler(IUserService userService, IMediator mediator, 
            IProcessoRepository processoRepository,
            IAtividadeRepository atividadeRepository,
            IPerguntaChecklistRepository perguntaChecklistRepository,
            IPerguntaChecklistRepository respostaChecklistRepository) : base(userService)
        {
            _mediator = mediator;
            _processoRepository = processoRepository;
            _atividadeRepository = atividadeRepository;
            _perguntaChecklistRepository = perguntaChecklistRepository;
            _respostaChecklistRepository = respostaChecklistRepository;
        }

        public async Task<CommandResult<ProcessoEntity>> Handle(ProcessoCriarCommand request, CancellationToken cancellationToken)
        {
            var processo = new ProcessoEntity(request.CriadoPor, request.Organismo, request.Norma, request.Titulo,
                request.Descricao);

            //Verifica se usuario existe.
            UsuarioExiste(request.CriadoPor);

            //Adiciona erros da entidade.
            Erros.AddRange(processo.Erros);

            if (Invalido)
                return new CommandResult<ProcessoEntity>()
                {
                    Erros = Erros,
                    Mensagem = "Não foi possivel criar o processo",
                    Valido = Valido,
                    Retorno = null
                };

            var retorno = await _processoRepository.AdicionarAsync(processo);

            return new CommandResult<ProcessoEntity>()
            {
                Erros = Erros,
                Mensagem = "Processo criado com sucesso.",
                Valido = Valido,
                Retorno = processo
            };

        }

        public async Task<CommandResult<bool>> Handle(ProcessoAtualizarCommand request, CancellationToken cancellationToken)
        {
            var processo = await _processoRepository.ObterPorCodigo(request.Codigo);

            if (processo is null)
                Erros.Add(new Erro("Processo", $"O Processo informado não existe { request.Codigo }"));

            if (Invalido)
                return new CommandResult<bool>() { Valido = false, Erros = Erros, Mensagem = "Ocorreu um problema ao alterar o registro.", Retorno = false };

            processo.Alterar( request.Codigo, request.ModificadoPor, request.EstadoRegistro, request.Titulo, request.Descricao, request.Organismo, request.Norma );
            
            //Verifica se usuario existe.
            UsuarioExiste(request.ModificadoPor);

            //Adiciona erros da entidade.
            Erros.AddRange(processo.Erros);

            if (Invalido)
                return new CommandResult<bool>()
                {
                    Erros = Erros,
                    Mensagem = "Não foi possivel alterar o processo",
                    Valido = Valido,
                    Retorno = false
                };

            var retorno = await _processoRepository.AdicionarAsync(processo);

            return new CommandResult<bool>()
            {
                Erros = Erros,
                Mensagem = "Processo alterado com sucesso.",
                Valido = Valido,
                Retorno = retorno == 1
            };
        }

        public async Task<CommandResult<bool>> Handle(ProcessoExcluirCommand request, CancellationToken cancellationToken)
        {
            var processo = await _processoRepository.ObterPorCodigo(request.Codigo);
            var consulta = await _processoRepository.Listar(new ProcessoListarQueryFilter() {Codigo = request.Codigo});
            
            if (processo is null)
                Erros.Add(new Erro("Processo", $"O Processo informado não existe { request.Codigo }"));

            
            //Verifica se usuario existe.
            UsuarioExiste(request.ModificadoPor);
            
            if(consulta.FirstOrDefault().Atividades.Any())
                Erros.Add(new Erro("Processo", $"O Processo informado { request.Codigo } possui atividades vinculadas."));


            if (consulta.FirstOrDefault().PerguntasChecklist.Any())
                Erros.Add(new Erro("Processo", $"O Processo informado { request.Codigo } possui perguntas vinculadas."));

            if (consulta.FirstOrDefault().RespostasChecklist.Any())
                Erros.Add(new Erro("Processo", $"O Processo informado { request.Codigo } possui respostas vinculadas."));

            if (Invalido)
                return new CommandResult<bool>() { Valido = false, Erros = Erros, Mensagem = "Ocorreu um problema ao alterar o registro.", Retorno = false };

            var retorno = await _processoRepository.ExcluirAsync(processo.Codigo);

            return new CommandResult<bool>()
            {
                Erros = Erros,
                Mensagem = "Processo excluido com sucesso.",
                Valido = Valido,
                Retorno = retorno == 1
            };
        }
    }
}