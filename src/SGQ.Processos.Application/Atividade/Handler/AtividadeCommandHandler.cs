﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using SGQ.Core.Application.Handler;
using SGQ.Core.Domain.Command;
using SGQ.Core.Domain.Entity;
using SGQ.Crosscutting.Identity.Context;
using SGQ.Processos.Domain.Atividade.Command;
using SGQ.Processos.Domain.Atividade.Entity;
using SGQ.Processos.Domain.Processo.Command;
using SGQ.Processos.Infra.Repositories;

namespace SGQ.Processos.Application.Atividade.Handler
{
    public class AtividadeCommandHandlerProcessoCommandHandler : BaseCommandHandler,
        IRequestHandler<AtividadeCriarCommand, CommandResult<AtividadeEntity>>,
        IRequestHandler<AtividadeAtualizarCommand, CommandResult<bool>>,
        IRequestHandler<AtividadeExcluirCommand, CommandResult<bool>>
    {
        private readonly IAtividadeRepository _atividadeRepository;

        public AtividadeCommandHandlerProcessoCommandHandler(IUserService userService, IAtividadeRepository atividadeRepository) : base(userService)
        {
            _atividadeRepository = atividadeRepository;
        }

        public async Task<CommandResult<AtividadeEntity>> Handle(AtividadeCriarCommand request, CancellationToken cancellationToken)
        {
            var atividade = new AtividadeEntity(request.CriadoPor, request.Processo, request.Responsavel, request.Titulo,
                request.Descricao);

            //Verifica se usuario existe.
            UsuarioExiste(request.CriadoPor);

            //Adiciona erros da entidade.
            Erros.AddRange(atividade.Erros);

            if (Invalido)
                return new CommandResult<AtividadeEntity>()
                {
                    Erros = Erros,
                    Mensagem = "Não foi possivel criar o processo",
                    Valido = Valido,
                    Retorno = null
                };

            var retorno = await _atividadeRepository.AdicionarAsync(atividade);

            return new CommandResult<AtividadeEntity>()
            {
                Erros = Erros,
                Mensagem = "Processo criado com sucesso.",
                Valido = Valido,
                Retorno = atividade
            };

        }

        public async Task<CommandResult<bool>> Handle(AtividadeAtualizarCommand request, CancellationToken cancellationToken)
        {
            var atividade = await _atividadeRepository.ObterPorCodigo(request.Codigo);

            if (atividade is null)
                Erros.Add(new Erro("Processo", $"A Atividade informado não existe { request.Codigo }"));

            if (Invalido)
                return new CommandResult<bool>() { Valido = false, Erros = Erros, Mensagem = "Ocorreu um problema ao alterar o registro.", Retorno = false };

            atividade.Alterar(request.Codigo, request.ModificadoPor, request.EstadoRegistro, request.Titulo, request.Descricao, request.Processo, request.Responsavel);

            //Verifica se usuario existe.
            UsuarioExiste(request.ModificadoPor);

            //Adiciona erros da entidade.
            Erros.AddRange(atividade.Erros);

            if (Invalido)
                return new CommandResult<bool>()
                {
                    Erros = Erros,
                    Mensagem = "Não foi possivel alterar a atividade",
                    Valido = Valido,
                    Retorno = false
                };

            var retorno = await _atividadeRepository.AtualizarAsync(atividade);

            return new CommandResult<bool>()
            {
                Erros = Erros,
                Mensagem = "Atividade alterada com sucesso.",
                Valido = Valido,
                Retorno = retorno == 1
            };
        }

        public async Task<CommandResult<bool>> Handle(AtividadeExcluirCommand request, CancellationToken cancellationToken)
        {
            var atividade = await _atividadeRepository.ObterPorCodigo(request.Codigo);

            if (atividade is null)
                Erros.Add(new Erro("Processo", $"A Atividade informado não existe { request.Codigo }"));


            //Verifica se usuario existe.
            UsuarioExiste(request.ModificadoPor);

            if (Invalido)
                return new CommandResult<bool>() { Valido = false, Erros = Erros, Mensagem = "Ocorreu um problema ao alterar o registro.", Retorno = false };

            var retorno = await _atividadeRepository.ExcluirAsync(atividade.Codigo);

            return new CommandResult<bool>()
            {
                Erros = Erros,
                Mensagem = "Atividade excluida com sucesso.",
                Valido = Valido,
                Retorno = retorno == 1
            };
        }
    }
}