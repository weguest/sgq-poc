USE [master]
GO
/****** Object:  Database [SGQ-DATABASE]    Script Date: 05/09/2019 18:09:17 ******/
CREATE DATABASE [SGQ-DATABASE]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'SGQ-DATABASE', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\SGQ-DATABASE.mdf' , SIZE = 4288KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'SGQ-DATABASE_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\SGQ-DATABASE_log.ldf' , SIZE = 1072KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [SGQ-DATABASE] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [SGQ-DATABASE].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [SGQ-DATABASE] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [SGQ-DATABASE] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [SGQ-DATABASE] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [SGQ-DATABASE] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [SGQ-DATABASE] SET ARITHABORT OFF 
GO
ALTER DATABASE [SGQ-DATABASE] SET AUTO_CLOSE ON 
GO
ALTER DATABASE [SGQ-DATABASE] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [SGQ-DATABASE] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [SGQ-DATABASE] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [SGQ-DATABASE] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [SGQ-DATABASE] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [SGQ-DATABASE] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [SGQ-DATABASE] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [SGQ-DATABASE] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [SGQ-DATABASE] SET  ENABLE_BROKER 
GO
ALTER DATABASE [SGQ-DATABASE] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [SGQ-DATABASE] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [SGQ-DATABASE] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [SGQ-DATABASE] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [SGQ-DATABASE] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [SGQ-DATABASE] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [SGQ-DATABASE] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [SGQ-DATABASE] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [SGQ-DATABASE] SET  MULTI_USER 
GO
ALTER DATABASE [SGQ-DATABASE] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [SGQ-DATABASE] SET DB_CHAINING OFF 
GO
ALTER DATABASE [SGQ-DATABASE] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [SGQ-DATABASE] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [SGQ-DATABASE] SET DELAYED_DURABILITY = DISABLED 
GO
USE [SGQ-DATABASE]
GO
/****** Object:  Table [dbo].[Atividades]    Script Date: 05/09/2019 18:09:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Atividades](
	[Codigo] [nvarchar](30) NOT NULL,
	[CriadoPor] [nvarchar](max) NULL,
	[CriadoEm] [datetime2](7) NULL,
	[modificadoPor] [nvarchar](max) NULL,
	[modificadoEm] [datetime2](7) NULL,
	[EstadoRegistro] [int] NULL,
	[Titulo] [nvarchar](max) NULL,
	[Descricao] [nvarchar](max) NULL,
	[Processo] [nvarchar](30) NULL,
	[Responsavel] [nvarchar](30) NULL,
 CONSTRAINT [PK_Atividades] PRIMARY KEY CLUSTERED 
(
	[Codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[IncidenteProblema]    Script Date: 05/09/2019 18:09:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IncidenteProblema](
	[Incidente] [nvarchar](30) NOT NULL,
	[Problema] [nvarchar](30) NOT NULL,
 CONSTRAINT [PK_IncidenteProblema] PRIMARY KEY CLUSTERED 
(
	[Incidente] ASC,
	[Problema] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Incidentes]    Script Date: 05/09/2019 18:09:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Incidentes](
	[Codigo] [nvarchar](30) NOT NULL,
	[CriadoPor] [nvarchar](max) NULL,
	[CriadoEm] [datetime2](7) NULL,
	[modificadoPor] [nvarchar](max) NULL,
	[modificadoEm] [datetime2](7) NULL,
	[EstadoRegistro] [int] NULL,
	[Titulo] [nvarchar](max) NULL,
	[Descricao] [nvarchar](max) NULL,
	[Status] [nvarchar](10) NOT NULL,
	[Solucao] [nvarchar](max) NULL,
 CONSTRAINT [PK_Incidentes] PRIMARY KEY CLUSTERED 
(
	[Codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Ncs]    Script Date: 05/09/2019 18:09:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Ncs](
	[Codigo] [nvarchar](30) NOT NULL,
	[CriadoPor] [nvarchar](max) NULL,
	[CriadoEm] [datetime2](7) NULL,
	[modificadoPor] [nvarchar](max) NULL,
	[modificadoEm] [datetime2](7) NULL,
	[EstadoRegistro] [int] NULL,
	[Titulo] [nvarchar](max) NULL,
	[Descricao] [nvarchar](max) NULL,
	[Norma] [nvarchar](30) NOT NULL,
	[Processo] [nvarchar](30) NOT NULL,
	[Status] [nvarchar](10) NOT NULL,
	[FechadoEm] [datetime2](7) NULL,
	[FechadoPor] [datetime2](7) NULL,
	[AcoesCorretivas] [nvarchar](max) NULL,
	[AcoesPreventivas] [nvarchar](max) NULL,
 CONSTRAINT [PK_Ncs] PRIMARY KEY CLUSTERED 
(
	[Codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PerguntasChecklists]    Script Date: 05/09/2019 18:09:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PerguntasChecklists](
	[Codigo] [nvarchar](30) NOT NULL,
	[CriadoPor] [nvarchar](max) NULL,
	[CriadoEm] [datetime2](7) NULL,
	[modificadoPor] [nvarchar](max) NULL,
	[modificadoEm] [datetime2](7) NULL,
	[EstadoRegistro] [int] NULL,
	[Processo] [nvarchar](30) NULL,
	[Pergunta] [nvarchar](max) NULL,
	[Resposta_Esperada] [nvarchar](max) NULL,
 CONSTRAINT [PK_PerguntasChecklists] PRIMARY KEY CLUSTERED 
(
	[Codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Post]    Script Date: 05/09/2019 18:09:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Post](
	[Codigo] [nvarchar](30) NOT NULL,
	[CriadoPor] [nvarchar](max) NULL,
	[CriadoEm] [datetime2](7) NULL,
	[modificadoPor] [nvarchar](max) NULL,
	[modificadoEm] [datetime2](7) NULL,
	[EstadoRegistro] [int] NULL,
	[Titulo] [nvarchar](max) NULL,
	[Descricao] [nvarchar](max) NULL,
	[Conteudo] [nvarchar](max) NULL,
	[Publico] [bit] NULL,
 CONSTRAINT [PK_Post] PRIMARY KEY CLUSTERED 
(
	[Codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Problemas]    Script Date: 05/09/2019 18:09:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Problemas](
	[Codigo] [nvarchar](30) NOT NULL,
	[CriadoPor] [nvarchar](max) NULL,
	[CriadoEm] [datetime2](7) NULL,
	[modificadoPor] [nvarchar](max) NULL,
	[modificadoEm] [datetime2](7) NULL,
	[EstadoRegistro] [int] NULL,
	[Titulo] [nvarchar](max) NULL,
	[Descricao] [nvarchar](max) NULL,
	[Status] [nvarchar](10) NOT NULL,
	[Solucao] [nvarchar](max) NULL,
 CONSTRAINT [PK_Problemas] PRIMARY KEY CLUSTERED 
(
	[Codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Processos]    Script Date: 05/09/2019 18:09:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Processos](
	[Codigo] [nvarchar](30) NOT NULL,
	[CriadoPor] [nvarchar](max) NULL,
	[CriadoEm] [datetime2](7) NULL,
	[modificadoPor] [nvarchar](max) NULL,
	[modificadoEm] [datetime2](7) NULL,
	[EstadoRegistro] [int] NULL,
	[Titulo] [nvarchar](max) NULL,
	[Descricao] [nvarchar](max) NULL,
	[Organismo] [nvarchar](30) NULL,
	[norma] [nvarchar](120) NULL,
 CONSTRAINT [PK_Processos] PRIMARY KEY CLUSTERED 
(
	[Codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[RespostasChecklists]    Script Date: 05/09/2019 18:09:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RespostasChecklists](
	[Codigo] [nvarchar](30) NOT NULL,
	[CriadoPor] [nvarchar](max) NULL,
	[CriadoEm] [datetime2](7) NULL,
	[modificadoPor] [nvarchar](max) NULL,
	[modificadoEm] [datetime2](7) NULL,
	[EstadoRegistro] [int] NULL,
	[Grupo] [nvarchar](30) NULL,
	[Turno] [nvarchar](30) NULL,
	[Processo] [nvarchar](30) NULL,
	[Pergunta] [nvarchar](max) NULL,
	[Resposta_Esperada] [nvarchar](max) NULL,
	[Resposta] [nvarchar](max) NULL,
 CONSTRAINT [PK_RespostasChecklists] PRIMARY KEY CLUSTERED 
(
	[Codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
INSERT [dbo].[Post] ([Codigo], [CriadoPor], [CriadoEm], [modificadoPor], [modificadoEm], [EstadoRegistro], [Titulo], [Descricao], [Conteudo], [Publico]) VALUES (N'POS2019090502024999', N'fca477ee-0e62-4f5b-b0ff-68484aebca24', CAST(N'2019-09-05 14:27:02.1870000' AS DateTime2), N'fca477ee-0e62-4f5b-b0ff-68484aebca24', CAST(N'2019-09-05 18:55:47.4770000' AS DateTime2), 0, N'Teste', N'teste 55050', N'teste 505050', 0)
GO
INSERT [dbo].[Post] ([Codigo], [CriadoPor], [CriadoEm], [modificadoPor], [modificadoEm], [EstadoRegistro], [Titulo], [Descricao], [Conteudo], [Publico]) VALUES (N'POS2019090503063109', N'fca477ee-0e62-4f5b-b0ff-68484aebca24', CAST(N'2019-09-05 15:40:06.6530000' AS DateTime2), NULL, NULL, 1, N'aaaa', N'eeee', N'eee', 1)
GO
INSERT [dbo].[Processos] ([Codigo], [CriadoPor], [CriadoEm], [modificadoPor], [modificadoEm], [EstadoRegistro], [Titulo], [Descricao], [Organismo], [norma]) VALUES (N'PRC2019082903065328', N'fca477ee-0e62-4f5b-b0ff-68484aebca24', CAST(N'2019-08-29 15:05:06.3500000' AS DateTime2), NULL, NULL, 1, N'Critérios para a qualificação e certificação de inspetores de soldagem', N'Esta Norma estabelece os critérios e a sistemática para a qualificação e certificação de inspetores de soldagem, e descreve as atribuições e responsabilidades para os níveis de qualificação estabelecidos. ', N'ABNT', N'NBR 14842')
GO
ALTER TABLE [dbo].[Atividades]  WITH CHECK ADD  CONSTRAINT [FK_Atividades_Processos] FOREIGN KEY([Processo])
REFERENCES [dbo].[Processos] ([Codigo])
GO
ALTER TABLE [dbo].[Atividades] CHECK CONSTRAINT [FK_Atividades_Processos]
GO
ALTER TABLE [dbo].[IncidenteProblema]  WITH CHECK ADD  CONSTRAINT [FK_IncidenteProblema_Incidentes] FOREIGN KEY([Incidente])
REFERENCES [dbo].[Incidentes] ([Codigo])
GO
ALTER TABLE [dbo].[IncidenteProblema] CHECK CONSTRAINT [FK_IncidenteProblema_Incidentes]
GO
ALTER TABLE [dbo].[IncidenteProblema]  WITH CHECK ADD  CONSTRAINT [FK_IncidenteProblema_Problemas] FOREIGN KEY([Problema])
REFERENCES [dbo].[Problemas] ([Codigo])
GO
ALTER TABLE [dbo].[IncidenteProblema] CHECK CONSTRAINT [FK_IncidenteProblema_Problemas]
GO
ALTER TABLE [dbo].[Ncs]  WITH CHECK ADD  CONSTRAINT [FK_Ncs_Processos] FOREIGN KEY([Processo])
REFERENCES [dbo].[Processos] ([Codigo])
GO
ALTER TABLE [dbo].[Ncs] CHECK CONSTRAINT [FK_Ncs_Processos]
GO
ALTER TABLE [dbo].[PerguntasChecklists]  WITH CHECK ADD  CONSTRAINT [FK_PerguntasChecklists_Processos] FOREIGN KEY([Processo])
REFERENCES [dbo].[Processos] ([Codigo])
GO
ALTER TABLE [dbo].[PerguntasChecklists] CHECK CONSTRAINT [FK_PerguntasChecklists_Processos]
GO
ALTER TABLE [dbo].[RespostasChecklists]  WITH CHECK ADD  CONSTRAINT [FK_RespostasChecklists_Processos] FOREIGN KEY([Processo])
REFERENCES [dbo].[Processos] ([Codigo])
GO
ALTER TABLE [dbo].[RespostasChecklists] CHECK CONSTRAINT [FK_RespostasChecklists_Processos]
GO
USE [master]
GO
ALTER DATABASE [SGQ-DATABASE] SET  READ_WRITE 
GO
