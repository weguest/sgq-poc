
CREATE DATABASE [SGQ-DATABASE];
GO

USE [SGQ-DATABASE]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


/*************************************************************************************************************/
/****   INCIDENTES/PROBLEMAS E NC       **********************************************************************/
/*************************************************************************************************************/
IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'dbo' AND  TABLE_NAME = 'Incidentes'))
	DROP TABLE [dbo].[Incidentes]

IF NOT (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'dbo' AND  TABLE_NAME = 'Incidentes'))
BEGIN
	CREATE TABLE [dbo].[Incidentes](
		[Codigo] [nvarchar](30) NOT NULL,
		[CriadoPor] [nvarchar](max) NULL,
		[CriadoEm] [datetime2](7) NULL,
		[modificadoPor] [nvarchar](max) NULL,
		[modificadoEm] [datetime2](7) NULL,
		[EstadoRegistro] [int] NULL,
		[Titulo] [nvarchar](max) NULL,
		[Descricao] [nvarchar](max) NULL,
		[Status][nvarchar](10) NOT NULL,
		[Solucao][nvarchar](max)  NULL,
	 CONSTRAINT [PK_Incidentes] PRIMARY KEY CLUSTERED 
	(
		[Codigo] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END

GO

IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'dbo' AND  TABLE_NAME = 'Problemas'))
	DROP TABLE [dbo].[Problemas]

IF NOT (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'dbo' AND  TABLE_NAME = 'Problemas'))
BEGIN
	
	CREATE TABLE [dbo].[Problemas](
		[Codigo] [nvarchar](30) NOT NULL,
		[CriadoPor] [nvarchar](max) NULL,
		[CriadoEm] [datetime2](7) NULL,
		[modificadoPor] [nvarchar](max) NULL,
		[modificadoEm] [datetime2](7) NULL,
		[EstadoRegistro] [int] NULL,
		[Titulo] [nvarchar](max) NULL,
		[Descricao] [nvarchar](max) NULL,
		[Status][nvarchar](10) NOT NULL, 
		[Solucao] [nvarchar](max) NULL,
	 CONSTRAINT [PK_Problemas] PRIMARY KEY CLUSTERED 
	(
		[Codigo] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

	
END

GO

IF NOT (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'dbo' AND  TABLE_NAME = 'IncidenteProblema'))
BEGIN
	CREATE TABLE [dbo].[IncidenteProblema](
		[Incidente] [varchar](30) NOT NULL,
		[Problema] [varchar](30) NOT NULL,
	 CONSTRAINT [PK_IncidenteProblema] PRIMARY KEY CLUSTERED 
	(
		[Incidente] ASC,
		[Problema] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]
END 

GO

IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'dbo' AND  TABLE_NAME = 'Ncs'))
	DROP TABLE [dbo].[Ncs]

IF NOT (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'dbo' AND  TABLE_NAME = 'Ncs'))
BEGIN
	
	CREATE TABLE [dbo].[Ncs](
		[Codigo] [nvarchar](30) NOT NULL,
		[CriadoPor] [nvarchar](max) NULL,
		[CriadoEm] [datetime2](7) NULL,
		[modificadoPor] [nvarchar](max) NULL,
		[modificadoEm] [datetime2](7) NULL,
		[EstadoRegistro] [int] NULL,
		[Titulo] [varchar](max) NULL,
		[Descricao] [varchar](max) NULL,
		[Norma] [nvarchar](30) NOT NULL,
		[Processo] [nvarchar](30) NOT NULL,
		[Status] [nvarchar](10) NOT NULL,
		[FechadoEm] [datetime2](7) NULL,
		[FechadoPor] [nvarchar](max) NULL,
		[AcoesCorretivas] [nvarchar](max) NULL,
		[AcoesPreventivas] [nvarchar](max) NULL,
	 CONSTRAINT [PK_Ncs] PRIMARY KEY CLUSTERED 
	(
		[Codigo] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

	
END


/*************************************************************************************************************/
/****   PROCESSOS        *************************************************************************************/
/*************************************************************************************************************/

IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'dbo' AND  TABLE_NAME = 'Processos'))
	DROP TABLE [dbo].[Processos]

IF NOT (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'dbo' AND  TABLE_NAME = 'Processos'))
BEGIN
	
	CREATE TABLE [dbo].[Processos](
		[Codigo] [nvarchar](30) NOT NULL,
		[CriadoPor] [nvarchar](max) NULL,
		[CriadoEm] [datetime2](7) NULL,
		[modificadoPor] [nvarchar](max) NULL,
		[modificadoEm] [datetime2](7) NULL,
		[EstadoRegistro] [int] NULL,
		[Titulo] [nvarchar](max) NULL,
		[Descricao] [nvarchar](max) NULL,
		[Organismo] [nvarchar](30) NULL,
		[norma] [nvarchar](120) NULL,
	 CONSTRAINT [PK_Processos] PRIMARY KEY CLUSTERED 
	(
		[Codigo] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

	
END

IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'dbo' AND  TABLE_NAME = 'Atividades'))
	DROP TABLE [dbo].[Atividades]

IF NOT (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'dbo' AND  TABLE_NAME = 'Atividades'))
BEGIN
	
	CREATE TABLE [dbo].[Atividades](
		[Codigo] [nvarchar](30) NOT NULL,
		[CriadoPor] [nvarchar](max) NULL,
		[CriadoEm] [datetime2](7) NULL,
		[modificadoPor] [nvarchar](max) NULL,
		[modificadoEm] [datetime2](7) NULL,
		[EstadoRegistro] [int] NULL,
		[Titulo] [nvarchar](max) NULL,
		[Descricao] [nvarchar](max) NULL,
		[Processo] [nvarchar](30) NULL,
		[Responsavel] [nvarchar](30) NULL,
	 CONSTRAINT [PK_Atividades] PRIMARY KEY CLUSTERED 
	(
		[Codigo] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

	
END

IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'dbo' AND  TABLE_NAME = 'PerguntasChecklists'))
	DROP TABLE [dbo].[PerguntasChecklists]

IF NOT (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'dbo' AND  TABLE_NAME = 'PerguntasChecklists'))
BEGIN
	
	CREATE TABLE [dbo].[PerguntasChecklists](
		[Codigo] [nvarchar](30) NOT NULL,
		[CriadoPor] [nvarchar](max) NULL,
		[CriadoEm] [datetime2](7) NULL,
		[modificadoPor] [nvarchar](max) NULL,
		[modificadoEm] [datetime2](7) NULL,
		[EstadoRegistro] [int] NULL,
		[Processo] [nvarchar](30) NULL,
		[Pergunta] [nvarchar](max) NULL,
		[Resposta_Esperada] [nvarchar](max) NULL
	 CONSTRAINT [PK_PerguntasChecklists] PRIMARY KEY CLUSTERED 
	(
		[Codigo] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

	
END

IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'dbo' AND  TABLE_NAME = 'RespostasChecklists'))
	DROP TABLE [dbo].[RespostasChecklists]

IF NOT (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'dbo' AND  TABLE_NAME = 'RespostasChecklists'))
BEGIN
	
	CREATE TABLE [dbo].[RespostasChecklists](
		[Codigo] [nvarchar](30) NOT NULL,
		[CriadoPor] [nvarchar](max) NULL,
		[CriadoEm] [datetime2](7) NULL,
		[modificadoPor] [nvarchar](max) NULL,
		[modificadoEm] [datetime2](7) NULL,
		[EstadoRegistro] [int] NULL,
		[Grupo] [nvarchar](30) NULL,
		[Turno] [nvarchar](30) NULL,
		[Processo] [nvarchar](30) NULL,
		[Pergunta] [nvarchar](max) NULL,
		[Resposta_Esperada] [nvarchar](max) NULL,
		[Resposta] [nvarchar](max) NULL
	 CONSTRAINT [PK_RespostasChecklists] PRIMARY KEY CLUSTERED 
	(
		[Codigo] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

	
END

GO

INSERT [dbo].[Processos] ([Codigo], [CriadoPor], [CriadoEm], [modificadoPor], [modificadoEm], [EstadoRegistro], [Titulo], [Descricao], [Organismo], [norma]) 
VALUES (N'PRC2019082903065328', N'fca477ee-0e62-4f5b-b0ff-68484aebca24', CAST(N'2019-08-29 15:05:06.3500000' AS DateTime2), NULL, NULL, 1, N'Critérios para a qualificação e certificação de inspetores de soldagem', N'Esta Norma estabelece os critérios e a sistemática para a qualificação e certificação de inspetores de soldagem, e descreve as atribuições e responsabilidades para os níveis de qualificação estabelecidos. ', N'ABNT', N'NBR 14842')


IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'dbo' AND  TABLE_NAME = 'Post'))
	DROP TABLE [dbo].[Post]

IF NOT (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'dbo' AND  TABLE_NAME = 'Post'))
BEGIN
	CREATE TABLE [dbo].[Post](
		[Codigo] [nvarchar](30) NOT NULL,
		[CriadoPor] [nvarchar](max) NULL,
		[CriadoEm] [datetime2](7) NULL,
		[modificadoPor] [nvarchar](max) NULL,
		[modificadoEm] [datetime2](7) NULL,
		[EstadoRegistro] [int] NULL,
		[Titulo] [nvarchar](max) NULL,
		[Descricao] [nvarchar](max) NULL,
		[Conteudo] [nvarchar](max) NULL,
		[Publico] bit NULL,
	 CONSTRAINT [PK_Post] PRIMARY KEY CLUSTERED 
	(
		[Codigo] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END

GO
