﻿using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SGQ.Crosscutting.Swagger;
using SGQ.Core.Infra.Repositories;
using SGQ.Crosscutting.Identity;
using SGQ.Processos.Infra.Repositories;
using System;
using System.Data;
using System.Data.SqlClient;
using SGQ.Core.Domain.Event;
using SGQ.Infra.Bus;

namespace SGQ.Processos.Presentation.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().AddJsonOptions(options =>
            {
                options.SerializerSettings.Converters.Add(new Newtonsoft.Json.Converters.StringEnumConverter());
                options.SerializerSettings.NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore;
            }).SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            //Domain Bus
            services.AddTransient<IEventBus, RabbitMQBus>(sp => {
                var scopeFactory = sp.GetRequiredService<IServiceScopeFactory>();
                return new RabbitMQBus(sp.GetService<IMediator>(), scopeFactory, Configuration);
            });

            //Registrar Identity
            services.RegisterIdentityServices(Configuration);

            //Registrar Swagger
            services.RegisterSwaggerServices(Configuration, "Processos API", "v1.0");

            //Injetar Dependencias
            services.AddScoped<IDbConnection, SqlConnection>(opt => new SqlConnection(Configuration["SGQ-DATABASE:database"]));
            services.AddScoped<IUnitOfWork, UnitOfWork>();

            services.AddTransient<IProcessoRepository, ProcessoRepository>();
            services.AddTransient<IAtividadeRepository, AtividadeRepository>();
            services.AddTransient<IPerguntaChecklistRepository, PerguntaChecklistRepository>();
            services.AddTransient<IRespostaChecklistRepository, RespostaChecklistRepository>();


            //Injetar MediaTR
            var assembly = AppDomain.CurrentDomain.Load("SGQ.Processos.Application");
            services.AddMediatR(assembly);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.RegisterSwaggerApplication("API Processos v1.0");
            app.RegisterIdentityApplication(env);

            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
