﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SGQ.Core.Domain.Command;
using SGQ.Core.Domain.Entity;
using SGQ.Core.Infra.Repositories;
using SGQ.Processos.Domain.Atividade.Command;
using SGQ.Processos.Domain.PerguntaChecklist.Command;
using SGQ.Processos.Domain.Processo.Command;
using SGQ.Processos.Domain.Processo.Query;
using SGQ.Processos.Domain.RespostaChecklist.Command;
using SGQ.Processos.Domain.RespostaChecklist.Query;
using SGQ.Processos.Infra.Repositories;

namespace SGQ.Processos.Presentation.API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class ProcessoController : CustomBaseController
    {
        private readonly IMediator _mediator;
        private readonly IUnitOfWork _uow;
        private readonly IProcessoRepository _processoRepository;
        private readonly IAtividadeRepository _atividadeRepository;
        private readonly IPerguntaChecklistRepository _perguntaChecklistRepository;
        private readonly IRespostaChecklistRepository _respostaChecklistRepository;

        public ProcessoController(
            IMediator mediator, 
            IUnitOfWork uow, 
            IProcessoRepository processoRepository,
            IAtividadeRepository atividadeRepository,
            IPerguntaChecklistRepository perguntaChecklistRepository,
            IRespostaChecklistRepository respostaChecklistRepository)
        {
            _mediator = mediator;
            _uow = uow;
            _processoRepository = processoRepository;
            _atividadeRepository = atividadeRepository;
            _perguntaChecklistRepository = perguntaChecklistRepository;
            _respostaChecklistRepository = respostaChecklistRepository;
            _uow.Open();
        }

        #region GET
        // GET api/processos
        [HttpGet]
        public async Task<IActionResult> Get([FromQuery] ProcessoListarQueryFilter filtro)
        {
            try
            {
                using (var transaction = _uow.Transaction)
                {
                    var retorno = await _processoRepository.Listar(filtro);
                    return Ok(retorno);
                }
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }

        // GET api/processos/5
        [HttpGet("{codigo}")]
        public async Task<IActionResult> Get([FromRoute] string codigo)
        {
            try
            {
                using (var transaction = _uow.Transaction)
                {
                    var retorno = await _processoRepository.ObterPorCodigo(codigo);
                    return Ok(retorno);
                }
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }

        [HttpGet("{codigo}/Atividade/{codigodAtividade}")]
        public async Task<IActionResult> GetAtividade([FromRoute] string codigo, string codigodAtividade)
        {
            try
            {
                using (var transaction = _uow.Transaction)
                {
                    var retorno = await _atividadeRepository.ObterPorCodigo(codigodAtividade);
                    return Ok(retorno);
                }
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }

        [HttpGet("{codigo}/PerguntaChecklist/{codigoPergunta}")]
        public async Task<IActionResult> GetPergunta([FromRoute] string codigo, string codigoPergunta)
        {
            try
            {
                using (var transaction = _uow.Transaction)
                {
                    var retorno = await _perguntaChecklistRepository.ObterPorCodigo(codigoPergunta);
                    return Ok(retorno);
                }
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }

        [HttpGet("{codigo}/RespostaChecklist")]
        public async Task<IActionResult> GetResposta([FromRoute] string codigo, [FromQuery] RespostaChecklistListarQueryFilter filtro )
        {
            try
            {
                using (var transaction = _uow.Transaction)
                {
                    var retorno = await _respostaChecklistRepository.Listar(filtro);
                    return Ok(retorno);
                }
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }

        [HttpGet("{codigo}/RespostaChecklist/{codigoResposta}")]
        public async Task<IActionResult> GetResposta([FromRoute] string codigo, string codigoResposta)
        {
            try
            {
                using (var transaction = _uow.Transaction)
                {
                    var retorno = await _respostaChecklistRepository.ObterPorCodigo(codigoResposta);
                    return Ok(retorno);
                }
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }
        #endregion

        #region Put
        [HttpPut("{codigo}")]
        public async Task<IActionResult> Put(string codigo, [FromBody] ProcessoAtualizarCommand value)
        {
            try
            {
                using (var transaction = _uow.Transaction)
                {
                    value.ModificadoPor = _userId;
                    var result = await _mediator.Send(value);

                    if (result.Valido)
                    {
                        transaction.Commit();
                        return Ok(result);
                    }
                    else
                    {
                        transaction.Rollback();
                        return BadRequest(result);
                    }
                }
            }
            catch (Exception ex)
            {
                return BadRequest(new CommandResult<bool>() { Valido = false, Erros = new List<Erro>() { new Erro("Criar", ex.Message) } });
            }
        }

        [HttpPut("{codigo}/Atividade")]
        public async Task<IActionResult> Put([FromRoute] string codigo, [FromBody] AtividadeAtualizarCommand value)
        {
            try
            {
                value.ModificadoPor = _userId;
                using (var transaction = _uow.Transaction)
                {
                    var result = await _mediator.Send(value);

                    if (result.Valido)
                    {
                        transaction.Commit();
                        return Ok(result);
                    }
                    else
                    {
                        transaction.Rollback();
                        return BadRequest(result);
                    }
                }
            }
            catch (Exception ex)
            {
                return BadRequest(new CommandResult<bool>() { Valido = false, Erros = new List<Erro>() { new Erro("Criar", ex.Message) } });
            }
        }

        [HttpPut("{codigo}/PerguntaChecklist")]
        public async Task<IActionResult> Put([FromRoute] string codigo, [FromBody] PerguntaChecklistAtualizarCommand value)
        {
            try
            {
                value.ModificadoPor = _userId;
                using (var transaction = _uow.Transaction)
                {
                    var result = await _mediator.Send(value);

                    if (result.Valido)
                    {
                        transaction.Commit();
                        return Ok(result);
                    }
                    else
                    {
                        transaction.Rollback();
                        return BadRequest(result);
                    }
                }
            }
            catch (Exception ex)
            {
                return BadRequest(new CommandResult<bool>() { Valido = false, Erros = new List<Erro>() { new Erro("Criar", ex.Message) } });
            }
        }

        [HttpPut("{codigo}/RespostaChecklist")]
        public async Task<IActionResult> Put([FromRoute] string codigo, [FromBody] RespostaChecklistAtualizarCommand value)
        {
            try
            {
                value.ModificadoPor = _userId;
                using (var transaction = _uow.Transaction)
                {
                    var result = await _mediator.Send(value);

                    if (result.Valido)
                    {
                        transaction.Commit();
                        return Ok(result);
                    }
                    else
                    {
                        transaction.Rollback();
                        return BadRequest(result);
                    }
                }
            }
            catch (Exception ex)
            {
                return BadRequest(new CommandResult<bool>() { Valido = false, Erros = new List<Erro>() { new Erro("Criar", ex.Message) } });
            }
        }
        #endregion

        #region Post
        // POST api/processos
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] ProcessoCriarCommand value)
        {
            try
            {
                using (var transaction = _uow.Transaction)
                {
                    value.CriadoPor = _userId;
                    var result = await _mediator.Send(value);

                    if (result.Valido)
                    {
                        transaction.Commit();
                        return Ok(result);
                    }
                    else
                    {
                        transaction.Rollback();
                        return BadRequest(result);
                    }
                }
            }
            catch (Exception ex)
            {
                return BadRequest(new CommandResult<bool>() { Valido = false, Erros = new List<Erro>() { new Erro("Criar", ex.Message) } });
            }
        }

        // POST api/processos/123456/Atividade
        [HttpPost("{codigo}/Atividade")]
        public async Task<IActionResult> Post([FromRoute] string codigo, [FromBody] AtividadeCriarCommand value)
        {
            try
            {
                using (var transaction = _uow.Transaction)
                {
                    value.CriadoPor = _userId;
                    value.Processo = codigo;
                    var result = await _mediator.Send(value);

                    if (result.Valido)
                    {
                        transaction.Commit();
                        return Ok(result);
                    }
                    else
                    {
                        transaction.Rollback();
                        return BadRequest(result);
                    }
                }
            }
            catch (Exception ex)
            {
                return BadRequest(new CommandResult<bool>() { Valido = false, Erros = new List<Erro>() { new Erro("Criar", ex.Message) } });
            }
        }

        // POST api/processos/123456/PerguntaChecklist
        [HttpPost("{codigo}/PerguntaChecklist")]
        public async Task<IActionResult> Post([FromRoute] string codigo, [FromBody] PerguntaChecklistCriarCommand value)
        {
            try
            {
                using (var transaction = _uow.Transaction)
                {
                    value.CriadoPor = _userId;
                    value.Processo = codigo;
                    var result = await _mediator.Send(value);

                    if (result.Valido)
                    {
                        transaction.Commit();
                        return Ok(result);
                    }
                    else
                    {
                        transaction.Rollback();
                        return BadRequest(result);
                    }
                }
            }
            catch (Exception ex)
            {
                return BadRequest(new CommandResult<bool>() { Valido = false, Erros = new List<Erro>() { new Erro("Criar", ex.Message) } });
            }
        }

        // POST api/processos/123456/RespostaChecklist
        [HttpPost("{codigo}/RespostaChecklist")]
        public async Task<IActionResult> Post([FromRoute] string codigo, [FromBody] RespostaChecklistCriarCommand value)
        {
            try
            {
                using (var transaction = _uow.Transaction)
                {
                    value.CriadoPor = _userId;
                    value.Processo = codigo;
                    var result = await _mediator.Send(value);

                    if (result.Valido)
                    {
                        transaction.Commit();
                        return Ok(result);
                    }
                    else
                    {
                        transaction.Rollback();
                        return BadRequest(result);
                    }
                }
            }
            catch (Exception ex)
            {
                return BadRequest(new CommandResult<bool>() { Valido = false, Erros = new List<Erro>() { new Erro("Criar", ex.Message) } });
            }
        }

        [HttpPost("{codigo}/RespostaChecklist/FormularQuestionario")]
        public async Task<IActionResult> Post([FromRoute] string codigo, [FromBody] RespostaFormularQuestionarioCommand value)
        {
            try
            {
                using (var transaction = _uow.Transaction)
                {
                    value.criadoPor = _userId;
                    value.Processo = codigo;
                    var result = await _mediator.Send(value);

                    if (result.Valido)
                    {
                        transaction.Commit();
                        return Ok(result);
                    }
                    else
                    {
                        transaction.Rollback();
                        return BadRequest(result);
                    }
                }
            }
            catch (Exception ex)
            {
                return BadRequest(new CommandResult<bool>() { Valido = false, Erros = new List<Erro>() { new Erro("Criar", ex.Message) } });
            }
        }

        [HttpPost("{codigo}/RespostaChecklist/ResponderQuestionario")]
        public async Task<IActionResult> Post([FromRoute] string codigo, [FromBody] RespostaChecklistResponderQuestionarioCommand value)
        {
            try
            {
                using (var transaction = _uow.Transaction)
                {
                    value.criadoPor = _userId;
                    value.Processo = codigo;
                    var result = await _mediator.Send(value);

                    if (result.Valido)
                    {
                        transaction.Commit();
                        return Ok(result);
                    }
                    else
                    {
                        transaction.Rollback();
                        return BadRequest(result);
                    }
                }
            }
            catch (Exception ex)
            {
                return BadRequest(new CommandResult<bool>() { Valido = false, Erros = new List<Erro>() { new Erro("Criar", ex.Message) } });
            }
        }
        #endregion Post

        #region Delete
        [HttpDelete("{codigo}/Atividade/{codigoAtividade}")]
        public async Task<IActionResult> DeleteAtividade([FromRoute] string codigo, string codigoAtividade)
        {
            var command = new AtividadeExcluirCommand();
            command.Codigo = codigoAtividade;
            command.ModificadoPor = _userId;

            try
            {
                using (var transaction = _uow.Transaction)
                {

                    var result = await _mediator.Send(command);

                    if (result.Valido)
                    {
                        transaction.Commit();
                        return Ok(result);
                    }
                    else
                    {
                        transaction.Rollback();
                        return BadRequest(result);
                    }
                }

            }
            catch (Exception ex)
            {
                return BadRequest(new CommandResult<bool>()
                {
                    Valido = false,
                    Erros = new List<Erro>() { new Erro("Criar", ex.Message) }
                });
            }
        }

        [HttpDelete("{codigo}/Pergunta/{codigoPergunta}")]
        public async Task<IActionResult> DeletePergunta([FromRoute] string codigo, string codigoPergunta)
        {
            var command = new PerguntaChecklistExcluirCommand();
            command.Codigo = codigo;
            command.ModificadoPor = _userId;

            try
            {
                using (var transaction = _uow.Transaction)
                {

                    var result = await _mediator.Send(command);

                    if (result.Valido)
                    {
                        transaction.Commit();
                        return Ok(result);
                    }
                    else
                    {
                        transaction.Rollback();
                        return BadRequest(result);
                    }
                }

            }
            catch (Exception ex)
            {
                return BadRequest(new CommandResult<bool>()
                {
                    Valido = false,
                    Erros = new List<Erro>() { new Erro("Criar", ex.Message) }
                });
            }
        }

        [HttpDelete("{codigo}/Resposta/{codigoResposta}")]
        public async Task<IActionResult> DeleteResposta([FromRoute] string codigo, string codigoResposta)
        {
            var command = new RespostaChecklistExcluirCommand();
            command.Codigo = codigo;
            command.ModificadoPor = _userId;

            try
            {
                using (var transaction = _uow.Transaction)
                {

                    var result = await _mediator.Send(command);

                    if (result.Valido)
                    {
                        transaction.Commit();
                        return Ok(result);
                    }
                    else
                    {
                        transaction.Rollback();
                        return BadRequest(result);
                    }
                }

            }
            catch (Exception ex)
            {
                return BadRequest(new CommandResult<bool>()
                {
                    Valido = false,
                    Erros = new List<Erro>() { new Erro("Criar", ex.Message) }
                });
            }
        }

        [HttpDelete("{codigo}")]
        public async Task<IActionResult> Delete(string codigo)
        {
            var command = new ProcessoExcluirCommand();
            command.Codigo = codigo;
            command.ModificadoPor = _userId;

            try
            {
                using (var transaction = _uow.Transaction)
                {

                    var result = await _mediator.Send(command);

                    if (result.Valido)
                    {
                        transaction.Commit();
                        return Ok(result);
                    }
                    else
                    {
                        transaction.Rollback();
                        return BadRequest(result);
                    }
                }

            }
            catch (Exception ex)
            {
                return BadRequest(new CommandResult<bool>()
                {
                    Valido = false,
                    Erros = new List<Erro>() { new Erro("Criar", ex.Message) }
                });
            }
        }
        #endregion Delete
    }
}
