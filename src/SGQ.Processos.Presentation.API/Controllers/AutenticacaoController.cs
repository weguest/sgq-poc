﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SGQ.Crosscutting.Identity.Context;

namespace SGQ.Processos.Presentation.API.Controllers
{
    [ApiController, Route("api/[controller]"), AllowAnonymous]
    public class AutenticacaoController : CustomBaseController
    {
        private readonly IUserService _userService;

        public AutenticacaoController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpPost]
        public async Task<IActionResult> Login([FromForm] string login, string senha)
        {
            var r = _userService.Login(login, senha);
            if (r.Succeeded == true) return Ok(r);
            return Forbid();
        }
    }
}