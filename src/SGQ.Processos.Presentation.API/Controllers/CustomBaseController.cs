﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace SGQ.Processos.Presentation.API.Controllers
{
    public class CustomBaseController : ControllerBase
    {
        protected string _userId
        {
            get { return User.FindFirstValue(ClaimTypes.NameIdentifier); }
        }
    }
}
