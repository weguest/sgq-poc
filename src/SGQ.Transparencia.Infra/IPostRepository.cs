﻿using SGQ.Core.Infra.Repositories;
using SGQ.Processos.Domain.Atividade.Entity;
using SGQ.Transparencia.Domain.Post.Entity;
using SGQ.Transparencia.Domain.Post.Query;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SGQ.Transparencia.Infra
{
    public interface IPostRepository: IBaseRepository<PostEntity>
    {
        Task<IEnumerable<PostListarQueryDTO>> Listar(PostListarQueryFilter filtro);
    }
}