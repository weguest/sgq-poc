﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using SGQ.Core.Infra.Repositories;
using SGQ.Processos.Domain.Atividade.Entity;
using SGQ.Transparencia.Domain.Post.Entity;
using SGQ.Transparencia.Domain.Post.Query;

namespace SGQ.Transparencia.Infra
{
    public class PostRepository : IPostRepository
    {
        private readonly IUnitOfWork _uow;

        #region SQL

        private string sqlListar = @"SELECT 
         [Codigo]
        ,[CriadoPor]
        ,[CriadoEm]
        ,[modificadoPor]
        ,[modificadoEm]
        ,[EstadoRegistro]
        ,[Titulo]
        ,[Descricao]
        ,[Conteudo]
        ,[Publico]
        FROM [dbo].[Post] (nolock)
        WHERE
        (@codigo IS NULL OR Codigo = @codigo) AND
        (@titulo IS NULL OR Titulo LIKE '%' + @titulo + '%') AND
        (@descricao IS NULL OR Descricao LIKE '%' + @descricao + '%') AND
        (@conteudo IS NULL OR Conteudo LIKE '%' + @conteudo + '%') AND
        (@publico IS NULL OR Publico = @publico)";

        private string sqlObterPorCodigo = @"SELECT 
             [Codigo]
            ,[CriadoPor]
            ,[CriadoEm]
            ,[modificadoPor]
            ,[modificadoEm]
            ,[EstadoRegistro]
            ,[Titulo]
            ,[Descricao]
            ,[Conteudo]
            ,[Publico]
        FROM [dbo].[Post] (nolock) 
        WHERE Codigo = @Codigo";

        private string sqlExcluir = @" DELETE FROM [dbo].[Post] WHERE Codigo = @Codigo";

        private string sqlAtualizar = @"UPDATE [dbo].[Post]
        SET [CriadoPor] = @CriadoPor
          ,[CriadoEm] = @CriadoEm
          ,[modificadoPor] = @modificadoPor
          ,[modificadoEm] = @modificadoEm
          ,[EstadoRegistro] = @EstadoRegistro
          ,[Titulo] = @Titulo
          ,[Descricao] = @Descricao
          ,[Conteudo] = @Conteudo
          ,[Publico] = @Publico
        WHERE Codigo = @Codigo";

        private string sqlInsert = @"INSERT INTO [dbo].[Post]
           ([Codigo]
           ,[CriadoPor]
           ,[CriadoEm]
           ,[modificadoPor]
           ,[modificadoEm]
           ,[EstadoRegistro]
           ,[Titulo]
           ,[Descricao]
           ,[Conteudo]
           ,[Publico])
     VALUES
           (@Codigo
           ,@CriadoPor
           ,@CriadoEm
           ,@modificadoPor
           ,@modificadoEm
           ,@EstadoRegistro
           ,@Titulo
           ,@Descricao
           ,@Conteudo
           ,@Publico)";
        #endregion

        public PostRepository(IUnitOfWork uow)
        {
            _uow = uow;
        }

        public async Task<IEnumerable<PostListarQueryDTO>> Listar(PostListarQueryFilter filtro)
        {
            var query = await _uow.Connection.QueryAsync<PostListarQueryDTO>(sqlListar, filtro, _uow.Transaction, _uow.Timeout, CommandType.Text);
            return query;
        }

        public async Task<int> AdicionarAsync(PostEntity entity)
        {
            var query = await _uow.Connection.ExecuteAsync(sqlInsert, entity, _uow.Transaction, _uow.Timeout, CommandType.Text);
            return query;
        }

        public async Task<int> AtualizarAsync(PostEntity entity)
        {
            var query = await _uow.Connection.ExecuteAsync(sqlAtualizar, entity, _uow.Transaction, _uow.Timeout, CommandType.Text);
            return query;
        }

        public async Task<int> ExcluirAsync(string codigo)
        {
            var query = await _uow.Connection.ExecuteAsync(sqlExcluir, new { Codigo = codigo }, _uow.Transaction, _uow.Timeout, CommandType.Text);
            return query;
        }

        public async Task<PostEntity> ObterPorCodigo(string codigo)
        {
            var query = await _uow.Connection.QueryAsync<PostEntity>(sqlObterPorCodigo, new { Codigo = codigo }, _uow.Transaction, _uow.Timeout, CommandType.Text);
            return query.SingleOrDefault();
        }
    }
}