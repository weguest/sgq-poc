﻿using MediatR;
using SGQ.Core.Application.Handler;
using SGQ.Core.Domain.Command;
using SGQ.Crosscutting.Identity.Context;
using SGQ.Transparencia.Domain.Post.Command;
using SGQ.Transparencia.Domain.Post.Entity;
using SGQ.Transparencia.Infra;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SGQ.Transparencia.Application.Post.Handler
{
    public class PostCommandHandler : BaseCommandHandler,
        IRequestHandler<PostCriarCommand, CommandResult<PostEntity>>,
        IRequestHandler<PostAtualizarCommand, CommandResult<PostEntity>>,
        IRequestHandler<PostExcluirCommand, CommandResult<bool>>
    {
        private readonly IPostRepository _postRepository;

        public PostCommandHandler(IUserService userService, IPostRepository postRepository) : base(userService)
        {
            _postRepository = postRepository;
        }

        public async Task<CommandResult<PostEntity>> Handle(PostCriarCommand request, CancellationToken cancellationToken)
        {
            var post = new PostEntity(request.Titulo, request.Descricao, request.Conteudo, request.Publico, request.CriadoPor);


            //Verifica se usuario existe.
            UsuarioExiste(request.CriadoPor);

            //Adiciona erros da entidade.
            Erros.AddRange(post.Erros);

            if (Invalido)
                return new CommandResult<PostEntity>()
                {
                    Erros = Erros,
                    Mensagem = "Não foi possivel criar o post",
                    Valido = Valido,
                    Retorno = null
                };

            var retorno = await _postRepository.AdicionarAsync(post);

            return new CommandResult<PostEntity>()
            {
                Erros = Erros,
                Mensagem = "Post criado com sucesso.",
                Valido = Valido,
                Retorno = post
            };
        }

        public async Task<CommandResult<PostEntity>> Handle(PostAtualizarCommand request, CancellationToken cancellationToken)
        {
            var post = await _postRepository.ObterPorCodigo(request.Codigo);
            
            if(post is null)
                return new CommandResult<PostEntity>()
                {
                    Erros = Erros,
                    Mensagem = $"Post de codigo {request.Codigo} não encontrado",
                    Valido = Valido,
                    Retorno = null
                };

            post.Alterar(request.Codigo, request.Titulo, request.Descricao, request.Conteudo, request.Public, request.ModificadoPor, request.EstadoRegistro);

            //Verifica se usuario existe.
            UsuarioExiste(request.ModificadoPor);

            //Adiciona erros da entidade.
            Erros.AddRange(post.Erros);

            if (Invalido)
                return new CommandResult<PostEntity>()
                {
                    Erros = Erros,
                    Mensagem = "Não foi possivel atualizar o post",
                    Valido = Valido,
                    Retorno = null
                };

            var retorno = await _postRepository.AtualizarAsync(post);

            return new CommandResult<PostEntity>()
            {
                Erros = Erros,
                Mensagem = "Post atualizado com sucesso.",
                Valido = Valido,
                Retorno = post
            };
        }

        public async Task<CommandResult<bool>> Handle(PostExcluirCommand request, CancellationToken cancellationToken)
        {
            var post = await _postRepository.ObterPorCodigo(request.Codigo);

            if (post is null)
                return new CommandResult<bool>()
                {
                    Erros = Erros,
                    Mensagem = $"Post de codigo {request.Codigo} não encontrado",
                    Valido = Valido,
                    Retorno = false
                };


            //Verifica se usuario existe.
            UsuarioExiste(request.ModificadoPor);

            //Adiciona erros da entidade.
            Erros.AddRange(post.Erros);

            if (Invalido)
                return new CommandResult<bool>()
                {
                    Erros = Erros,
                    Mensagem = "Não foi possivel excluir o post",
                    Valido = Valido,
                    Retorno = false
                };

            var retorno = await _postRepository.ExcluirAsync( request.Codigo );

            return new CommandResult<bool>()
            {
                Erros = Erros,
                Mensagem = "Post excluido com sucesso.",
                Valido = Valido,
                Retorno = true
            };
        }
    }
}
