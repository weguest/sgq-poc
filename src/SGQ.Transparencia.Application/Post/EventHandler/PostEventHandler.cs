﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using MediatR;
using SGQ.Core.Domain.Event;
using SGQ.Core.Infra.Repositories;
using SGQ.Transparencia.Domain.Post.Command;
using SGQ.Transparencia.Domain.Post.Events;

namespace SGQ.Transparencia.Application.Post.EventHandler
{
    public class PostEventHandler : IEventHandler<NaoConformidadeCriadaEvent>
    {

        private readonly IMediator _mediator;
        private readonly IUnitOfWork _uow;

        public PostEventHandler(IMediator mediator, IUnitOfWork uow)
        {
            _mediator = mediator;
            _uow = uow;
            _uow.Open();
        }
        public Task Handler(NaoConformidadeCriadaEvent @event)
        {
            var retorno = _mediator.Send(new PostCriarCommand()
            {
                CriadoPor = @event.Criadopor,
                Titulo = $"Não Conformidade do processo {@event.Processo} e norma {@event.Norma}",
                Descricao = @event.Descricao,
                Publico = false,
                Conteudo = $"Nao conformidade quanto a norma  {@event.Norma} do processo {@event.Processo}."
            }).GetAwaiter().GetResult();

            _uow.Transaction.Commit();

            return Task.CompletedTask;
        }

    }
}
