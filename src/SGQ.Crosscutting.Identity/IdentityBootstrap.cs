﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using projeto.identity.infra.identity.Services;
using SGQ.Crosscutting.Identity.Context;
using SGQ.Crosscutting.Identity.Models;
using System;
using System.Text;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity.UI;

namespace SGQ.Crosscutting.Identity
{
    public static class IdentityBootstrap
    {

        private static IConfiguration _configuration { get; set; }

        public static IServiceCollection RegisterIdentityServices(this IServiceCollection services, IConfiguration configuration)
        {

            _configuration = configuration;

            services.AddDbContext<ApplicationDbContext>(options =>
            {
                options.UseSqlServer(_configuration["Identity:database"], opt => opt.EnableRetryOnFailure());
            });

            services.AddIdentity<AppUser, IdentityRole>()
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders();

            services.Configure<IdentityOptions>(options =>
            {
                // Password settings
                options.Password.RequireDigit = false;
                options.Password.RequiredLength = 3;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireUppercase = false;
                options.Password.RequireLowercase = false;
            });

            var sharedKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["Jwt:Key"]));
            var key = new SigningCredentials(sharedKey, SecurityAlgorithms.HmacSha256Signature);

            services.AddAuthentication(authOptions =>
            {
                authOptions.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                authOptions.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(jwtBearerOptions =>
            {
                jwtBearerOptions.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuer = false,
                    ValidateActor = false,
                    ValidateAudience = false,
                    ValidateLifetime = true,
                    ValidateIssuerSigningKey = true,
                    ValidIssuer = _configuration["Jwt:Issuer"],
                    ValidAudience = _configuration["Jwt:Audience"],
                    IssuerSigningKey = key.Key
                };
            });

            services.AddAuthorization(auth =>
            {
                auth.AddPolicy("Bearer", new AuthorizationPolicyBuilder()
                    .AddAuthenticationSchemes(JwtBearerDefaults.AuthenticationScheme‌​)
                    .RequireAuthenticatedUser().Build());

                auth.AddPolicy("user", policy => policy.RequireClaim("Store", "user"));
                auth.AddPolicy("admin", policy => policy.RequireClaim("Store", "admin"));
            });

            services.AddAuthorization(options =>
            {

            });

            services.AddTransient<IUserService, UserService>();

            SeedAdmin(services);

            return services;
        }

        public static IServiceCollection RegisterIdentityServiceUsingCookies(this IServiceCollection services,
            IConfiguration configuration)
        {

            _configuration = configuration;

            services.Configure<CookiePolicyOptions>(options =>
            {
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            services.AddDbContext<ApplicationDbContext>(options =>
            {
                options.UseSqlServer(_configuration["Identity:database"], opt => opt.EnableRetryOnFailure());
            });

            services.AddIdentity<AppUser, IdentityRole>()
                .AddEntityFrameworkStores<ApplicationDbContext>();

            services.Configure<IdentityOptions>(options =>
            {
                // Password settings
                options.Password.RequireDigit = false;
                options.Password.RequiredLength = 3;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireUppercase = false;
                options.Password.RequireLowercase = false;
            });

            services.ConfigureApplicationCookie(options =>
            {
                // Cookie settings
                options.Cookie.HttpOnly = true;
                options.ExpireTimeSpan = TimeSpan.FromMinutes( Int32.Parse(_configuration["Jwt:ExpiryMinutes"]) );

                options.LoginPath = "/Seguranca/Autenticacao/Login";
                options.AccessDeniedPath = "/Seguranca/Autenticacao/AcessoNegado";
                options.SlidingExpiration = true;
            });

            services.AddTransient<IUserService, UserService>();

            SeedAdmin(services);

            return services;
        }

        private static void SeedAdmin(IServiceCollection services)
        {
            var buildServiceProvider = services.BuildServiceProvider();
            var userService = buildServiceProvider.GetService<IUserService>();
            userService.SeedAdmin();
        }

        public static IApplicationBuilder RegisterIdentityApplication(this IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseAuthentication();
            return app;
        }
    }
}
