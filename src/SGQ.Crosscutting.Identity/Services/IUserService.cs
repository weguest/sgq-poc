﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using SGQ.Crosscutting.Identity.Context.Models;
using SGQ.Crosscutting.Identity.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SGQ.Crosscutting.Identity.Context
{
    public interface IUserService
    {

        IdentityResult<AppUser> PorId(string id);
        IdentityResult<AppUser> Register(string login, string password, string role);
        IdentityResult<object> Login(string login, string password);
        IdentityResult<object> Funcao(string id, string funcao);
        IdentityResult<object> FuncaoListar(string id);
        IdentityResult<object> FromCookie(string id);
        void SeedAdmin();
    }
}
