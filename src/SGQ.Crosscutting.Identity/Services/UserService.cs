﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using SGQ.Crosscutting.Identity.Context;
using SGQ.Crosscutting.Identity.Context.Models;
using SGQ.Crosscutting.Identity.Models;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace projeto.identity.infra.identity.Services
{
    public class UserService : IUserService
    {
        private readonly ApplicationDbContext _context;
        private readonly SignInManager<AppUser> _signInManager;
        private readonly UserManager<AppUser> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;

        private readonly IConfiguration _configuration;
        private IUserService _userServiceImplementation;


        public UserService(
            ApplicationDbContext context,
            IConfiguration configuration,
            SignInManager<AppUser> signInManager,
            UserManager<AppUser> userManager,
            RoleManager<IdentityRole> roleManager)
        {
            _context = context;
            _userManager = userManager;
            _roleManager = roleManager;
            _signInManager = signInManager;
            _configuration = configuration;
        }

        public IdentityResult<object> Login(string login, string password)
        {

            var resultado = new IdentityResult<object>();
            var user = _userManager.FindByNameAsync(login).GetAwaiter().GetResult();

            if (user != null)
            {
                var result = _signInManager.CheckPasswordSignInAsync(user, password, false).GetAwaiter().GetResult();
                if (result.Succeeded)
                {
                    resultado.Succeeded = true;
                    resultado.Data = GetTokenJWT(user);
                    return resultado;
                }
                else
                {
                    resultado.Errors.Add("Usuário ou senha não conferem.");
                    resultado.Succeeded = false;
                    return resultado;
                }
            }
            else
            {
                resultado.Errors.Add("Usuário ou senha não conferem.");
                resultado.Succeeded = false;
                return resultado;
            }
        }

        private object GetTokenJWT(AppUser user)
        {
            var roles = _userManager.GetRolesAsync(user).GetAwaiter().GetResult();
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["Jwt:Key"]));

            #region claims
            IdentityOptions _options = new IdentityOptions();
            var claims = new List<Claim>
            {
                new Claim(_options.ClaimsIdentity.UserIdClaimType, user.Id.ToString()),
                new Claim(_options.ClaimsIdentity.UserNameClaimType, user.UserName)
            };

            var userClaims = _userManager.GetClaimsAsync(user).GetAwaiter().GetResult();
            var userRoles = _userManager.GetRolesAsync(user).GetAwaiter().GetResult();
            claims.AddRange(userClaims);
            foreach (var userRole in userRoles)
            {
                claims.Add(new Claim(ClaimTypes.Role, userRole));
                var role = _roleManager.FindByNameAsync(userRole).GetAwaiter().GetResult();
                if (role != null)
                {
                    var roleClaims = _roleManager.GetClaimsAsync(role).GetAwaiter().GetResult();
                    foreach (Claim roleClaim in roleClaims)
                    {
                        claims.Add(roleClaim);
                    }
                }
            }
            #endregion

            var tokenHandler = new JwtSecurityTokenHandler();
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Audience = _configuration["Jwt:Audience"],
                Issuer = _configuration["Jwt:Issuer"],
                Subject = new ClaimsIdentity(claims),
                Expires = DateTime.Now.ToUniversalTime().AddHours(Int32.Parse(_configuration["Jwt:ExpiryMinutes"])),
                SigningCredentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha256Signature),
            };

            var token_code = tokenHandler.WriteToken(tokenHandler.CreateToken(tokenDescriptor));

            return new {
                token = token_code,
                expires = DateTime.Now.ToUniversalTime().AddHours(24)
            };
        }

        public IdentityResult<AppUser> PorId(string id)
        {
            var resultado = new IdentityResult<AppUser>();
            var user = _userManager.FindByIdAsync(id).GetAwaiter().GetResult();

            if (user is null)
            {
                resultado.Errors.Add("Usuário não encontrado.");
                resultado.Succeeded = false;
                return resultado;
            }
            else
            {
                resultado.Succeeded = true;
                resultado.Data = user;
                return resultado;
            }
        }

        public IdentityResult<AppUser> Register(string login, string password, string role)
        {
            var resultado = new IdentityResult<AppUser>();
            var user = new AppUser { UserName = login, Email = login };

            var exist = _userManager.FindByNameAsync(login).GetAwaiter().GetResult();
            if (exist != null)
            {
                resultado.Succeeded = true;
                resultado.Data = exist;
                return resultado;
            }

            var result = _userManager.CreateAsync(user, password).GetAwaiter().GetResult();
            if (result.Succeeded == false)
            {
                resultado.Succeeded = false;
                resultado.Errors = result.Errors.Select(x => x.Description).ToList();
                return resultado;
            }
            else
            {

                var roleCheck = _roleManager.RoleExistsAsync(role).GetAwaiter().GetResult();
                if (!roleCheck)
                {
                    //create the roles and seed them to the database
                    var roleResult = _roleManager.CreateAsync(new IdentityRole(role)).GetAwaiter().GetResult();
                }
                _context.SaveChanges();


                var currentUser = _userManager.FindByNameAsync(login).GetAwaiter().GetResult();
                var roleresult = _userManager.AddToRoleAsync(currentUser, role).GetAwaiter().GetResult();

                _context.SaveChanges();

                resultado.Succeeded = true;
                resultado.Data = currentUser;
                return resultado;
            }
        }

        public IdentityResult<object> Funcao(string id, string funcao)
        {

            var retorno = new IdentityResult<object>();

            var roleCheck = _roleManager.RoleExistsAsync(funcao).GetAwaiter().GetResult();
            if (!roleCheck)
            {
                //create the roles and seed them to the database
                var roleResult = _roleManager.CreateAsync(new IdentityRole(funcao)).GetAwaiter().GetResult();
                _context.SaveChanges();
            }

            var usuario = _userManager.FindByIdAsync(id).GetAwaiter().GetResult();
            var roleresult = _userManager.AddToRoleAsync(usuario, funcao).GetAwaiter().GetResult();

            if (roleresult.Succeeded)
            {
                retorno.Succeeded = true;
                retorno.Data = roleresult;
                return retorno;
            }
            else
            {
                retorno.Succeeded = false;
                retorno.Data = roleresult;
                retorno.Errors = roleresult.Errors.Select(e => e.Description).ToList();
                return retorno;
            }
        }

        public IdentityResult<object> FuncaoListar(string id)
        {
            var retorno = new IdentityResult<object>();
            var usuario = _userManager.FindByIdAsync(id).GetAwaiter().GetResult();
            var funcoes = _userManager.GetRolesAsync(usuario).GetAwaiter().GetResult();

            retorno.Succeeded = true;
            retorno.Data = funcoes;

            return retorno;
        }

        public IdentityResult<object> FromCookie(string id)
        {
            var resultado = new IdentityResult<object>();
            var user =  _userManager.FindByIdAsync(userId: id).GetAwaiter().GetResult();

            if (user is null)
                return resultado;

            resultado.Succeeded = true;
            resultado.Data = GetTokenJWT(user);
            
            return resultado;
        }

        public void SeedAdmin()
        {
            var usuario = _userManager.FindByNameAsync("Admin").GetAwaiter().GetResult();
            if(usuario == null)
                this.Register("Admin", "123456", "Admin");
        }
    }

}
