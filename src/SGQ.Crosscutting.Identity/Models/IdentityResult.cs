﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SGQ.Crosscutting.Identity.Context.Models
{
    public class IdentityResult<T> where T : class
    {

        public bool Succeeded { get; set; }
        public List<string> Errors { get; set; } = new List<string>();
        public T Data { get; set; }
    }
}
