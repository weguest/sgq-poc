﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SGQ.Core.Infra.Repositories;
using SGQ.Incidentes.Infra.Repositories;

namespace SGQ.Incidentes.Presentation.API.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class SumarizadoController : CustomBaseController
    {
        private readonly IUnitOfWork _uow;
        private readonly IIncidentesRepository _incidentesRepository;
        private readonly IProblemaRepository _problemaRepository;
        private readonly INaoConformidadeRepository _naoConformidadeRepository;

        public SumarizadoController(IUnitOfWork uow, 
            IIncidentesRepository incidentesRepository, 
            IProblemaRepository problemaRepository,
            INaoConformidadeRepository naoConformidadeRepository)
        {
            _uow = uow;
            _incidentesRepository = incidentesRepository;
            _problemaRepository = problemaRepository;
            _naoConformidadeRepository = naoConformidadeRepository;
        }

        [HttpGet("Incidente")]
        public async Task<IActionResult> Incidente()
        {
            try
            {
                var retorno = await _incidentesRepository.SumarizarIncidentes();
                return Ok(retorno);
            }
            catch (Exception e)
            {
                return BadRequest();
            }
        }

        [HttpGet("Problema")]
        public async Task<IActionResult> Problema()
        {
            try
            {
                var retorno = await _problemaRepository.SumarizarProblemas();
                return Ok(retorno);
            }
            catch (Exception e)
            {
                return BadRequest();
            }
        }

        [HttpGet("NaoConformidade")]
        public async Task<IActionResult> NaoConformidade()
        {
            try
            {
                var retorno = await _naoConformidadeRepository.SumarizarNaoConformidades();
                return Ok(retorno);
            }
            catch (Exception e)
            {
                return BadRequest();
            }
        }
    }
}