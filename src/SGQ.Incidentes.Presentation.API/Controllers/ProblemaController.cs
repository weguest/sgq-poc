﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SGQ.Core.Domain.Command;
using SGQ.Core.Domain.Entity;
using SGQ.Core.Infra.Repositories;
using SGQ.Incidentes.Domain.Problema.Command;
using SGQ.Incidentes.Domain.Problema.Query;
using SGQ.Incidentes.Infra.Repositories;

namespace SGQ.Incidentes.Presentation.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProblemaController : CustomBaseController
    {
        private readonly IMediator _mediator;
        private readonly IUnitOfWork _uow;
        private readonly IProblemaRepository _problemaRepository;
        private readonly IIncidentesRepository _incidentesRepository;

       

        public ProblemaController(IMediator mediator, IUnitOfWork uow, IProblemaRepository problemaRepository)
        {
            _mediator = mediator;
            _uow = uow;
            _problemaRepository = problemaRepository;

            _uow.Open();
        }

        [HttpGet]
        public async Task<IActionResult> Get([FromQuery]ProblemaListarQueryFilter queryFilter)
        {
            try
            {
                using (var transaction = _uow.Transaction)
                {
                    var retorno = await _problemaRepository.Listar(queryFilter);
                    return Ok(retorno);
                }
            }
            catch (Exception ex)
            {
                return BadRequest();
            }

        }

        [HttpGet("{codigo}")]
        public async Task<IActionResult> Get(string codigo)
        {

            try
            {
                using (var transaction = _uow.Transaction)
                {
                    var result = await _problemaRepository.ObterPorCodigo(codigo);

                    if (result is null)
                        return NotFound();

                    return Ok(result);
                }
            }
            catch (Exception ex)
            {
                return BadRequest(new CommandResult<bool>() { Valido = false, Erros = new List<Erro>() { new Erro("Criar", ex.Message) } });
            }


        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] ProblemaCriarCommand value)
        {
            try
            {
                using (var transaction = _uow.Transaction)
                {
                    value.CriadoPor = _userId;
                    var result = await _mediator.Send(value);

                    if (result.Valido)
                    {
                        transaction.Commit();
                        return Ok(result);
                    }
                    else
                    {
                        transaction.Rollback();
                        return BadRequest(result);
                    }
                }
            }
            catch (Exception ex)
            {
                return BadRequest(new CommandResult<bool>() { Valido = false, Erros = new List<Erro>() { new Erro("Criar", ex.Message) } });
            }
        }

        [HttpPost("{codigo}/Resolver")]
        public async Task<IActionResult> Post([FromRoute] string codigo, [FromBody] ProblemaResolverCommand value)
        {
            try
            {
                using (var transaction = _uow.Transaction)
                {
                    value.ModificadoPor = _userId;
                    value.Codigo = codigo;
                    var result = await _mediator.Send(value);

                    if (result.Valido)
                    {
                        transaction.Commit();
                        return Ok(result);
                    }
                    else
                    {
                        transaction.Rollback();
                        return BadRequest(result);
                    }
                }
            }
            catch (Exception ex)
            {
                return BadRequest(new CommandResult<bool>() { Valido = false, Erros = new List<Erro>() { new Erro("Criar", ex.Message) } });
            }
        }

        [HttpPut("{codigo}")]
        public async Task<IActionResult> Put(string codigo, [FromBody] ProblemaAtualizarCommand value)
        {
            try
            {
                using (var transaction = _uow.Transaction)
                {
                    value.ModificadoPor = _userId;
                    value.Codigo = codigo;
                    var result = await _mediator.Send(value);

                    if (result.Valido)
                    {
                        transaction.Commit();
                        return Ok(result);
                    }
                    else
                    {
                        transaction.Rollback();
                        return BadRequest(result);
                    }
                }
            }
            catch (Exception ex)
            {
                return BadRequest(new CommandResult<bool>() { Valido = false, Erros = new List<Erro>() { new Erro("Criar", ex.Message) } });
            }
        }

        [HttpDelete("{codigo}")]
        public async Task<IActionResult> Delete(string codigo)
        {
            var command = new ProblemaExcluirCommand();
            command.Codigo = codigo;
            command.ModificadoPor = _userId;

            try
            {
                using (var transaction = _uow.Transaction)
                {

                    var result = await _mediator.Send(command);

                    if (result.Valido)
                    {
                        transaction.Commit();
                        return Ok(result);
                    }
                    else
                    {
                        transaction.Rollback();
                        return BadRequest(result);
                    }
                }

            }
            catch (Exception ex)
            {
                return BadRequest(new CommandResult<bool>()
                {
                    Valido = false,
                    Erros = new List<Erro>() { new Erro("Criar", ex.Message) }
                });
            }
        }

        [HttpGet("{codigo}/incidentes")]
        public async Task<IActionResult> GetIncidentes([FromRoute] string codigo)
        {
            try
            {
                using (var transaction = _uow.Transaction)
                {
                    var consulta = await _problemaRepository.ObterPorCodigo(codigo);

                    if (consulta is null)
                        return NotFound();
                    
                    return Ok(consulta.Incidentes);
                }
            }
            catch (Exception ex)
            {
                return BadRequest(new CommandResult<bool>() { Valido = false, Erros = new List<Erro>() { new Erro("Criar", ex.Message) } });
            }

        }

    }
}