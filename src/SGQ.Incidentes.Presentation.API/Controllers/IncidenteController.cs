﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using SGQ.Core.Domain.Command;
using SGQ.Core.Domain.Entity;
using SGQ.Core.Domain.Entity.Enum;
using SGQ.Core.Infra.Repositories;
using SGQ.Incidentes.Domain.Incidente.Command;
using SGQ.Incidentes.Domain.Incidente.Entity;
using SGQ.Incidentes.Domain.Incidente.Query;
using SGQ.Incidentes.Infra.Context;
using SGQ.Incidentes.Infra.Repositories;


namespace SGQ.Incidentes.Presentation.API.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class IncidenteController : ControllerBase
    {

        private string _userId
        {
            get { return User.FindFirstValue(ClaimTypes.NameIdentifier); }
        }

        private readonly IMediator _mediator;
        private readonly IUnitOfWork _uow;
        private readonly IIncidentesRepository _incidentesRepository;


        // GET api/values
        public IncidenteController(IMediator mediator, IUnitOfWork uow, IIncidentesRepository incidentesRepository)
        {
            _mediator = mediator;
            _uow = uow;
            _incidentesRepository = incidentesRepository;

            _uow.Open();
        }

        [HttpGet]
        public async Task<IActionResult> Get([FromQuery]IncidentesListarQueryFilter filtro)
        {
            try
            {
                using (var transaction = _uow.Transaction)
                {
                    var lista = await _incidentesRepository.Listar(filtro);
                    return Ok(lista);
                }
            }
            catch (Exception ex)
            {
                return BadRequest(new CommandResult<bool>() { Valido = false, Erros = new List<Erro>() { new Erro("Listar", ex.Message) } });
            }

        }

        // GET api/values/5
        [HttpGet("{codigo}")]
        public async Task<IActionResult> Get(string codigo)
        {

            try
            {
                using (var transaction = _uow.Transaction)
                {
                    var result = await _incidentesRepository.ObterPorCodigo(codigo);
                    return Ok(result);
                }
            }
            catch (Exception ex)
            {
                return BadRequest(new CommandResult<bool>() { Valido = false, Erros = new List<Erro>() { new Erro("Get", ex.Message) } });
            }


        }

        // POST api/values
        [Authorize]
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] IncidenteCriarCommand value)
        {
            try
            {
                using (var transaction = _uow.Transaction)
                {
                    value.CriadoPor = _userId;
                    var result = await _mediator.Send(value);

                    if (result.Valido)
                    {
                        transaction.Commit();
                        return Ok(result);
                    }
                    else
                    {
                        transaction.Rollback();
                        return BadRequest(result);
                    }
                }
            }
            catch (Exception ex)
            {
                return BadRequest(new CommandResult<bool>() { Valido = false, Erros = new List<Erro>() { new Erro("Criar", ex.Message) } });
            }
        }

        [HttpPost("{codigo}/Fechar")]
        public async Task<IActionResult> Post([FromRoute] string codigo, [FromBody] IncidenteFecharCommand value)
        {
            try
            {
                using (var transaction = _uow.Transaction)
                {
                    value.ModificadoPor = _userId;
                    value.Codigo = codigo;
                    var result = await _mediator.Send(value);

                    if (result.Valido)
                    {
                        transaction.Commit();
                        return Ok(result);
                    }
                    else
                    {
                        transaction.Rollback();
                        return BadRequest(result);
                    }
                }
            }
            catch (Exception ex)
            {
                return BadRequest(new CommandResult<bool>() { Valido = false, Erros = new List<Erro>() { new Erro("Criar", ex.Message) } });
            }
        }

        // PUT api/values/5
        [Authorize]
        [HttpPut("{codigo}")]
        public async Task<IActionResult> Put(string codigo, [FromBody] IncidenteAtualizarCommand value)
        {

            value.Codigo = codigo;
            value.ModificadoPor = _userId;
            
            try
            {
                using (var transaction = _uow.Transaction)
                {
                    var result = await _mediator.Send(value);

                    if (result.Valido)
                    {
                        transaction.Commit();
                        return Ok(result);
                    }
                    else
                    {
                        transaction.Rollback();
                        return BadRequest(result);
                    }
                }
            }
            catch (Exception ex)
            {
                return BadRequest(new CommandResult<bool>() { Valido = false, Erros = new List<Erro>() { new Erro("Alterar", ex.Message) } });
            }
        }

        // DELETE api/values/5
        [Authorize]
        [HttpDelete("{codigo}")]
        public async Task<IActionResult> Delete(string codigo)
        {
            var command = new IncidenteExcluirCommand();
            command.Codigo = codigo;
            command.EstadoRegistro = EnumEstadoRegistro.Excluido;
            command.ModificadoPor = _userId;

            try
            {
                using (var transaction = _uow.Transaction)
                {

                    var result = await _mediator.Send(command);

                    if (result.Valido)
                    {
                        transaction.Commit();
                        return Ok(result);
                    }
                    else
                    {
                        transaction.Rollback();
                        return BadRequest(result);
                    }
                }
            }
            catch (Exception ex)
            {
                return BadRequest(new CommandResult<bool>() { Valido = false, Erros = new List<Erro>() { new Erro("Excluir", ex.Message) } });
            }
        }
    }
}
