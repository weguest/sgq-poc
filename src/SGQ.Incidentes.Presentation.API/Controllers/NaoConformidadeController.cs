﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using SGQ.Core.Domain.Command;
using SGQ.Core.Domain.Entity;
using SGQ.Core.Infra.Repositories;
using SGQ.Incidentes.Domain.NaoConformidade.Command;
using SGQ.Incidentes.Domain.NaoConformidade.Query;
using SGQ.Incidentes.Domain.NC.Command;
using SGQ.Incidentes.Domain.Problema.Command;
using SGQ.Incidentes.Infra.Repositories;

namespace SGQ.Incidentes.Presentation.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class NaoConformidadeController : CustomBaseController
    {
        private readonly IMediator _mediator;
        private readonly INaoConformidadeRepository _naoConformidadeRepository;
        private readonly IUnitOfWork _uow;

        public NaoConformidadeController(IMediator mediator, IUnitOfWork uow,
            INaoConformidadeRepository naoConformidadeRepository)
        {
            _mediator = mediator;
            _uow = uow;
            _naoConformidadeRepository = naoConformidadeRepository;

            _uow.Open();
        }

        [HttpGet]
        public async Task<IActionResult> Get([FromQuery] NaoConformidadeListarQueryFilter queryFilter)
        {
            try
            {
                using (var transaction = _uow.Transaction)
                {
                    var retorno = await _naoConformidadeRepository.Listar(queryFilter);
                    return Ok(retorno);
                }
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }

        [HttpGet("{codigo}")]
        public async Task<IActionResult> Get(string codigo)
        {
            try
            {
                using (var transaction = _uow.Transaction)
                {
                    var result = await _naoConformidadeRepository.ObterPorCodigo(codigo);

                    if (result is null)
                        return NotFound();

                    return Ok(result);
                }
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] NaoConformidadeCriarCommand command)
        {
            try
            {
                using (var transaction = _uow.Transaction)
                {
                    command.CriadoPor = _userId;
                    var result = await _mediator.Send(command);

                    if (result.Valido)
                    {
                        transaction.Commit();
                        return Ok(result);
                    }
                    else
                    {
                        transaction.Rollback();
                        return BadRequest(result);
                    }
                }
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }

        [HttpPut("{codigo}")]
        public async Task<IActionResult> Put([FromRoute]string codigo, [FromBody] NaoConformidadeAtualizarCommand command)
        {
            try
            {
                using (var transaction = _uow.Transaction)
                {
                    command.ModificadoPor = _userId;
                    var result = await _mediator.Send(command);

                    if (result.Valido)
                    {
                        transaction.Commit();
                        return Ok(result);
                    }
                    else
                    {
                        transaction.Rollback();
                        return BadRequest(result);
                    }
                }
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }

        [HttpDelete("{codigo}")]
        public async Task<IActionResult> Delete(string codigo)
        {
            var command = new NaoConformidadeExcluirCommand();
            command.Codigo = codigo;

            try
            {
                using (var transaction = _uow.Transaction)
                {

                    var result = await _mediator.Send(command);

                    if (result.Valido)
                    {
                        transaction.Commit();
                        return Ok(result);
                    }
                    else
                    {
                        transaction.Rollback();
                        return BadRequest(result);
                    }
                }

            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }

        [HttpPost("{codigo}/Fechar")]
        public async Task<IActionResult> Post([FromRoute] string codigo, [FromBody] NaoConformidadeFecharCommand value)
        {
            try
            {
                using (var transaction = _uow.Transaction)
                {
                    value.ModificadoPor = _userId;
                    value.Codigo = codigo;
                    var result = await _mediator.Send(value);

                    if (result.Valido)
                    {
                        transaction.Commit();
                        return Ok(result);
                    }
                    else
                    {
                        transaction.Rollback();
                        return BadRequest(result);
                    }
                }
            }
            catch (Exception ex)
            {
                return BadRequest(new CommandResult<bool>() { Valido = false, Erros = new List<Erro>() { new Erro("Criar", ex.Message) } });
            }
        }
    }
}