﻿using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using SGQ.Crosscutting.Swagger;
using SGQ.Core.Infra.Repositories;
using SGQ.Crosscutting.Identity;
using SGQ.Incidentes.Infra.Repositories;
using System;
using System.Data;
using System.Data.SqlClient;
using SGQ.Core.Domain.Event;
using SGQ.Incidentes.Application.NaoConformidade.EventHandler;
using SGQ.Incidentes.Domain.NaoConformidade.Events;
using SGQ.Infra.Bus;

namespace SGQ.Incidentes.Presentation.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddMvc().AddJsonOptions(options =>
            {
                options.SerializerSettings.Converters.Add(new Newtonsoft.Json.Converters.StringEnumConverter());
                options.SerializerSettings.NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore;
            }).SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            #region EventBus
            //Domain Bus
            services.AddTransient<IEventBus, RabbitMQBus>(sp => {
                var scopeFactory = sp.GetRequiredService<IServiceScopeFactory>();
                return new RabbitMQBus(sp.GetService<IMediator>(), scopeFactory, Configuration);
            });

            //Subscriptions
            services.AddTransient<NaoConformidadeEventHandler>();

            //Domain Events
            services.AddTransient<IEventHandler<RespostaChecklistRespondidaEvent>, NaoConformidadeEventHandler>();

            #endregion EventBus

            //Registrar Identity
            services.RegisterIdentityServices(Configuration);

            //Registrar Swagger
            services.RegisterSwaggerServices(Configuration, "Incidentes API", "v1.0");

            //Injetar Dependencias

            #region InjetarDependencias

            services.AddScoped<IDbConnection, SqlConnection>(opt => new SqlConnection(Configuration["SGQ-DATABASE:database"]));

            services.AddDistributedRedisCache(options =>
            {
                options.Configuration = Configuration.GetConnectionString("Redis");
                options.InstanceName = "User_";
            });

            services.AddScoped<IUnitOfWork, UnitOfWork>();

            services.AddTransient<IIncidentesRepository, IncidentesRepository>();
            services.AddTransient<IProblemaRepository, ProblemaRepository>();
            services.AddTransient<INaoConformidadeRepository, NaoConformidadeRepository>();

            #endregion

            //Injetar MediaTR
            var assembly = AppDomain.CurrentDomain.Load("SGQ.Incidentes.Application");
            services.AddMediatR(assembly);

            //Adicionar Logger?
            services.AddSingleton<ILoggerFactory, LoggerFactory>();
            services.AddSingleton(typeof(ILogger<>), typeof(Logger<>));

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            

            app.RegisterSwaggerApplication("API Incidentes v1.0");
            app.RegisterIdentityApplication(env);

            app.UseHttpsRedirection();
            app.UseMvc();

            var eventBus = app.ApplicationServices.GetRequiredService<IEventBus>();
            eventBus.Subscribe<RespostaChecklistRespondidaEvent, NaoConformidadeEventHandler>();

        }
    }
}
