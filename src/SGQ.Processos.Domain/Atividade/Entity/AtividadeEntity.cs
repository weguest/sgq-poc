﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using SGQ.Core.Domain.Entity;
using SGQ.Core.Domain.Entity.Enum;

namespace SGQ.Processos.Domain.Atividade.Entity
{
    public class AtividadeEntity : EntityBase
    {
        public string Processo { get; set; }
        public string Titulo { get; set; } 
        public string Descricao { get; set; }
        public string Responsavel { get; set; }

        public AtividadeEntity()
        {
            
        }

        public AtividadeEntity(string criadopor, string processo, string responsavel, string titulo, string descricao) : base(criadopor)
        {

            Codigo = GerarNumeroRandomico("ATV", 12);
            Titulo = titulo;
            Descricao = descricao;
            Responsavel = responsavel;
            Processo = processo;

            if (string.IsNullOrWhiteSpace(titulo))
                this.Erros.Add(new Erro("titulo", "Você deve informar o titulo."));

            if (string.IsNullOrWhiteSpace(descricao))
                this.Erros.Add(new Erro("descricao", "Você deve informar uma descrição."));

            if (string.IsNullOrWhiteSpace(responsavel))
                this.Erros.Add(new Erro("norma", "Você deve informar a responsavel."));

            if (string.IsNullOrWhiteSpace(descricao))
                this.Erros.Add(new Erro("processo", "Você deve informar o processo."));
        }

        public void Alterar(string codigo, string modificadopor, EnumEstadoRegistro estado, string titulo, string descricao, string processo, string responsavel)
        {
            
            base.Alterar(codigo, modificadopor, estado);

            Titulo = titulo;
            Descricao = descricao;
            Responsavel = responsavel;
            Processo = processo;


            if (string.IsNullOrWhiteSpace(titulo))
                this.Erros.Add(new Erro("titulo", "Você deve informar o titulo."));

            if (string.IsNullOrWhiteSpace(descricao))
                this.Erros.Add(new Erro("descricao", "Você deve informar uma descrição."));

            if (string.IsNullOrWhiteSpace(responsavel))
                this.Erros.Add(new Erro("norma", "Você deve informar a norma."));

            if (string.IsNullOrWhiteSpace(descricao))
                this.Erros.Add(new Erro("processo", "Você deve informar o processo."));

        }

    }
}
