﻿using MediatR;
using SGQ.Core.Domain.Command;

namespace SGQ.Processos.Domain.Atividade.Command
{
    public class AtividadeExcluirCommand : IRequest<CommandResult<bool>>
    {
        public string Codigo { get; set; }
        public string ModificadoPor { get; set; }
    }
}