﻿using MediatR;
using SGQ.Core.Domain.Command;
using SGQ.Processos.Domain.Atividade.Entity;

namespace SGQ.Processos.Domain.Atividade.Command
{
    public class AtividadeCriarCommand : IRequest<CommandResult<AtividadeEntity>>
    {
        public string CriadoPor { get; set; }
        public string Titulo { get; set; }
        public string Descricao { get; set; }
        public string Processo { get; set; }
        public string Responsavel { get; set; }
    }
}