﻿using System;
using MediatR;
using SGQ.Core.Domain.Command;
using SGQ.Core.Domain.Entity.Enum;

namespace SGQ.Processos.Domain.Atividade.Command
{
    public class AtividadeAtualizarCommand : IRequest<CommandResult<bool>>
    {
        public string Codigo { get; set; }
        public EnumEstadoRegistro EstadoRegistro { get; set; }
        public DateTime ModificadoEm { get; set; } = DateTime.Now;
        public string ModificadoPor { get; set; }
        public string Titulo { get; set; }
        public string Descricao { get; set; }
        public string Processo { get; set; }
        public string Responsavel { get; set; }
    }
}