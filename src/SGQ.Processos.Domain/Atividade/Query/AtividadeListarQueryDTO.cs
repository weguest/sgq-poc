﻿using System;
using System.Collections.Generic;
using System.Text;
using SGQ.Core.Domain.Query;

namespace SGQ.Processos.Domain.Atividade.Query
{
    public class AtividadeListarQueryDTO : IQueryDTO
    {
        public string codigo { get; set; }
        public string processo { get; set; }
        public string titulo { get; set; }
        public string descricao { get; set; }
        public string responsavel { get; set; }
    }
}
