﻿using System;
using System.Collections.Generic;
using System.Text;
using SGQ.Core.Domain.Entity.Enum;

namespace SGQ.Processos.Domain.PerguntaChecklist.Query
{
    public class PerguntaChecklistListarQueryFilter
    {
        public string codigo { get; set; }
        public string processo { get; set; }
        public string pergunta { get; set; }
        public string resposta_esperada { get; set; }
        public EnumEstadoRegistro? EstadoRegistro { get; set; } = EnumEstadoRegistro.Ativo;
    }
}
