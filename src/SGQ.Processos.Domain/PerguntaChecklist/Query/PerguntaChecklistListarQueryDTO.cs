﻿using SGQ.Core.Domain.Query;

namespace SGQ.Processos.Domain.PerguntaChecklist.Query
{
    public class PerguntaChecklistListarQueryDTO : IQueryDTO
    {
        public string codigo { get; set; }
        public string processo { get; set; }
        public string pergunta { get; set; }
        public string resposta_esperada { get; set; }
    }
}