﻿using SGQ.Core.Domain.Entity;
using SGQ.Core.Domain.Entity.Enum;

namespace SGQ.Processos.Domain.PerguntaChecklist.Entity
{
    public class PerguntaChecklistEntity : EntityBase
    {
        public string Processo { get; set; }
        public string Pergunta { get; set; }
        public string Resposta_esperada { get; set; }

        public PerguntaChecklistEntity()
        {
            
        }

        public PerguntaChecklistEntity(string criadopor, string processo, string pergunta, string respostaEsperada) : base(criadopor)
        {

            Codigo = GerarNumeroRandomico("PCK", 12);
            Processo = processo;
            Pergunta = pergunta;
            Resposta_esperada = respostaEsperada;

            if (string.IsNullOrWhiteSpace(processo))
                this.Erros.Add(new Erro("processo", "Você deve informar o processo."));

            if (string.IsNullOrWhiteSpace(pergunta))
                this.Erros.Add(new Erro("pergunta", "Você deve informar uma pergunta."));

            if (string.IsNullOrWhiteSpace(respostaEsperada))
                this.Erros.Add(new Erro("respostaEsperada", "Você deve informar a resposta esperada."));

        }

        public void Alterar(string codigo, string modificadopor, EnumEstadoRegistro estado, string processo, string pergunta, string respostaEsperada)
        {
            base.Alterar(codigo, modificadopor, estado);

            Processo = processo;
            Pergunta = pergunta;
            Resposta_esperada = respostaEsperada;

            if (string.IsNullOrWhiteSpace(processo))
                this.Erros.Add(new Erro("processo", "Você deve informar o processo."));

            if (string.IsNullOrWhiteSpace(pergunta))
                this.Erros.Add(new Erro("pergunta", "Você deve informar uma pergunta."));

            if (string.IsNullOrWhiteSpace(respostaEsperada))
                this.Erros.Add(new Erro("respostaEsperada", "Você deve informar a resposta esperada."));

        }
    }
}
