﻿using MediatR;
using SGQ.Core.Domain.Command;
using SGQ.Core.Domain.Entity.Enum;
using System;

namespace SGQ.Processos.Domain.PerguntaChecklist.Command
{
    public class PerguntaChecklistAtualizarCommand : IRequest<CommandResult<bool>>
    {
        public string Codigo { get; set; }
        public EnumEstadoRegistro EstadoRegistro { get; set; }
        public DateTime ModificadoEm { get; set; } = DateTime.Now;
        public string ModificadoPor { get; set; }
        public string Titulo { get; set; }
        public string Descricao { get; set; }
        public string Processo { get; set; }
        public string Pergunta { get; set; }
        public string Resposta_Esperada { get; set; }
    }
}