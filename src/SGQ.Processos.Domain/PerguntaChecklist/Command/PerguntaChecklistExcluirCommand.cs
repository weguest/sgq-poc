﻿using MediatR;
using SGQ.Core.Domain.Command;

namespace SGQ.Processos.Domain.PerguntaChecklist.Command
{
    public class PerguntaChecklistExcluirCommand : IRequest<CommandResult<bool>>
    {
        public string Codigo { get; set; }
        public string ModificadoPor { get; set; }
    }
}