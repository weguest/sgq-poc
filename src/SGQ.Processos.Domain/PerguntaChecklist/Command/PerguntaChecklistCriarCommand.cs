﻿using MediatR;
using SGQ.Core.Domain.Command;
using SGQ.Processos.Domain.PerguntaChecklist.Entity;

namespace SGQ.Processos.Domain.PerguntaChecklist.Command
{
    public class PerguntaChecklistCriarCommand : IRequest<CommandResult<PerguntaChecklistEntity>>
    {
        public string CriadoPor { get; set; }
        public string Titulo { get; set; }
        public string Descricao { get; set; }
        public string Processo { get; set; }
        public string Pergunta { get; set; }
        public string Resposta_Esperada { get; set; }
    }
}