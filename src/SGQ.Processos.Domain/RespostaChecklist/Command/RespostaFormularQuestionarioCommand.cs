﻿using System;
using System.Collections.Generic;
using System.Text;
using MediatR;
using SGQ.Core.Domain.Command;
using SGQ.Core.Domain.Entity.Enum;

namespace SGQ.Processos.Domain.RespostaChecklist.Command
{
    public class RespostaFormularQuestionarioCommand : IRequest<CommandResult<bool>>
    {
        public string criadoPor { get; set; }
        public string Grupo { get; set; }
        public string Turno { get; set; }
        public string Processo { get; set; }
    }
}
