﻿using MediatR;
using SGQ.Core.Domain.Command;

namespace SGQ.Processos.Domain.RespostaChecklist.Command
{
    public class RespostaChecklistExcluirCommand : IRequest<CommandResult<bool>>
    {
        public string Codigo { get; set; }
        public string ModificadoPor { get; set; }
    }
}