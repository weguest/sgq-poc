﻿using System;
using System.Collections.Generic;
using System.Text;
using MediatR;
using SGQ.Core.Domain.Command;

namespace SGQ.Processos.Domain.RespostaChecklist.Command
{
    public class RespostaChecklistResponderQuestionarioCommand : IRequest<CommandResult<bool>>
    {
        public string criadoPor { get; set; }

        public string Processo { get; set; }

        public List<RespostaDTO> respostas { get; set; } = new List<RespostaDTO>();

    }

    public class RespostaDTO
    {
        public string codigo { get; set; }
        public string grupo { get; set; }
        public string turno { get; set; }
        public string resposta { get; set; }
    }  
}
