﻿using MediatR;
using SGQ.Core.Domain.Command;
using SGQ.Processos.Domain.RespostaChecklist.Entity;

namespace SGQ.Processos.Domain.RespostaChecklist.Command
{
    public class RespostaChecklistCriarCommand : IRequest<CommandResult<RespostaChecklistEntity>>
    {
        public string CriadoPor { get; set; }
        public string Grupo { get; set; }
        public string Turno { get; set; }
        public string Processo { get; set; }
        public string Pergunta { get; set; }
        public string Resposta_Esperada { get; set; }
        public string Resposta { get; set; }
    }
}