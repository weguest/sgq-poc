﻿using System;
using System.Collections.Generic;
using System.Text;
using SGQ.Core.Domain.Entity;
using SGQ.Core.Domain.Entity.Enum;

namespace SGQ.Processos.Domain.RespostaChecklist.Entity
{
    public class RespostaChecklistEntity : EntityBase
    {
        public string Processo { get; set; }
        public string Grupo { get; set; }
        public string Turno { get; set; }
        public string Pergunta { get; set; }
        public string Resposta_Esperada { get; set; }
        public string Resposta { get; set; }

        public RespostaChecklistEntity()
        {
            
        }

        public RespostaChecklistEntity(string criadopor, string processo, string grupo, string turno, string pergunta, string respostaEsperada, string resposta) : base(criadopor)
        {

            Codigo = GerarNumeroRandomico("RCK", 12);
            Processo = processo;
            Grupo = grupo;
            Turno = turno;
            Pergunta = pergunta;
            Resposta_Esperada = respostaEsperada;
            Resposta = resposta;


            if (string.IsNullOrWhiteSpace(turno))
                this.Erros.Add(new Erro("turno", "Você deve informar o turno."));

            if (string.IsNullOrWhiteSpace(grupo))
                this.Erros.Add(new Erro("grupo", "Você deve informar o grupo."));

            if (string.IsNullOrWhiteSpace(processo))
                this.Erros.Add(new Erro("processo", "Você deve informar o processo."));

            if (string.IsNullOrWhiteSpace(pergunta))
                this.Erros.Add(new Erro("pergunta", "Você deve informar uma pergunta."));

            if (string.IsNullOrWhiteSpace(respostaEsperada))
                this.Erros.Add(new Erro("respostaEsperada", "Você deve informar a resposta esperada."));

        }

        public void Alterar(string codigo, string modificadopor, EnumEstadoRegistro estado, string processo, string grupo, string turno, string pergunta, string respostaEsperada, string resposta)
        {
            base.Alterar(codigo, modificadopor, estado);

            Grupo = grupo;
            Turno = turno;
            Processo = processo;
            Pergunta = pergunta;
            Resposta_Esperada = respostaEsperada;
            Resposta = resposta;

            if (string.IsNullOrWhiteSpace(turno))
                this.Erros.Add(new Erro("turno", "Você deve informar o turno."));

            if (string.IsNullOrWhiteSpace(grupo))
                this.Erros.Add(new Erro("grupo", "Você deve informar o grupo."));

            if (string.IsNullOrWhiteSpace(processo))
                this.Erros.Add(new Erro("processo", "Você deve informar o processo."));

            if (string.IsNullOrWhiteSpace(pergunta))
                this.Erros.Add(new Erro("pergunta", "Você deve informar uma pergunta."));

            if (string.IsNullOrWhiteSpace(respostaEsperada))
                this.Erros.Add(new Erro("respostaEsperada", "Você deve informar a resposta esperada."));
        }

        public void Responder(string resposta)
        {
            Resposta = resposta;

            if (string.IsNullOrWhiteSpace(resposta))
                this.Erros.Add(new Erro("resposta", "Você deve informar a resposta."));
        }

        public bool GerarNaoCoformidade()
        {
            return Resposta != Resposta_Esperada;
        }


    }
}
