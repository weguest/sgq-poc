﻿using System;
using System.Collections.Generic;
using System.Text;
using SGQ.Core.Domain.Entity.Enum;
using SGQ.Core.Domain.Query;
using SGQ.Processos.Domain.Processo.Query;

namespace SGQ.Processos.Domain.RespostaChecklist.Query
{
    public class RespostaChecklistListarQueryFilter : IQueryFilter<RespostaChecklistListarQueryDTO>
    {
        public string codigo { get; set; }
        public string turno { get; set; }
        public string grupo { get; set; }
        public string processo { get; set; }
        public string pergunta { get; set; }
        public string resposta_esperada { get; set; }
        public string resposta { get; set; }
        public EnumEstadoRegistro? EstadoRegistro { get; set; } = EnumEstadoRegistro.Ativo;
    }
}
