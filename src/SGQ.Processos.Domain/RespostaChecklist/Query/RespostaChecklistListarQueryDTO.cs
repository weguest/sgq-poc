﻿namespace SGQ.Processos.Domain.RespostaChecklist.Query
{
    public class RespostaChecklistListarQueryDTO
    {
        public string codigo { get; set; }
        public string turno { get; set; }
        public string grupo { get; set; }
        public string processo { get; set; }
        public string pergunta { get; set; }
        public string resposta_esperada { get; set; }
        public string resposta { get; set; }
    }
}