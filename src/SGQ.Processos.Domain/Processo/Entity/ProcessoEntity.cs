﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using SGQ.Core.Domain.Entity;
using SGQ.Core.Domain.Entity.Enum;

namespace SGQ.Processos.Domain.Processo.Entity
{
    public class ProcessoEntity : EntityBase
    {
        public string Organismo { get; set; }
        public string Norma { get; set; }
        public string Titulo { get; set; }
        public string Descricao { get; set; }

        public ProcessoEntity()
        {

        }

        public ProcessoEntity(string criadopor, string organismo, string norma, string titulo, string descricao) : base(criadopor)
        {

            Codigo = GerarNumeroRandomico("PRC", 12);
            Titulo = titulo;
            Descricao = descricao;
            Norma = norma;
            Organismo = organismo;
            
            if (string.IsNullOrWhiteSpace(titulo))
                this.Erros.Add(new Erro("titulo", "Você deve informar o titulo."));

            if (string.IsNullOrWhiteSpace(descricao))
                this.Erros.Add(new Erro("descricao", "Você deve informar uma descrição."));

            if (string.IsNullOrWhiteSpace(norma))
                this.Erros.Add(new Erro("norma", "Você deve informar a norma."));

            if (string.IsNullOrWhiteSpace(descricao))
                this.Erros.Add(new Erro("organismo", "Você deve informar o organismo."));
        }

        public void Alterar(string codigo, string modificadopor, EnumEstadoRegistro estado, string titulo, string descricao, string organismo, string norma)
        {
            base.Alterar(codigo, modificadopor, estado);

            Titulo = titulo;
            Descricao = descricao;
            Norma = norma;
            Organismo = organismo;

            if (string.IsNullOrWhiteSpace(titulo))
                this.Erros.Add(new Erro("titulo", "Você deve informar o titulo."));

            if (string.IsNullOrWhiteSpace(descricao))
                this.Erros.Add(new Erro("descricao", "Você deve informar uma descrição."));

            if (string.IsNullOrWhiteSpace(norma))
                this.Erros.Add(new Erro("norma", "Você deve informar a norma."));

            if (string.IsNullOrWhiteSpace(descricao))
                this.Erros.Add(new Erro("organismo", "Você deve informar o organismo."));

        }
    }
}
