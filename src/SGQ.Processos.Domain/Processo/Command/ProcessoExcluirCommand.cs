﻿using System;
using System.Collections.Generic;
using System.Text;
using MediatR;
using SGQ.Core.Domain.Command;

namespace SGQ.Processos.Domain.Processo.Command
{
    public class ProcessoExcluirCommand : IRequest<CommandResult<bool>>
    {
        public string Codigo { get; set; }
        public string ModificadoPor { get; set; }
    }
}
