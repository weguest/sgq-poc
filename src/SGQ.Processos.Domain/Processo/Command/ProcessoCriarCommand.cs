﻿using MediatR;
using SGQ.Core.Domain.Command;
using SGQ.Processos.Domain.Processo.Entity;

namespace SGQ.Processos.Domain.Processo.Command
{
    public class ProcessoCriarCommand : IRequest<CommandResult<ProcessoEntity>>
    {
        public string CriadoPor { get; set; }
        public string Titulo { get; set; }
        public string Descricao { get; set; }
        public string Organismo { get; set; }
        public string Norma { get; set; }
    }
}
