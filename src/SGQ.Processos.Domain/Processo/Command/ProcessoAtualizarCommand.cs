﻿using MediatR;
using SGQ.Core.Domain.Command;
using SGQ.Core.Domain.Entity.Enum;
using System;

namespace SGQ.Processos.Domain.Processo.Command
{
    public class ProcessoAtualizarCommand : IRequest<CommandResult<bool>>
    {
        public string Codigo { get; set; }
        public EnumEstadoRegistro EstadoRegistro { get; set; }
        public DateTime ModificadoEm { get; set; } = DateTime.Now;
        public string ModificadoPor { get; set; }
        public string Titulo { get; set; }
        public string Descricao { get; set; }
        public string Organismo { get; set; }
        public string Norma { get; set; }
    }
}
