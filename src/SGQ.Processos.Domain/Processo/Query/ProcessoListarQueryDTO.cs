﻿using System;
using System.Collections.Generic;
using System.Text;
using SGQ.Core.Domain.Query;
using SGQ.Processos.Domain.Atividade.Entity;
using SGQ.Processos.Domain.Atividade.Query;
using SGQ.Processos.Domain.PerguntaChecklist.Entity;
using SGQ.Processos.Domain.PerguntaChecklist.Query;
using SGQ.Processos.Domain.RespostaChecklist.Entity;
using SGQ.Processos.Domain.RespostaChecklist.Query;

namespace SGQ.Processos.Domain.Processo.Query
{
    public class ProcessoListarQueryDTO : IQueryDTO
    {
        public string CriadoPor { get; set; }
        public string Codigo { get; set; }
        public string Titulo { get; set; }
        public string Descricao { get; set; }
        public string Organismo { get; set; }
        public string Norma { get; set; }
        public IEnumerable<AtividadeListarQueryDTO> Atividades { get; set; } = new List<AtividadeListarQueryDTO>();
        public IEnumerable<PerguntaChecklistListarQueryDTO> PerguntasChecklist { get; set; } = new List<PerguntaChecklistListarQueryDTO>();
        public IEnumerable<RespostaChecklistListarQueryDTO> RespostasChecklist { get; set; } = new List<RespostaChecklistListarQueryDTO>();
    }
}
