﻿using System;
using System.Collections.Generic;
using System.Text;
using SGQ.Core.Domain.Query;

namespace SGQ.Processos.Domain.Processo.Query
{
    public class ProcessoListarQueryFilter : IQueryFilter<ProcessoListarQueryDTO>
    {
        public string Codigo { get; set; }
        public string Titulo { get; set; }
        public string Descricao { get; set; }
        public string Processo { get; set; }
        public string Norma { get; set; }
    }
}
