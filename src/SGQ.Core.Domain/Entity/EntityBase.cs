﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using SGQ.Core.Domain.Entity.Enum;

namespace SGQ.Core.Domain.Entity
{
    public class EntityBase : Notificavel, IEntityBase
    {
        [Key]
        public string Codigo { get; set; }
        public string CriadoPor { get; set; }
        public DateTime? CriadoEm { get; set; }

        public string modificadoPor { get; set; }
        public DateTime? modificadoEm { get; set; }

        public int? EstadoRegistro { get; set; }

        [NotMapped]
        public EnumEstadoRegistro estado
        {
            get { return EstadoRegistro != null ? (EnumEstadoRegistro) EstadoRegistro : EnumEstadoRegistro.Ativo; }
        }

        #region Codigo

        public string GerarNumeroRandomico(string prefixo, int tamanho)
        {
            var hoje = DateTime.UtcNow;
            var ramdomnumber1 = new Random().Next(1000, 9999);
            var ramdomnumber2 = new Random().Next(1000, 9999);
            var ramdomnumber3 = new Random().Next(1000, 9999);
            return string.Format("{0}{1}-{2}-{3}-{4}", prefixo, String.Format("{0:yyyyMMddhhss}", hoje), ramdomnumber1, ramdomnumber2, ramdomnumber3);
        }

        #endregion

        public EntityBase() { }
        public EntityBase(string criadoPor)
        {
            this.CriadoEm = DateTime.UtcNow;
            this.CriadoPor = criadoPor;
            this.EstadoRegistro = EnumEstadoRegistro.Ativo.GetHashCode();

            if (criadoPor is null)
                this.Erros.Add(new Erro("criadoPor","Você deve informar o usuário de criação."));
        }
        public void Alterar(string codigo, string modificadoPor, EnumEstadoRegistro estadoRegistro)
        {
            this.Codigo = codigo;
            this.modificadoEm = DateTime.UtcNow;
            this.modificadoPor = modificadoPor;
            this.EstadoRegistro = estadoRegistro.GetHashCode();

            if (modificadoPor is null)
                this.Erros.Add(new Erro("modificadoPor", "Você deve informar o usuário de modificação."));

            if (estadoRegistro == null)
                this.Erros.Add(new Erro("estadoRegistro", "Você deve informar o estado do registro."));

        }


    }
}
