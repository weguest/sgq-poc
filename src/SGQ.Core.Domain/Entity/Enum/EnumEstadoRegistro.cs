﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SGQ.Core.Domain.Entity.Enum
{
    public enum EnumEstadoRegistro
    {
        Ativo = 1,
        Inativo = 2,
        Excluido = 3
    }
}
