﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using Newtonsoft.Json;

namespace SGQ.Core.Domain.Entity
{
    public abstract class Notificavel
    {
        #region Validacao
        [NotMapped]
        [JsonIgnore]
        public List<Erro> Erros { get; } = new List<Erro>();

        [NotMapped]
        [JsonIgnore]
        public bool Valido
        {
            get { return Erros.Count() == 0; }
        }

        [NotMapped]
        [JsonIgnore]
        public bool Invalido
        {
            get { return Erros.Any(); }
        }

        #endregion
    }

    public class Erro
    {

        public Erro(string field, string message)
        {
            Key = field;
            Message = message;
        }

        public string Key { get; set; }
        public string Message { get; set; }
    }
}
