﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SGQ.Core.Domain.Query
{
    public class QueryCommand<T>
    {
        public int pagina { get; set; } = 1;

        public int itemsPorPagina { get; set; } = 10;

        public int total { get; set; } = 0;

        public T data { get; set; }

        public Dictionary<object, object> filtros { get; set; }
    }
}
