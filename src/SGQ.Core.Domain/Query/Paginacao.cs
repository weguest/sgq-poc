﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SGQ.Core.Domain.Query
{
    public class Paginacao<T>
    {
        public int Pagina { get; set; } = 1;
        public int ItemsPorPagina { get; set; }
        public int TotalItems { get; set; }
        public string Ordem { get; set; } = "codigo";
        public string OrdemDirecao { get; set; } = "ASC";
        public T Dados { get; set; }
    }
}
