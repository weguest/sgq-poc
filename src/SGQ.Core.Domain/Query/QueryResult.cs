﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SGQ.Core.Domain.Query
{
    public class QueryResult<T>
    {
        public bool Sucesso { get; set; } = false;
        public Paginacao<T> Paginacao { get; set; } = new Paginacao<T>();
    }
}
