﻿using System;
using System.Collections.Generic;
using System.Text;
using SGQ.Core.Domain.Entity;

namespace SGQ.Core.Domain.Command
{
    public class CommandResult<T>
    {

        public static CommandResult<T> Erro(IList<Erro> erros, string msg="")
        {
            return new CommandResult<T>()
            {
                Erros = erros,
                Mensagem = msg,
                Valido = false,
                Retorno = default(T)
            };
        }

        public bool Valido { get; set; }
        public string Mensagem { get; set; }
        public IList<Erro> Erros { get; set; } = new List<Erro>();
        public T Retorno { get; set; }
    }
}
