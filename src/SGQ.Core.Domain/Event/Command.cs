﻿using System;
using System.Collections.Generic;
using System.Text;
using MediatR;

namespace SGQ.Core.Domain.Event
{
    public abstract class Command : Message
    {
        public DateTime Timestamp { get; protected set; }

        protected Command()
        {
            this.Timestamp = DateTime.Now;
        }
    }

    public abstract class Message : IRequest<bool>
    {
        public string MessageType { get; protected set; }

        protected Message()
        {
            this.MessageType = GetType().Name;
        }
    }
}
