﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SGQ.Core.Domain.Event
{
    public abstract class Event : BaseEvent
    {
        public DateTime TimeStamp { get; protected set; }

        protected Event()
        {
            this.TimeStamp = DateTime.Now;
        }
    }
}
