﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SGQ.Core.Domain.Event
{
    public abstract class BaseEvent
    {
        public string Codigo { get; set; }
        public string CriadoPor { get; set; }
        public DateTime? CriadoEm { get; set; }
        public string ModificadoPor { get; set; }
        public DateTime? ModificadoEm { get; set; }
        public int? EstadoRegistro { get; set; }
    }
}
