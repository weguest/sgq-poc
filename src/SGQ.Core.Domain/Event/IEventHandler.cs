﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SGQ.Core.Domain.Event
{
    public interface IEventHandler<in TEvent> : IEventHandler where TEvent : Event
    {
        Task Handler(TEvent @event);
    }

    public interface IEventHandler
    {
    }
}
