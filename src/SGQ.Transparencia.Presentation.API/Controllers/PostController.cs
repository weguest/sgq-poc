﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using SGQ.Core.Domain.Command;
using SGQ.Core.Domain.Entity;
using SGQ.Core.Infra.Repositories;
using SGQ.Transparencia.Domain.Post.Command;
using SGQ.Transparencia.Domain.Post.Query;
using SGQ.Transparencia.Infra;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace SGQ.Transparencia.Presentation.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PostController : ControllerBase
    {
        private readonly IMediator _mediator;
        private readonly IUnitOfWork _uow;
        private readonly IPostRepository _postRepository;

        protected string _userId
        {
            get { return User.FindFirstValue(ClaimTypes.NameIdentifier); }
        }
        public PostController(IMediator mediator, IUnitOfWork uow, IPostRepository postRepository)
        {
            _mediator = mediator;
            _uow = uow;
            _postRepository = postRepository;
            _uow.Open();
        }

        // GET api/values
        [HttpGet]
        public async Task<IActionResult> Get([FromQuery] PostListarQueryFilter filtro)
        {
            try
            {
                using (var transaction = _uow.Transaction)
                {
                    var retorno = await _postRepository.Listar(filtro);
                    return Ok(retorno);
                }
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }

        // GET api/values/5
        [HttpGet("{codigo}")]
        public async Task<IActionResult> Get(string codigo)
        {
            try
            {
                using (var transaction = _uow.Transaction)
                {
                    var retorno = await _postRepository.ObterPorCodigo(codigo);
                    return Ok(retorno);
                }
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }

        // POST api/values
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] PostCriarCommand value)
        {
            try
            {
                using (var transaction = _uow.Transaction)
                {
                    value.CriadoPor = _userId;
                    var result = await _mediator.Send(value);

                    if (result.Valido)
                    {
                        transaction.Commit();
                        return Ok(result);
                    }
                    else
                    {
                        transaction.Rollback();
                        return BadRequest(result);
                    }
                }
            }
            catch (Exception ex)
            {
                return BadRequest(new CommandResult<bool>() { Valido = false, Erros = new List<Erro>() { new Erro("Criar", ex.Message) } });
            }
        }

        // PUT api/values/5
        [HttpPut("{codigo}")]
        public async Task<IActionResult> Put(string codigo, [FromBody] PostAtualizarCommand value)
        {
            try
            {
                using (var transaction = _uow.Transaction)
                {
                    value.ModificadoPor = _userId;
                    var result = await _mediator.Send(value);

                    if (result.Valido)
                    {
                        transaction.Commit();
                        return Ok(result);
                    }
                    else
                    {
                        transaction.Rollback();
                        return BadRequest(result);
                    }
                }
            }
            catch (Exception ex)
            {
                return BadRequest(new CommandResult<bool>() { Valido = false, Erros = new List<Erro>() { new Erro("Criar", ex.Message) } });
            }
        }

        // DELETE api/values/5
        [HttpDelete("{codigo}")]
        public async Task<IActionResult> Delete(string codigo, [FromBody] PostExcluirCommand value)
        {
            try
            {
                using (var transaction = _uow.Transaction)
                {
                    value.ModificadoPor = _userId;
                    var result = await _mediator.Send(value);

                    if (result.Valido)
                    {
                        transaction.Commit();
                        return Ok(result);
                    }
                    else
                    {
                        transaction.Rollback();
                        return BadRequest(result);
                    }
                }
            }
            catch (Exception ex)
            {
                return BadRequest(new CommandResult<bool>() { Valido = false, Erros = new List<Erro>() { new Erro("Criar", ex.Message) } });
            }
        }

    }
}
