﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SGQ.Crosscutting.Identity;
using System;
using SGQ.Crosscutting.Swagger;
using System.Data;
using System.Data.SqlClient;
using SGQ.Core.Infra.Repositories;
using SGQ.Transparencia.Infra;
using MediatR;
using SGQ.Core.Domain.Event;
using SGQ.Infra.Bus;
using SGQ.Processos.Domain.RespostaChecklist.Events;
using SGQ.Transparencia.Application.Post.EventHandler;
using SGQ.Transparencia.Domain.Post.Events;

namespace SGQ.Transparencia.Presentation.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            #region EventBus
            //Domain Bus
            services.AddTransient<IEventBus, RabbitMQBus>(sp => {
                var scopeFactory = sp.GetRequiredService<IServiceScopeFactory>();
                return new RabbitMQBus(sp.GetService<IMediator>(), scopeFactory, Configuration);
            });

            //Subscriptions
            services.AddTransient<PostEventHandler>();

            //Domain Events
            services.AddTransient<IEventHandler<NaoConformidadeCriadaEvent>, PostEventHandler>();

            #endregion EventBus

            //Registrar Identity
            services.RegisterIdentityServices(Configuration);

            //Registrar Swagger
            services.RegisterSwaggerServices(Configuration, "Transparencia API", "v1.0");

            //Injetar Dependencias
            services.AddScoped<IDbConnection, SqlConnection>(opt => new SqlConnection(Configuration["SGQ-DATABASE:database"]));
            services.AddScoped<IUnitOfWork, UnitOfWork>();

            services.AddTransient<IPostRepository, PostRepository>();


            //Injetar MediaTR
            var assembly = AppDomain.CurrentDomain.Load("SGQ.Transparencia.Application");
            services.AddMediatR(assembly);

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.RegisterSwaggerApplication("API Transparencia v1.0");
            app.RegisterIdentityApplication(env);

            var eventBus = app.ApplicationServices.GetRequiredService<IEventBus>();
            eventBus.Subscribe<NaoConformidadeCriadaEvent, PostEventHandler>();

            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
