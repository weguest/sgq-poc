﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Caching.Distributed;
using Newtonsoft.Json;
using SGQ.Core.Domain.Entity;

namespace SGQ.Core.Infra.Repositories
{
    public class CacheRepository<T> where T : IEntityBase
    {
        private readonly IDistributedCache _distributedCache;

        public CacheRepository(IDistributedCache distributedCache)
        {
            _distributedCache = distributedCache;
        }

        public async Task AdicionarAsync(T entity)
        {
            var entrie = await _distributedCache.GetAsync(entity.Codigo);
            if (entrie != null) await _distributedCache.RemoveAsync(entity.Codigo);

            await _distributedCache.SetStringAsync(entity.Codigo, JsonConvert.SerializeObject(entity));
        }

        public async Task AtualizarAsync(T entity)
        {
            await _distributedCache.SetStringAsync(entity.Codigo, JsonConvert.SerializeObject(entity));
        }

        public async Task ExcluirAsync(string codigo)
        {
            await _distributedCache.RemoveAsync(codigo);
        }

        public async Task<T> ObterPorCodigo(string codigo)
        {
            var entrie = await _distributedCache.GetAsync(codigo);
            if (entrie is null) return default(T);
            var value = Encoding.UTF8.GetString(entrie);
            return JsonConvert.DeserializeObject<T>(value);
        }
    }
}
