﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace SGQ.Core.Infra.Repositories
{
    public class UnitOfWork : IUnitOfWork, IDisposable
    {
        private IDbConnection _connection;
        private IDbTransaction _transaction;
        private int _timeout;

        public UnitOfWork(IDbConnection connection)
        {
            _connection = connection;
        }

        public IDbConnection Connection {
            get
            {
                if(_connection.State == ConnectionState.Closed)
                    _connection.Open();
                return _connection;
            }
        }
        public IDbTransaction Transaction
        {
            get {
                _transaction = _transaction ?? _connection.BeginTransaction();
                return _transaction;
            }
        }
        public int Timeout
        {
            get { return _timeout; }
        }
        public void setTimeout(int minutes)
        {
            _timeout = minutes * 60;
        }

        public void Open()
        {
            if(_connection.State == ConnectionState.Closed)
                _connection.Open();;
        }

        public void Commit()
        {
            _transaction.Commit();
        }

        public void Rollback()
        {
            _transaction.Rollback();
        }

        public void Dispose()
        {
            if(_connection?.State == ConnectionState.Open)
                _connection.Close();

            _connection?.Dispose();
            _transaction?.Dispose();
        }
    }

    public interface IUnitOfWork
    {
        IDbConnection Connection { get;  }
        IDbTransaction Transaction { get; }
        int Timeout { get; }
        void setTimeout(int minutes);
        

        void Open();
        void Commit();
        void Rollback();
    }
}
