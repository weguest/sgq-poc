﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SGQ.Core.Domain.Entity;

namespace SGQ.Core.Infra.Repositories
{
    public interface IBaseRepository<TEntity>  where TEntity : class, IEntityBase
    {
        Task<TEntity> ObterPorCodigo(string codigo);
        Task<int> AdicionarAsync(TEntity entity);
        Task<int> AtualizarAsync(TEntity entity);
        Task<int> ExcluirAsync(string codigo);
    }
}
