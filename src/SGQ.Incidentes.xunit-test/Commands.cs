using MediatR;
using Microsoft.Extensions.DependencyInjection;
using SGQ.Crosscutting.Identity.Context;
using SGQ.Incidentes.Application.Incidente.Handler;
using SGQ.Incidentes.Domain.Incidente.Command;
using SGQ.Incidentes.Infra.Repositories;
using SGQ.Incidentes.xunit_test.Moks;
using System;
using System.Threading;
using Xunit;

namespace SGQ.Incidentes.xunit_test
{
    public class Commands
    {
        private CancellationToken cancellationToken = new CancellationToken();
        private ServiceCollection _serviceCollection;
        private ServiceProvider _buildServiceProvider;
        private IMediator _mediator;

        public Commands()
        {
            //Dependency Injection
            _serviceCollection = new ServiceCollection();

            //Mock
            _serviceCollection.AddTransient<IUserService, MockUserService>();
            _serviceCollection.AddTransient<IIncidentesRepository, MockIncidentesRepository>();

            //Handlers
            _serviceCollection.AddTransient<IncidenteCommandHandler>();

            //Mediator;
            var assembly = AppDomain.CurrentDomain.Load("SGQ.Incidentes.xunit-test");
            _serviceCollection.AddMediatR(assembly);

            _buildServiceProvider = _serviceCollection.BuildServiceProvider();
            _mediator = _buildServiceProvider.GetService<IMediator>();
        }
        
        [Theory]
        [InlineData("Titulo 01", "", "", false)]
        [InlineData("", "Descricao 01", "", false)]
        [InlineData("", "", "c5f39608-b9fa-40c9-9c86-f88521862c83", false)]
        [InlineData("Titulo 01", "Descricao 01", "INVALIDO", false)]
        public async void DeveSerIncidenteCriarInvalido(string titulo, string descricao, string criadopor, bool saida)
        {
            IncidenteCriarCommand command = new IncidenteCriarCommand();

            command.Titulo = titulo;
            command.Descricao = descricao;
            command.CriadoPor = criadopor;

            var handler = _buildServiceProvider.GetService<IncidenteCommandHandler>();
            var resultado = await handler.Handle(command, cancellationToken);

            Assert.Equal(saida, resultado.Valido);
        }

        [Theory]
        [InlineData("Titulo 01", "", "", false)]
        [InlineData("", "Descricao 01", "", false)]
        [InlineData("", "", "c5f39608-b9fa-40c9-9c86-f88521862c83", false)]
        [InlineData("Titulo 01", "Descricao 01", "INVALIDO", false)]
        public async void DeveSerIncidenteAtualizarComBaseNaTeoria(string titulo, string descricao, string criadopor, bool saida)
        {
            IncidenteAtualizarCommand command = new IncidenteAtualizarCommand();

            command.Titulo = titulo;
            command.Descricao = descricao;
            command.ModificadoPor = criadopor;

            var handler = _buildServiceProvider.GetService<IncidenteCommandHandler>();
            var resultado = await handler.Handle(command, cancellationToken);

            Assert.Equal(saida, resultado.Valido);
        }

        [Theory]
        [InlineData(1, "", "USUARIOVALIDO", false)]
        [InlineData(2, null, "USUARIOVALIDO", false)]
        [InlineData(3, "55551111122", "INVALIDO", false)]
        [InlineData(4, "5555", "USUARIOVALIDO", true)]
        public async void DeveSerIncidenteExcluirComBaseNaTeoriao(int testIndex, string codigo, string modificadopor, bool saida)
        {
            IncidenteExcluirCommand command = new IncidenteExcluirCommand();

            command.Codigo = codigo;
            command.ModificadoPor = modificadopor;

            var handler = _buildServiceProvider.GetService<IncidenteCommandHandler>();
            var resultado = await handler.Handle(command, cancellationToken);

            Assert.Equal(saida, resultado.Valido);
        }

    }
}
