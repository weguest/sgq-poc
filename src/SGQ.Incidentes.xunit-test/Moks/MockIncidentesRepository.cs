﻿using SGQ.Incidentes.Domain.Incidente.Entity;
using SGQ.Incidentes.Infra.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using SGQ.Incidentes.Domain.Incidente.Query;

namespace SGQ.Incidentes.xunit_test.Moks
{

    public class MockIncidentesRepository : IIncidentesRepository
    {
        public async Task<IncidenteEntity> ObterPorCodigo(string codigo)
        {
            if(string.IsNullOrWhiteSpace(codigo)) return null;
            return new IncidenteEntity(codigo,"titulo","lalaal");
        }

        public async Task<int> AdicionarAsync(IncidenteEntity entity)
        {
            return 1;
        }

        public async Task<int> AtualizarAsync(IncidenteEntity entity)
        {
            return 1;
        }

        public async Task<int> ExcluirAsync(string codigo)
        {
            return 1;
        }

        public async Task<IEnumerable<IncidenteListarQueryDTO>> Listar(IncidentesListarQueryFilter command)
        {
            return new List<IncidenteListarQueryDTO>();
        }

        public async Task<IEnumerable<IncidenteSumarizarQueryDTO>> SumarizarIncidentes()
        {
            return new List<IncidenteSumarizarQueryDTO>();
        }
    }
}
