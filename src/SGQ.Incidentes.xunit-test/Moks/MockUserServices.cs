﻿using System;
using System.Collections.Generic;
using System.Text;
using SGQ.Crosscutting.Identity.Context;
using SGQ.Crosscutting.Identity.Context.Models;
using SGQ.Crosscutting.Identity.Models;

namespace SGQ.Incidentes.xunit_test.Moks
{
    public class MockUserService : IUserService
    {
        public IdentityResult<AppUser> PorId(string id)
        {

            if(id == "INVALIDO")
            {
                return new IdentityResult<AppUser>() { Succeeded = false };
            }

            return new IdentityResult<AppUser>() { Succeeded = true, Errors = new List<string>(), Data = new AppUser() { } };
        }

        public IdentityResult<AppUser> Register(string login, string password, string role)
        {
            throw new NotImplementedException();
        }

        public IdentityResult<object> Login(string login, string password)
        {
            throw new NotImplementedException();
        }

        public IdentityResult<object> Funcao(string id, string funcao)
        {
            throw new NotImplementedException();
        }

        public IdentityResult<object> FuncaoListar(string id)
        {
            throw new NotImplementedException();
        }

        public void SeedAdmin()
        {
            throw new NotImplementedException();
        }

        public IdentityResult<object> FromCookie(string id)
        {
            throw new NotImplementedException();
        }
    }
}
