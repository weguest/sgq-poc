﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SGQ.Crosscutting.Identity.Context;
using SGQ.Presentation.Site.Models.Autenticacao;
using System.Threading.Tasks;
using DNTBreadCrumb.Core;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Identity;
using Newtonsoft.Json;
using SGQ.Crosscutting.Identity.Models;

namespace SGQ.Presentation.Site.Controllers
{
    [Area("Seguranca")]
    [BreadCrumb(Title = "Segurança", UseDefaultRouteUrl = true, Order = 0)]
    public class AutenticacaoController : BaseController
    {
        private readonly IUserService _userService;
        private readonly SignInManager<AppUser> _signInManager;

        public AutenticacaoController(IUserService userService, SignInManager<AppUser> signInManager) : base(userService)
        {
            _userService = userService;
            _signInManager = signInManager;
        }

        [AllowAnonymous]
        [HttpGet]
        public IActionResult Login()
        {
            return View(new LoginViewModel());
        }

        [AllowAnonymous]
        [HttpPost("Login")]
        public async Task<IActionResult> LoginPostAsync(LoginViewModel postLoginVM, string returnUrl = "")
        {
            if (ModelState.IsValid == false)
            {
                return View("Login", postLoginVM);
            }

            var autenticacao = _signInManager.PasswordSignInAsync(postLoginVM.Login, postLoginVM.Password, false, true).Result;

            if (autenticacao.Succeeded == false)
            {
                ModelState.AddModelError("NotFound", "Usuário não encontrado");
                return View("Login", postLoginVM);
            }
            else
            {
                var token = _userService.Login(postLoginVM.Login, postLoginVM.Password);
                HttpContext.Session.SetString("token", JsonConvert.SerializeObject(token));

                
                if ( !string.IsNullOrWhiteSpace(returnUrl) )
                    return Redirect(returnUrl);
                else
                    return RedirectToAction("Perfil", "Autenticacao");
            }
        }

        [Authorize]
        [HttpGet]
        public async Task<IActionResult> Logout()
        {
            await _signInManager.SignOutAsync();
            return LocalRedirect("/");
        }

        [Authorize]
        [HttpGet]
        public async Task<IActionResult> Perfil()
        {
            return View();
        }

        [AllowAnonymous]
        [HttpGet]
        public async Task<IActionResult> AcessoNegado()
        {
            return View();
        }
    }
}