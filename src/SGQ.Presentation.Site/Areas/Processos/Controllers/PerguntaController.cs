﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using SGQ.Core.Domain.Command;
using SGQ.Presentation.Site.Areas.Processos.Models;
using SGQ.Presentation.Site.Controllers;
using SGQ.Processos.Domain.Atividade.Command;
using SGQ.Processos.Domain.PerguntaChecklist.Command;
using System.Threading.Tasks;
using DNTBreadCrumb.Core;
using SGQ.Crosscutting.Identity.Context;

namespace SGQ.Presentation.Site.Areas.Processos.Controllers
{
    [Area("Processos")]
    [BreadCrumb(Title = "Pergunta", UseDefaultRouteUrl = true, Order = 0)]
    public class PerguntaController : BaseController
    {
        private readonly IMapper _mapper;
        private readonly IConfiguration _configuration;

        public PerguntaController(IUserService userService, IConfiguration configuration, IMapper mapper) : base(userService)
        {
            _mapper = mapper;
            _configuration = configuration;
        }

        public IActionResult Index()
        {
            return View();
        }

        #region Criar
        [HttpGet("Processos/{controller}/{action}/{codigo?}")]
        public IActionResult Criar(string codigo)
        {
            var pergunta = new PerguntaViewModel() { processo = codigo };
            return View("CriarEditar", pergunta);
        }


        [HttpPost("Processos/{controller}/{action}")]
        public async Task<IActionResult> Criar(PerguntaViewModel model)
        {
            var retorno = new CommandResult<PerguntaViewModel>();

            var command = _mapper.Map<PerguntaChecklistCriarCommand>(model);
            retorno = await PostAsync<CommandResult<PerguntaViewModel>>($"{_configuration["apis-endpoints:processos"]}api/processo/{model.processo}/PerguntaChecklist", command);

            if (retorno == null)
            {
                ModelState.AddModelError("Consulta", "Não foi possível acessar o recurso solicitado.");
                retorno = new CommandResult<PerguntaViewModel>();
            }

            if (!retorno.Valido)
            {
                foreach (var item in retorno.Erros)
                {
                    ModelState.AddModelError(item.Key, item.Message);
                }
            }
            else
            {
                ShowMessage(EnumMessageType.success, retorno.Retorno.codigo, "Codigo da atividade gerada", true);
                return RedirectToAction("Alterar", "Processo", new { area = "Processos", codigo = model.processo });
            }

            return View("CriarEditar", model);
        }
        #endregion

        #region Alterar
        [HttpGet("Processos/{controller}/{action}/{codigoFromRoute?}")]
        public async Task<IActionResult> Alterar(string codigoFromRoute)
        {
            var cod = codigoFromRoute.Split('_')[0];
            var prc = codigoFromRoute.Split('_')[1];

            var retorno = await GetAsync<PerguntaViewModel>($"{_configuration["apis-endpoints:processos"]}api/processo/{prc}/PerguntaChecklist/{cod}");

            if (retorno == null)
            {
                ModelState.AddModelError("Consulta", "Não foi possível acessar o recurso solicitado.");
                retorno = new PerguntaViewModel();
            }

            return View("CriarEditar", retorno);
        }

        [HttpPost("Processos/{controller}/{action}/{codigo?}")]
        public async Task<IActionResult> Alterar(PerguntaViewModel model)
        {

            var retorno = new CommandResult<bool>();

            var command = _mapper.Map<AtividadeAtualizarCommand>(model);
            retorno = await PutAsync<CommandResult<bool>>($"{_configuration["apis-endpoints:processos"]}api/processo/{model.processo}/PerguntaChecklist", command);

            if (retorno == null)
            {
                ModelState.AddModelError("Consulta", "Não foi possível acessar o recurso solicitado.");
                retorno = new CommandResult<bool>();
            }

            if (!retorno.Retorno)
            {
                foreach (var item in retorno.Erros)
                {
                    ModelState.AddModelError(item.Key, item.Message);
                }
            }
            else
            {
                ShowMessage(EnumMessageType.success, "Registro salvo com sucesso!");
                return RedirectToAction("Alterar", "Processo", new { area = "Processos", codigo = model.processo });
            }

            return View("CriarEditar", model);
        }
        #endregion
    }
}