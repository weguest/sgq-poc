﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DNTBreadCrumb.Core;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Configuration;
using SGQ.Core.Domain.Command;
using SGQ.Crosscutting.Identity.Context;
using SGQ.Incidentes.Domain.Incidente.Command;
using SGQ.Presentation.Site.Areas.Processos.Models;
using SGQ.Presentation.Site.Controllers;
using SGQ.Processos.Domain.Processo.Command;

namespace SGQ.Presentation.Site.Areas.Processos.Controllers
{
    [Authorize]
    [Area("Processos")]
    [BreadCrumb(Title = "Processo", UseDefaultRouteUrl = true, Order = 0)]
    public class ProcessoController : BaseController
    {
        private readonly IConfiguration _configuration;
        private readonly IMapper _mapper;

        public ProcessoController(IUserService userService, IConfiguration configuration, IMapper mapper) : base(userService)
        {
            _configuration = configuration;
            _mapper = mapper;
        }

        public async Task<IActionResult> Index()
        {
            var retorno = await GetAsync<List<ProcessoViewModel>>($"{_configuration["apis-endpoints:processos"]}api/processo");
            if (retorno == null)
            {
                ModelState.AddModelError("Consulta", "Não foi possível consulta o recurso solicitado.");
                retorno = new List<ProcessoViewModel>();
            }

            var model = new IndexProcessoViewModel();
            model.Lista = retorno;

            return View("Index", model);
        }

        [HttpPost("Processos/{controller}/{action}")]
        public async Task<IActionResult> Index(IndexProcessoViewModel model)
        {

            var retorno = await GetAsync<List<ProcessoViewModel>>($"{_configuration["apis-endpoints:processos"]}api/processo?" + model.Filtro.UrlEncodedContent());
            if (retorno == null)
            {
                ModelState.AddModelError("Consulta", "Não foi possível consultar o recurso selecionado.");
                retorno = new List<ProcessoViewModel>();
            }
            
            model.Lista = retorno;

            return View("Index", model);
        }


        [HttpGet]
        public IActionResult Criar()
        {
            var atividade = new ProcessoViewModel();
            return View("CriarEditar", atividade);
        }


        [HttpPost("Processos/{controller}/{action}")]
        public async Task<IActionResult> Criar(ProcessoViewModel model)
        {
            var retorno = new CommandResult<bool>();

            var command = _mapper.Map<ProcessoCriarCommand>(model);
            retorno = await PostAsync<CommandResult<bool>>($"{_configuration["apis-endpoints:processos"]}api/processo/", command);

            if (retorno == null)
            {
                ModelState.AddModelError("Consulta", "Não foi possível acessar o recurso solicitado.");
                retorno = new CommandResult<bool>();
            }

            if (!retorno.Retorno)
            {
                foreach (var item in retorno.Erros)
                {
                    ModelState.AddModelError(item.Key, item.Message);
                }
            }
            else
            {
                ShowMessage(EnumMessageType.success, "Registro salvo com sucesso!");
                return RedirectToAction("Criar", "Processo", new { area = "Processos" });
            }

            return View("CriarEditar", model);
        }



        //[HttpPost]
        //public async Task<IActionResult> Salvar([FromForm] ProcessoViewModel value)
        //{
        //    var retorno = new CommandResult<bool>();

        //    if (value.codigo is null)
        //    {
        //        var command = _mapper.Map<ProcessoCriarCommand>(value);
        //        retorno = await PostAsync<CommandResult<bool>>($"{_configuration["apis-endpoints:processos"]}api/processo", command);
        //    }
        //    else
        //    {
        //        var command = _mapper.Map<ProcessoAtualizarCommand>(value);
        //        retorno = await PutAsync<CommandResult<bool>>($"{_configuration["apis-endpoints:processos"]}api/processo/{value.codigo}", command);
        //    }

        //    if (retorno == null)
        //    {
        //        ModelState.AddModelError("Consulta", "Não foi possível acessar o recurso solicitado.");
        //        retorno = new CommandResult<bool>();
        //    }

        //    if (!retorno.Retorno)
        //    {
        //        foreach (var item in retorno.Erros)
        //        {
        //            ModelState.AddModelError(item.Key, item.Message);
        //        }
        //    }
        //    else
        //    {
        //        ShowMessage(EnumMessageType.success, "Registro salvo com sucesso!");
        //    }


        //    return RedirectToAction("Index");
        //}

        [HttpGet("Processos/{controller}/{action}/{codigo?}")]
        public async Task<IActionResult> Alterar(string codigo)
        {
            var retorno = await GetAsync<List<ProcessoViewModel>>($"{_configuration["apis-endpoints:processos"]}api/processo?codigo={codigo}");

            if (retorno == null || retorno.Count() == 0)
            {
                ModelState.AddModelError("Consulta", "Não foi possível acessar o recurso solicitado.");
                retorno = new List<ProcessoViewModel>();
                return RedirectToAction("Index");
            }

            if (retorno?.Any() == false)
            {
                ModelState.AddModelError("Consulta", "O recurso solicitado não existe.");
                retorno = new List<ProcessoViewModel>();
                return RedirectToAction("Index");
            }

            ViewBag.Grupos = retorno.FirstOrDefault()?.respostasChecklist.GroupBy(x=>x.grupo).Select(x => new SelectListItem(x.Key, x.Key)).ToList();
            ViewBag.Turnos = retorno.FirstOrDefault()?.respostasChecklist.GroupBy(x => x.turno).Select(x => new SelectListItem(x.Key, x.Key)).ToList();

            return View("CriarEditar", retorno.FirstOrDefault());
        }

        [HttpPost("Processos/{controller}/{action}/{codigo?}")]
        public async Task<IActionResult> Alterar(ProcessoViewModel model)
        {

            var retorno = new CommandResult<bool>();

            var command = _mapper.Map<ProcessoAtualizarCommand>(model);
            retorno = await PutAsync<CommandResult<bool>>($"{_configuration["apis-endpoints:processos"]}api/processo/", command);

            if (retorno == null)
            {
                ModelState.AddModelError("Consulta", "Não foi possível acessar o recurso solicitado.");
                retorno = new CommandResult<bool>();
            }

            if (!retorno.Retorno)
            {
                foreach (var item in retorno.Erros)
                {
                    ModelState.AddModelError(item.Key, item.Message);
                }
            }
            else
            {
                ShowMessage(EnumMessageType.success, "Registro salvo com sucesso!");
                return RedirectToAction("Alterar", "Processo", new { area = "Processos", codigo = model.codigo });
            }

            return View("CriarEditar", model);
        }

        [HttpGet("Processos/{controller}/{action}/{codigo?}")]
        public async Task<IActionResult> Excluir(string codigo)
        {

            var retorno = new CommandResult<bool>();

            retorno = await DeleteAsync<CommandResult<bool>>($"{_configuration["apis-endpoints:processos"]}api/processo/{codigo}");

            if (retorno == null)
            {
                ModelState.AddModelError("Consulta", "Não foi possível acessar o recurso solicitado.");
                retorno = new CommandResult<bool>();
            }

            if (!retorno.Retorno)
            {
                foreach (var item in retorno.Erros)
                {
                    ModelState.AddModelError(item.Key, item.Message);
                }
            }
            else
            {
                ShowMessage(EnumMessageType.success, "Registro excluido com sucesso!");
                return RedirectToAction("Index", "Processo", new { area = "Processos"});
            }

            return RedirectToAction("Index");
        }

    }
}