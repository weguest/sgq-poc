﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using SGQ.Core.Domain.Command;
using SGQ.Presentation.Site.Areas.Processos.Models;
using SGQ.Presentation.Site.Controllers;
using SGQ.Processos.Domain.Atividade.Command;
using SGQ.Processos.Domain.RespostaChecklist.Command;
using System.Threading.Tasks;
using DNTBreadCrumb.Core;
using SGQ.Crosscutting.Identity.Context;


namespace SGQ.Presentation.Site.Areas.Processos.Controllers
{
    [Area("Processos")]
    [BreadCrumb(Title = "Resposta", UseDefaultRouteUrl = true, Order = 0)]
    public class RespostaController : BaseController
    {
        private readonly IMapper _mapper;
        private readonly IConfiguration _configuration;

        public RespostaController(IUserService userService, IConfiguration configuration, IMapper mapper) : base(userService)
        {
            _mapper = mapper;
            _configuration = configuration;
        }

        public IActionResult Index()
        {
            return View();
        }

        #region Criar
        [HttpGet("Processos/{controller}/{action}/{codigo?}")]
        public IActionResult Criar(string codigo)
        {
            var atividade = new RespostaViewModel() { processo = codigo };
            return View("CriarEditar", atividade);
        }


        [HttpPost("Processos/{controller}/{action}")]
        public async Task<IActionResult> Criar(RespostaViewModel model)
        {
            var retorno = new CommandResult<RespostaViewModel>();

            var command = _mapper.Map<RespostaChecklistCriarCommand>(model);
            retorno = await PostAsync<CommandResult<RespostaViewModel>>($"{_configuration["apis-endpoints:processos"]}api/processo/{model.processo}/RespostaChecklist", command);

            if (retorno == null)
            {
                ModelState.AddModelError("Consulta", "Não foi possível acessar o recurso solicitado.");
                retorno = new CommandResult<RespostaViewModel>();
            }

            if (!retorno.Valido)
            {
                foreach (var item in retorno.Erros)
                {
                    ModelState.AddModelError(item.Key, item.Message);
                }
            }
            else
            {
                ShowMessage(EnumMessageType.success, retorno.Retorno.codigo, "Codigo da atividade gerada", true);
                return RedirectToAction("Alterar", "Processo", new { area = "Processos", codigo = model.processo });
            }

            return View("CriarEditar", model);
        }
        #endregion

        #region Alterar
        [HttpGet("Processos/{controller}/{action}/{codigoFromRoute?}")]
        public async Task<IActionResult> Alterar(string codigoFromRoute)
        {
            var cod = codigoFromRoute.Split('_')[0];
            var prc = codigoFromRoute.Split('_')[1];

            var retorno = await GetAsync<RespostaViewModel>($"{_configuration["apis-endpoints:processos"]}api/processo/{prc}/RespostaChecklist/{cod}");

            if (retorno == null)
            {
                ModelState.AddModelError("Consulta", "Não foi possível acessar o recurso solicitado.");
                retorno = new RespostaViewModel();
            }

            return View("CriarEditar", retorno);
        }

        [HttpPost("Processos/{controller}/{action}/{codigo?}")]
        public async Task<IActionResult> Alterar(RespostaViewModel model)
        {

            var retorno = new CommandResult<bool>();

            var command = _mapper.Map<RespostaChecklistAtualizarCommand>(model);
            retorno = await PutAsync<CommandResult<bool>>($"{_configuration["apis-endpoints:processos"]}api/processo/{model.processo}/RespostaChecklist", command);

            if (retorno == null)
            {
                ModelState.AddModelError("Consulta", "Não foi possível acessar o recurso solicitado.");
                retorno = new CommandResult<bool>();
            }

            if (!retorno.Retorno)
            {
                foreach (var item in retorno.Erros)
                {
                    ModelState.AddModelError(item.Key, item.Message);
                }
            }
            else
            {
                ShowMessage(EnumMessageType.success, "Registro salvo com sucesso!");
                return RedirectToAction("Alterar", "Processo", new { area = "Processos", codigo = model.processo });
            }

            return View("CriarEditar", model);
        }
        #endregion


        public async Task<IActionResult> FormularQuestionario(RespostaViewModel model)
        {
            ///api/Processo/{codigo}/RespostaChecklist/FormularQuestionario

            var command = _mapper.Map<RespostaFormularQuestionarioCommand>(model);
            var retorno = await PostAsync<CommandResult<bool>>($"{_configuration["apis-endpoints:processos"]}api/processo/{model.processo}/RespostaChecklist/FormularQuestionario", command);

            if (retorno == null)
            {
                ModelState.AddModelError("Consulta", "Não foi possível acessar o recurso solicitado.");
                retorno = new CommandResult<bool>();
            }

            if (!retorno.Retorno)
            {
                foreach (var item in retorno.Erros)
                {
                    ModelState.AddModelError(item.Key, item.Message);
                }
            }
            else
            {
                ShowMessage(EnumMessageType.success, "Registro salvo com sucesso!");
                return RedirectToAction("Alterar", "Processo", new { area = "Processos", codigo = model.processo });
            }

            return RedirectToAction("Alterar", "Processo", new { area = "Processos", codigo = model.processo });
        }

        public async Task<IActionResult> ResponderQuestionario([FromForm] RespostaChecklistResponderQuestionarioCommand model)
        {
            var command = _mapper.Map<RespostaChecklistResponderQuestionarioCommand>(model);
            var retorno = await PostAsync<CommandResult<bool>>($"{_configuration["apis-endpoints:processos"]}api/processo/{model.Processo}/RespostaChecklist/ResponderQuestionario", command);

            if (retorno == null)
            {
                ModelState.AddModelError("Consulta", "Não foi possível acessar o recurso solicitado.");
                retorno = new CommandResult<bool>();
            }

            if (!retorno.Retorno)
            {
                foreach (var item in retorno.Erros)
                {
                    ModelState.AddModelError(item.Key, item.Message);
                }
            }
            else
            {
                ShowMessage(EnumMessageType.success, "Registro salvo com sucesso!");
                return RedirectToAction("Alterar", "Processo", new { area = "Processos", codigo = model.Processo });
            }

            return RedirectToAction("Alterar", "Processo", new { area = "Processos", codigo = model.Processo });
        }

        ///RespostaChecklist/

        [HttpGet("Processos/{controller}/{action}")]
        public async Task<IActionResult> FiltrarRespostas([FromQuery]string processo, string grupo, string turno)
        {
            var retorno = await GetAsync<List<RespostaViewModel>>($"{_configuration["apis-endpoints:processos"]}api/Processo/{processo}/RespostaChecklist?turno={turno}&grupo={grupo}");
            if (retorno == null)
            {
                ModelState.AddModelError("Consulta", "Não foi possível acessar o recurso solicitado.");
                retorno = new List<RespostaViewModel>();
            }

            return View("_ListaRespostas", retorno);
        }

    }
}