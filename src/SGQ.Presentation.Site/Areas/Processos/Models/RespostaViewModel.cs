﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SGQ.Presentation.Site.Areas.Processos.Models
{
    public class RespostaViewModel
    {
        public string codigo { get; set; }
        public string processo { get; set; }
        public string grupo { get; set; }
        public string turno { get; set; }
        public string pergunta { get; set; }
        public string resposta_esperada { get; set; }
        public string resposta { get; set; }
        public int? estadoregistro { get; set; }
    }
}
