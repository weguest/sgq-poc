﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace SGQ.Presentation.Site.Areas.Processos.Models
{
    public class ProcessoViewModel
    {
        public string codigo { get; set; }
        public string criadopor { get; set; }
        public DateTime? criadoem { get; set; }
        public string modificadopor { get; set; }
        public DateTime? modificadoem { get; set; }
        public int? estadoregistro { get; set; }
        public string organismo { get; set; }
        public string norma { get; set; }
        public string titulo { get; set; }
        public string descricao { get; set; }
        public IEnumerable<AtividadeViewModel> atividades { get; set; } = new List<AtividadeViewModel>();
        public IEnumerable<PerguntaViewModel> perguntasChecklist { get; set; } = new List<PerguntaViewModel>();
        public IEnumerable<RespostaViewModel> respostasChecklist { get; set; } = new List<RespostaViewModel>();
    }

    public class IndexProcessoViewModel
    {
        public FiltroProcessoViewModel Filtro = new FiltroProcessoViewModel();
        public IEnumerable<ProcessoViewModel> Lista = new List<ProcessoViewModel>();
    }

    public class FiltroProcessoViewModel
    {
        public string codigo { get; set; }
        public string titulo { get; set; }
        public string descricao { get; set; }
        public string norma { get; set; }
        public string organismo { get; set; }
        public string estadoRegistro { get; set; } = "Ativo";

        public string UrlEncodedContent()
        {
            var keyValues = new List<KeyValuePair<string, string>>();
            keyValues.Add(new KeyValuePair<string, string>("codigo", codigo));
            keyValues.Add(new KeyValuePair<string, string>("titulo", titulo));
            keyValues.Add(new KeyValuePair<string, string>("descricao", descricao));
            keyValues.Add(new KeyValuePair<string, string>("norma", norma));
            keyValues.Add(new KeyValuePair<string, string>("organismo", organismo));
            keyValues.Add(new KeyValuePair<string, string>("estadoRegistro", estadoRegistro));

            return new FormUrlEncodedContent(keyValues).ReadAsStringAsync().Result;
        }
    }

}
