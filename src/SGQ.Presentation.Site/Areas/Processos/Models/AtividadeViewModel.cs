﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SGQ.Presentation.Site.Areas.Processos.Models
{
    public class AtividadeViewModel
    {
        public string codigo { get; set; }
        public string processo { get; set; }
        public string titulo { get; set; }
        public string descricao { get; set; }
        public string responsavel { get; set; }
        public int? estadoregistro { get; set; }
    }
}
