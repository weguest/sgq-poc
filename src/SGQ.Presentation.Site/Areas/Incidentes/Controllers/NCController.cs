﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Configuration;
using SGQ.Core.Domain.Command;
using SGQ.Incidentes.Domain.Incidente.Command;
using SGQ.Presentation.Site.Areas.Incidentes.Models;
using SGQ.Presentation.Site.Controllers;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DNTBreadCrumb.Core;
using Microsoft.AspNetCore.Authorization;
using SGQ.Crosscutting.Identity.Context;
using SGQ.Incidentes.Domain.NaoConformidade.Command;
using SGQ.Incidentes.Domain.NC.Command;

namespace SGQ.Presentation.Site.Areas.Incidentes.Controllers
{
    [Authorize]
    [Area("Incidentes")]
    [BreadCrumb(Title = "Não Conformidades", UseDefaultRouteUrl = true, Order = 0)]
    public class NCController : BaseController
    {
        private readonly IConfiguration _configuration;
        private readonly IMapper _mapper;

        public NCController(IUserService userService, IConfiguration configuration, IMapper mapper) : base(userService)
        {
            _configuration = configuration;
            _mapper = mapper;
        }
        

        [HttpGet]
        public async Task<IActionResult> Index()
        {
            var retorno = await GetAsync<List<NCViewModel>>($"{_configuration["apis-endpoints:incidentes"]}api/NaoConformidade");
            if (retorno == null)
            {
                ModelState.AddModelError("Consulta", "Não foi possível consultar as não conformidades.");
                retorno = new List<NCViewModel>();
            }

            var model = new IndexNCViewModel();
            model.Lista = retorno;

            return View("Index", model);
        }

        [HttpPost("Incidentes/{controller}/{action}")]
        public async Task<IActionResult> Index(IndexNCViewModel model)
        {

            var retorno = await GetAsync<List<NCViewModel>>($"{_configuration["apis-endpoints:incidentes"]}api/NaoConformidade?" + model.Filtro.UrlEncodedContent());
            if (retorno == null)
            {
                ModelState.AddModelError("Consulta", "Não foi possível consultar as não conformidades.");
                retorno = new List<NCViewModel>();
            }

            model.Lista = retorno;

            return View("Index", model);
        }

        [HttpGet]
        public IActionResult Criar()
        {
            return View("CriarEditar", new NCViewModel());
        }

        [HttpPost("Incidentes/{controller}/{action}")]
        public async Task<IActionResult> Criar([FromForm] NCViewModel value)
        {
            var retorno = new CommandResult<NCViewModel>();

            var command = _mapper.Map<NaoConformidadeCriarCommand>(value);
            retorno = await PostAsync<CommandResult<NCViewModel>>($"{_configuration["apis-endpoints:incidentes"]}api/NaoConformidade", command);
            
            if (retorno == null)
            {
                ModelState.AddModelError("Consulta", "Não foi possível acessar o recurso solicitado.");
                retorno = new CommandResult<NCViewModel>();
                retorno.Valido = false;
            }
            
            if (!retorno.Valido)
            {
                foreach (var item in retorno.Erros)
                {
                    ModelState.AddModelError(item.Key, item.Message);
                }
            }
            else
            {
                ShowMessage(EnumMessageType.success, retorno.Retorno.codigo, "Codigo da Não Conformidade.", true);
                return RedirectToAction("Index", "NC", new { area = "Incidentes" });
            }

            return View("CriarEditar", value);
        }

        [HttpGet("Incidentes/{controller}/{action}/{codigo?}")]
        public async Task<IActionResult> Alterar(string codigo)
        {
            var retorno = await GetAsync<NCViewModel>($"{_configuration["apis-endpoints:incidentes"]}api/NaoConformidade/{codigo}");

            if (retorno == null)
            {
                ModelState.AddModelError("Consulta", "Não foi possível acessar o recurso solicitado.");
                retorno = new NCViewModel();
            }

            return View("CriarEditar", retorno);
        }

        [HttpPost("Incidentes/{controller}/{action}")]
        public async Task<IActionResult> Alterar([FromForm] NCViewModel value)
        {
            var retorno = new CommandResult<NCViewModel>();

            var command = _mapper.Map<NaoConformidadeAtualizarCommand>(value);
            retorno = await PutAsync<CommandResult<NCViewModel>>($"{_configuration["apis-endpoints:incidentes"]}api/NaoConformidade/"+value.codigo, command);

            if (retorno == null)
            {
                ModelState.AddModelError("Consulta", "Não foi possível acessar o recurso solicitado.");
                retorno = new CommandResult<NCViewModel>();
                retorno.Valido = false;
            }

            if (!retorno.Valido)
            {
                foreach (var item in retorno.Erros)
                {
                    ModelState.AddModelError(item.Key, item.Message);
                }
            }
            else
            {
                ShowMessage(EnumMessageType.success, "Registro salvo com sucesso!");
                return RedirectToAction("Index", "NC", new { area = "Incidentes" });
            }

            return View("CriarEditar", value);
        }

        [HttpGet("Incidentes/{controller}/{action}/{codigo?}")]
        public async Task<IActionResult> Excluir([FromRoute] string codigo)
        {
            var retorno = await DeleteAsync<CommandResult<bool>>($"{_configuration["apis-endpoints:incidentes"]}api/NaoConformidade/{codigo}");
            if (retorno == null)
            {
                ModelState.AddModelError("Consulta", "Não foi possível acessar o recurso solicitado.");
                retorno = new CommandResult<bool>();
            }

            if (!retorno.Retorno)
            {
                foreach (var item in retorno.Erros)
                {
                    ModelState.AddModelError(item.Key, item.Message);
                }
            }
            else
            {
                ShowMessage(EnumMessageType.success, "Registro excluido com sucesso!");
            }

            return RedirectToAction("Index");
        }

        [HttpPost("Incidentes/{controller}/{action}/{codigo?}")]
        public async Task<IActionResult> Fechar([FromRoute] string codigo, [FromForm] NCViewModel value)
        {
            var command = _mapper.Map<NaoConformidadeFecharCommand>(value);
            var retorno = await PostAsync<CommandResult<NCViewModel>>($"{_configuration["apis-endpoints:incidentes"]}api/NaoConformidade/" + codigo + "/Fechar", command);

            if (retorno == null)
            {
                ModelState.AddModelError("Consulta", "Não foi possível acessar o recurso solicitado.");
                retorno = new CommandResult<NCViewModel>();
            }

            if (retorno.Valido == false)
            {
                foreach (var item in retorno.Erros)
                {
                    ModelState.AddModelError(item.Key, item.Message);
                }
            }
            else
            {
                ShowMessage(EnumMessageType.success, "Registro alterado com sucesso!");
                return RedirectToAction("Index");
            }

            return View("CriarEditar", value);
        }

    }

}