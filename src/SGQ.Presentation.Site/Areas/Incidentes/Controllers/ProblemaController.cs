﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Configuration;
using SGQ.Core.Domain.Command;
using SGQ.Incidentes.Domain.Incidente.Command;
using SGQ.Presentation.Site.Areas.Incidentes.Models;
using SGQ.Presentation.Site.Controllers;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DNTBreadCrumb.Core;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.Rendering;
using SGQ.Crosscutting.Identity.Context;
using SGQ.Incidentes.Domain.NC.Command;
using SGQ.Incidentes.Domain.Problema.Command;

namespace SGQ.Presentation.Site.Areas.Incidentes.Controllers
{
    [Authorize]
    [Area("Incidentes")]
    [BreadCrumb(Title = "Problema", UseDefaultRouteUrl = true, Order = 0)]
    public class ProblemaController : BaseController
    {
        private readonly IConfiguration _configuration;
        private readonly IMapper _mapper;

        public ProblemaController(IUserService userService, IConfiguration configuration, IMapper mapper) : base(userService)
        {
            _configuration = configuration;
            _mapper = mapper;
            
        }


        [HttpGet]
        public async Task<IActionResult> Index()
        {
            var retorno = await GetAsync<List<ProblemaViewModel>>($"{_configuration["apis-endpoints:incidentes"]}api/Problema");
            if (retorno == null)
            {
                ModelState.AddModelError("Consulta", "Não foi possível consultar as não conformidades.");
                retorno = new List<ProblemaViewModel>();
            }

            var retornoSumarizado = await GetAsync<List<SumarizadoViewModel>>($"{_configuration["apis-endpoints:incidentes"]}api/sumarizado/Problema");
            if (retornoSumarizado == null)
            {
                ModelState.AddModelError("Consulta", "Não foi possível consultar os incidentes.");
                retornoSumarizado = new List<SumarizadoViewModel>();
            }
            ViewBag.sumarizado = retornoSumarizado;

            var model = new IndexProblemaViewModel();
            model.Lista = retorno;
            
            return View("Index", model);
        }

        [HttpPost("Incidentes/{controller}/{action}")]
        public async Task<IActionResult> Index(IndexProblemaViewModel model)
        {

            var retorno = await GetAsync<List<ProblemaViewModel>>($"{_configuration["apis-endpoints:incidentes"]}api/Problema?" + model.Filtro.UrlEncodedContent());
            if (retorno == null)
            {
                ModelState.AddModelError("Consulta", "Não foi possível consultar as não conformidades.");
                retorno = new List<ProblemaViewModel>();
            }

            model.Lista = retorno;

            var retornoSumarizado = await GetAsync<List<SumarizadoViewModel>>($"{_configuration["apis-endpoints:incidentes"]}api/sumarizado/Problema");
            if (retornoSumarizado == null)
            {
                ModelState.AddModelError("Consulta", "Não foi possível consultar os incidentes.");
                retornoSumarizado = new List<SumarizadoViewModel>();
            }
            ViewBag.sumarizado = retornoSumarizado;

            return View("Index", model);
        }

        [HttpGet]
        public async Task<IActionResult> Criar()
        {
            var incidentes = await GetAsync<List<IncidenteViewModel>>($"{_configuration["apis-endpoints:incidentes"]}api/Incidente");
            if (incidentes is null) incidentes = new List<IncidenteViewModel>();
            ViewBag.Incidentes = incidentes.Select(x => new SelectListItem() { Text = x.titulo, Value =x.codigo});
            
            return View("CriarEditar", new ProblemaViewModel());
        }

        //[HttpPost]
        //public async Task<IActionResult> Salvar([FromForm] ProblemaViewModel value)
        //{
        //    var retorno = new CommandResult<ProblemaViewModel>();

        //    if (value.codigo is null)
        //    {
        //        var command = _mapper.Map<ProblemaCriarCommand>(value);
        //        retorno = await PostAsync<CommandResult<ProblemaViewModel>>($"{_configuration["apis-endpoints:incidentes"]}api/Problema", command);
        //    }
        //    else
        //    {
        //        var command = _mapper.Map<ProblemaAtualizarCommand>(value);
        //        retorno = await PutAsync<CommandResult<ProblemaViewModel>>($"{_configuration["apis-endpoints:incidentes"]}api/Problema/{value.codigo}", command);
        //    }

        //    if (retorno == null)
        //    {
        //        ModelState.AddModelError("Consulta", "Não foi possível acessar o recurso solicitado.");
        //        retorno = new CommandResult<ProblemaViewModel>();
        //        retorno.Valido = false;
        //    }

        //    if (!retorno.Valido)
        //    {
        //        foreach (var item in retorno.Erros)
        //        {
        //            ModelState.AddModelError(item.Key, item.Message);
        //        }
        //    }
        //    else
        //    {
        //        ShowMessage(EnumMessageType.success, "Registro salvo com sucesso!");
        //    }


        //    return RedirectToAction("Index");
        //}

        [HttpPost("Incidentes/{controller}/{action}")]
        public async Task<IActionResult> Criar([FromForm] ProblemaViewModel value)
        {
            var retorno = new CommandResult<ProblemaViewModel>();

            var command = _mapper.Map<ProblemaCriarCommand>(value);
            command.Incidentes = value.incidentes.Select(o => o.codigo).ToList();

            retorno = await PostAsync<CommandResult<ProblemaViewModel>>($"{_configuration["apis-endpoints:incidentes"]}api/Problema", command);

            if (retorno == null)
            {
                ModelState.AddModelError("Consulta", "Não foi possível acessar o recurso solicitado.");
                retorno = new CommandResult<ProblemaViewModel>();
                retorno.Valido = false;
            }

            if (!retorno.Valido)
            {
                foreach (var item in retorno.Erros)
                {
                    ModelState.AddModelError(item.Key, item.Message);
                }
            }
            else
            {
                ShowMessage(EnumMessageType.success, retorno.Retorno.codigo, "Codigo do Problema.", true);
                return RedirectToAction("Index", "Problema", new { area = "Incidentes" });
            }

            return View("CriarEditar", value);
        }

        [HttpGet("Incidentes/{controller}/{action}/{codigo?}")]
        public async Task<IActionResult> Alterar(string codigo)
        {
            var retorno = await GetAsync<List<ProblemaViewModel>>($"{_configuration["apis-endpoints:incidentes"]}api/Problema?codigo={codigo}");

            if (retorno == null)
            {
                ModelState.AddModelError("Consulta", "Não foi possível acessar o recurso solicitado.");
                retorno = new List<ProblemaViewModel>();
            }

            foreach (var item in retorno)
            {
                item.incidentes = await GetAsync<List<IncidenteViewModel>>($"{_configuration["apis-endpoints:incidentes"]}api/Problema/{item.codigo}/incidentes");
            }

            var incidentes = await GetAsync<List<IncidenteViewModel>>($"{_configuration["apis-endpoints:incidentes"]}api/Incidente");
            if (incidentes is null) incidentes = new List<IncidenteViewModel>();
            ViewBag.Incidentes = incidentes.Select(x => new SelectListItem() { Text = x.titulo, Value = x.codigo });

            return View("CriarEditar", retorno.FirstOrDefault());
        }

        [HttpPost("Incidentes/{controller}/{action}/{id?}")]
        public async Task<IActionResult> Alterar([FromRoute] string id,  [FromForm] ProblemaViewModel value)
        {
            var retorno = new CommandResult<ProblemaViewModel>();

            var command = _mapper.Map<ProblemaAtualizarCommand>(value);
            retorno = await PutAsync<CommandResult<ProblemaViewModel>>($"{_configuration["apis-endpoints:incidentes"]}api/Problema/" + value.codigo, command);

            if (retorno == null)
            {
                ModelState.AddModelError("Consulta", "Não foi possível acessar o recurso solicitado.");
                retorno = new CommandResult<ProblemaViewModel>();
                retorno.Valido = false;
            }

            if (!retorno.Valido)
            {
                foreach (var item in retorno.Erros)
                {
                    ModelState.AddModelError(item.Key, item.Message);
                }
            }
            else
            {
                ShowMessage(EnumMessageType.success, "Registro salvo com sucesso!");
                return RedirectToAction("Index", "Problema", new { area = "Incidentes" });
            }

            return View("CriarEditar", value);
        }

        [HttpGet("Incidentes/{controller}/{action}/{codigo?}")]
        public async Task<IActionResult> Excluir([FromRoute] string codigo)
        {
            var retorno = await DeleteAsync<CommandResult<bool>>($"{_configuration["apis-endpoints:incidentes"]}api/Problema/{codigo}");
            if (retorno == null)
            {
                ModelState.AddModelError("Consulta", "Não foi possível acessar o recurso solicitado.");
                retorno = new CommandResult<bool>();
            }

            if (!retorno.Retorno)
            {
                foreach (var item in retorno.Erros)
                {
                    ModelState.AddModelError(item.Key, item.Message);
                }
            }
            else
            {
                ShowMessage(EnumMessageType.success, "Registro excluido com sucesso!");
            }

            return RedirectToAction("Index");
        }

        [HttpPost("Incidentes/{controller}/{action}")]
        public async Task<IActionResult> Resolver([FromForm] ProblemaViewModel value)
        {
            var retorno = new CommandResult<bool>();
            var command = _mapper.Map<ProblemaResolverCommand>(value);
            retorno = await PostAsync<CommandResult<bool>>($"{_configuration["apis-endpoints:incidentes"]}api/Problema/{value.codigo}/Resolver", command);

            if (retorno == null)
            {
                ModelState.AddModelError("Consulta", "Não foi possível acessar o recurso solicitado.");
                retorno = new CommandResult<bool>();
            }

            if (retorno.Valido == false)
            {
                foreach (var item in retorno.Erros)
                {
                    ModelState.AddModelError(item.Key, item.Message);
                }
            }
            else
            {
                ShowMessage(EnumMessageType.success, "Incidente fechado com sucesso!");
                return RedirectToAction("Index");
            }


            return View("CriarEditar", value);

        }

    }

}