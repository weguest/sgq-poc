﻿using AutoMapper;
using DNTBreadCrumb.Core;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using SGQ.Core.Domain.Command;
using SGQ.Crosscutting.Identity.Context;
using SGQ.Incidentes.Domain.Incidente.Command;
using SGQ.Presentation.Site.Areas.Incidentes.Models;
using SGQ.Presentation.Site.Controllers;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SGQ.Presentation.Site.Areas.Incidentes.Controllers
{
    [Authorize]
    [Area("Incidentes")]
    [BreadCrumb(Title = "Incidentes", UseDefaultRouteUrl = true, Order = 0)]
    public class IncidenteController : BaseController
    {
        private readonly IConfiguration _configuration;
        private readonly IMapper _mapper;

        public IncidenteController(IUserService userService, IConfiguration configuration, IMapper mapper) : base(userService)
        {
            _configuration = configuration;
            _mapper = mapper;
        }

        [HttpGet]
        [BreadCrumb(Title = "Home", Order = 1)]
        public async Task<IActionResult> Home()
        {
            return View("Home");
        }


        [HttpGet]
        [BreadCrumb(Title = "Index", Order = 1)]
        public async Task<IActionResult> Index()
        {
            var retorno = await GetAsync<List<IncidenteViewModel>>($"{_configuration["apis-endpoints:incidentes"]}api/Incidente");
            if (retorno == null)
            {
                ModelState.AddModelError("Consulta", "Não foi possível consultar os incidentes.");
                retorno = new List<IncidenteViewModel>();
            }

            var model = new IndexIncidentesViewModel();
            model.Lista = retorno;

            var retornoSumarizado = await GetAsync<List<SumarizadoViewModel>>($"{_configuration["apis-endpoints:incidentes"]}api/sumarizado/Incidente");
            if (retornoSumarizado == null)
            {
                ModelState.AddModelError("Consulta", "Não foi possível consultar os incidentes.");
                retornoSumarizado = new List<SumarizadoViewModel>();
            }
            ViewBag.sumarizado = retornoSumarizado;

            return View("Index", model);
        }

        [HttpPost("Incidentes/{controller}/{action}")]
        public async Task<IActionResult> Index(IndexIncidentesViewModel model)
        {

            var retorno = await GetAsync<List<IncidenteViewModel>>($"{_configuration["apis-endpoints:incidentes"]}api/Incidente?" + model.Filtro.UrlEncodedContent());
            if (retorno == null)
            {
                ModelState.AddModelError("Consulta", "Não foi possível consultar os incidentes.");
                retorno = new List<IncidenteViewModel>();
            }

            model.Lista = retorno;

            var retornoSumarizado = await GetAsync<List<SumarizadoViewModel>>($"{_configuration["apis-endpoints:incidentes"]}api/sumarizado/Incidente");
            if (retornoSumarizado == null)
            {
                ModelState.AddModelError("Consulta", "Não foi possível consultar os incidentes.");
                retornoSumarizado = new List<SumarizadoViewModel>();
            }
            ViewBag.sumarizado = retornoSumarizado;

            return View("Index", model);
        }

        [HttpGet("Incidentes/{controller}/{action}")]
        [BreadCrumb(Title = "Criar", Order = 2)]
        public async Task<IActionResult> Criar()
        {
            return View("CriarEditar", new IncidenteViewModel());
        }


        [HttpPost("Incidentes/{controller}/{action}")]
        public async Task<IActionResult> Criar([FromForm] IncidenteViewModel value)
        {
            var retorno = new CommandResult<IncidenteViewModel>();
            var command = _mapper.Map<IncidenteCriarCommand>(value);
            retorno = await PostAsync<CommandResult<IncidenteViewModel>>($"{_configuration["apis-endpoints:incidentes"]}api/Incidente", command);

            if (retorno == null)
            {
                ModelState.AddModelError("Consulta", "Não foi possível acessar o recurso solicitado.");
                retorno = new CommandResult<IncidenteViewModel>();
            }

            if (retorno.Valido == false)
            {
                foreach (var item in retorno.Erros)
                {
                    ModelState.AddModelError(item.Key, item.Message);
                }
            }
            else
            {
                ShowMessage(EnumMessageType.success, retorno.Retorno.codigo, "Código do Incidente", true);
                return RedirectToAction("Index");
            }


            return View("CriarEditar", value);

        }

        [HttpGet("Incidentes/{controller}/{action}/{codigo?}")]
        [BreadCrumb(Title = "Alterar", Order = 2)]
        public async Task<IActionResult> Alterar(string codigo)
        {
            var retorno = await GetAsync<IncidenteViewModel>($"{_configuration["apis-endpoints:incidentes"]}api/Incidente/{codigo}");

            if (retorno == null)
            {
                ModelState.AddModelError("Consulta", "Não foi possível acessar o recurso solicitado.");
                retorno = new IncidenteViewModel();
            }
            
            return View("CriarEditar", retorno);
        }

        [HttpPost("Incidentes/{controller}/{action}/{codigo?}")]
        public async Task<IActionResult> Alterar(string codigo, [FromForm] IncidenteViewModel value)
        {
            var retorno = new CommandResult<IncidenteViewModel>();
            var command = _mapper.Map<IncidenteAtualizarCommand>(value);
            retorno = await PutAsync<CommandResult<IncidenteViewModel>>($"{_configuration["apis-endpoints:incidentes"]}api/Incidente/{value.codigo}", command);

            if (retorno == null)
            {
                ModelState.AddModelError("Consulta", "Não foi possível acessar o recurso solicitado.");
                retorno = new CommandResult<IncidenteViewModel>();
            }

            if (retorno.Valido == false)
            {
                foreach (var item in retorno.Erros)
                {
                    ModelState.AddModelError(item.Key, item.Message);
                }
            }
            else
            {
                ShowMessage(EnumMessageType.success, "Incidente atualizado com sucesso!");
                return RedirectToAction("Index");
            }


            return View("CriarEditar", value);

        }

        [HttpGet("Incidentes/{controller}/{action}/{codigo?}")]
        public async Task<IActionResult> Excluir([FromRoute] string codigo)
        {
            var retorno = await DeleteAsync<CommandResult<bool>>($"{_configuration["apis-endpoints:incidentes"]}api/Incidente/{codigo}");
            if (retorno == null)
            {
                ModelState.AddModelError("Consulta", "Não foi possível acessar o recurso solicitado.");
                retorno = new CommandResult<bool>();
            }

            if (!retorno.Retorno)
            {
                foreach (var item in retorno.Erros)
                {
                    ModelState.AddModelError(item.Key, item.Message);
                }
            }
            else
            {
                ShowMessage(EnumMessageType.success, "Registro excluido com sucesso!");
            }

            return RedirectToAction("Index");
        }

        [HttpPost("Incidentes/{controller}/{action}")]
        public async Task<IActionResult> FecharIncidente([FromForm] IncidenteViewModel value)
        {
            var retorno = new CommandResult<bool>();
            var command = _mapper.Map<IncidenteFecharCommand>(value);
            retorno = await PostAsync<CommandResult<bool>>($"{_configuration["apis-endpoints:incidentes"]}api/Incidente/{value.codigo}/Fechar", command);

            if (retorno == null)
            {
                ModelState.AddModelError("Consulta", "Não foi possível acessar o recurso solicitado.");
                retorno = new CommandResult<bool>();
            }

            if (retorno.Valido == false)
            {
                foreach (var item in retorno.Erros)
                {
                    ModelState.AddModelError(item.Key, item.Message);
                }
            }
            else
            {
                ShowMessage(EnumMessageType.success, "Incidente fechado com sucesso!");
                return RedirectToAction("Index");
            }


            return View("CriarEditar", value);

        }

    }

}