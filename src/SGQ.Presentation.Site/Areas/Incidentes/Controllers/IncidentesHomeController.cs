﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DNTBreadCrumb.Core;
using Microsoft.AspNetCore.Mvc;

namespace SGQ.Presentation.Site.Areas.Incidentes.Controllers
{
    [Area("Incidentes")]
    [BreadCrumb(Title = "Incidentes", UseDefaultRouteUrl = true, Order = 0)]
    public class IncidentesHomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
    }
}