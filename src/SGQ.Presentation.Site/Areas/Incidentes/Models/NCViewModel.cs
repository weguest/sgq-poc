﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Net.Http;

namespace SGQ.Presentation.Site.Areas.Incidentes.Models
{
    public class NCViewModel
    {
        /*
         * Usuário de Qualidade deve cadastrar não conformidades informando o
         * titulo, descrição, norma da não conformidade, processo a ela vinculado.
         */
        
        public string codigo { get; set; }
        public string estadoRegistro { get; set; } = "Ativo";
        public string titulo { get;set; }
        public string descricao { get;set; }
        public string norma { get;set; }
        public string processo { get;set; }
        public string status { get;set; } = "Aberto";
        public DateTime? fechadoem { get;set; }
        public string fechadopor { get;set; }

        [Display(Name = "Acoes Corretivas")]
        public string acoescorretivas { get;set; }

        [Display(Name = "Acoes Preventivas")]
        public string acoespreventivas { get;set; }
    }
    public class IndexNCViewModel
    {
        public FiltroNCViewModel Filtro { get; set; } = new FiltroNCViewModel();
        public List<NCViewModel> Lista { get; set; } = new List<NCViewModel>();
    }
    public class FiltroNCViewModel
    {
        public string codigo { get; set; }
        public string titulo { get; set; }
        public string descricao { get; set; }
        public string norma { get; set; }
        public string processo { get; set; }
        public string status { get; set; } = "Aberto";
        public string estadoRegistro { get; set; } = "Ativo";

        public string UrlEncodedContent()
        {
            var keyValues = new List<KeyValuePair<string, string>>();
            keyValues.Add(new KeyValuePair<string, string>("codigo", codigo));
            keyValues.Add(new KeyValuePair<string, string>("titulo", titulo));
            keyValues.Add(new KeyValuePair<string, string>("descricao", descricao));
            keyValues.Add(new KeyValuePair<string, string>("processo", processo));
            keyValues.Add(new KeyValuePair<string, string>("norma", norma));
            keyValues.Add(new KeyValuePair<string, string>("status", status));
            keyValues.Add(new KeyValuePair<string, string>("estadoRegistro", estadoRegistro));

            return new FormUrlEncodedContent(keyValues).ReadAsStringAsync().Result;
        }

    }
}
