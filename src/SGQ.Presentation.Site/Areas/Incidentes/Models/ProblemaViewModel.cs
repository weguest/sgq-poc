﻿using System.Collections.Generic;
using System.Net.Http;

namespace SGQ.Presentation.Site.Areas.Incidentes.Models
{
    public class ProblemaViewModel
    {
        /*
         * Usuário deve poder  inserir um problema informando o titulo,
         * descrição e incidente relacionado.
         * Os dados inclusos são consumidos pelo sistema de BI.
         */

        public string codigo { get; set; }
        public string titulo { get; set; }
        public string descricao { get; set; }
        public string status { get; set; } = "Aberto";
        public string solucao { get; set; }
        public string estadoRegistro { get; set; }
        public List<IncidenteViewModel> incidentes { get; set; } = new List<IncidenteViewModel>();
    }

    public class IndexProblemaViewModel
    {
        public FiltroProblemaViewModel Filtro { get; set; } = new FiltroProblemaViewModel();
        public List<ProblemaViewModel> Lista { get; set; } = new List<ProblemaViewModel>();
    }

    public class FiltroProblemaViewModel
    {
        public string codigo { get; set; }
        public string titulo { get; set; }
        public string descricao { get; set; }
        public string status { get; set; }
        public string estadoRegistro { get; set; } = "Ativo";

        public string UrlEncodedContent()
        {
            var keyValues = new List<KeyValuePair<string, string>>();
            keyValues.Add(new KeyValuePair<string, string>("codigo", codigo));
            keyValues.Add(new KeyValuePair<string, string>("titulo", titulo));
            keyValues.Add(new KeyValuePair<string, string>("descricao", descricao));
            keyValues.Add(new KeyValuePair<string, string>("status", descricao));
            keyValues.Add(new KeyValuePair<string, string>("estadoRegistro", estadoRegistro));

            return new FormUrlEncodedContent(keyValues).ReadAsStringAsync().Result;
        }
    }
}
