﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace SGQ.Presentation.Site.Areas.Incidentes.Models
{
    public class IncidenteViewModel
    {
        /*
         * Usuário deve poder  inserir um incidente ocorrido na producao, como parada de maquina,
         * produto defeituoso, falta de derminado insumo.
         * O usuário deve informar um titulo, descrição.
         * O dados inclusos são consumidos pelo módulo de BI.
        */

        public string codigo { get; set; }
        public string titulo { get; set; }
        public string descricao { get; set; }
        public string estadoRegistro { get; set; } 
        public string criadoPor { get; set; }
        public string status { get; set; } = "Aberto";
        public string solucao { get; set; }

        public override string ToString()
        {
            return codigo;
        }
    }

    public class SumarizadoViewModel
    {
        public string status { get; set; }
        public int count { get; set; }
    }

    public class IndexIncidentesViewModel
    {
        public FiltroIncidenteViewModel Filtro { get; set; } = new FiltroIncidenteViewModel();
        public List<IncidenteViewModel> Lista { get; set; } = new List<IncidenteViewModel>();
    }

    public class FiltroIncidenteViewModel
    {
        public string codigo { get; set; }
        public string status { get; set; } = "Aberto";
        public string titulo { get; set; }
        public string descricao { get; set; }
        public string estadoRegistro { get; set; } = "Ativo";

        public string UrlEncodedContent()
        {
            var keyValues = new List<KeyValuePair<string, string>>();
            keyValues.Add(new KeyValuePair<string, string>("codigo", codigo));
            keyValues.Add(new KeyValuePair<string, string>("titulo", titulo));
            keyValues.Add(new KeyValuePair<string, string>("descricao", descricao));
            keyValues.Add(new KeyValuePair<string, string>("estadoRegistro", estadoRegistro));
            keyValues.Add(new KeyValuePair<string, string>("status", status));

            return new FormUrlEncodedContent(keyValues).ReadAsStringAsync().Result;
        }
       
    }
}
