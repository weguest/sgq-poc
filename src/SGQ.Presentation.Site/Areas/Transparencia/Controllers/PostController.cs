﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DNTBreadCrumb.Core;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using SGQ.Core.Domain.Command;
using SGQ.Crosscutting.Identity.Context;
using SGQ.Presentation.Site.Areas.Transparencia.Models;
using SGQ.Presentation.Site.Controllers;
using SGQ.Transparencia.Domain.Post.Command;

namespace SGQ.Presentation.Site.Areas.Transparencia.Controllers
{
    [Area("Transparencia")]
    [BreadCrumb(Title = "Atividade", UseDefaultRouteUrl = true, Order = 0)]
    public class PostController : BaseController
    {

        private readonly IConfiguration _configuration;
        private readonly IMapper _mapper;

        public PostController(IUserService userService, IConfiguration configuration, IMapper mapper) : base(userService)
        {
            _configuration = configuration;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<IActionResult> Index()
        {
            var retorno = await GetAsync<List<PostViewModel>>($"{_configuration["apis-endpoints:transparencia"]}api/post");
            if (retorno == null)
            {
                ModelState.AddModelError("Consulta", "Não foi possível consulta o recurso solicitado.");
                retorno = new List<PostViewModel>();
            }

            var model = new IndexPostViewModel();
            model.Lista = retorno;

            return View("Index", model);
        }

        [HttpPost]
        public async Task<IActionResult> Index(IndexPostViewModel model)
        {
            var retorno = await GetAsync<List<PostViewModel>>($"{_configuration["apis-endpoints:transparencia"]}api/post?" + model.Filtro.UrlEncodedContent());
            if (retorno == null)
            {
                ModelState.AddModelError("Consulta", "Não foi possível consulta o recurso solicitado.");
                retorno = new List<PostViewModel>();
            }

            model.Lista = retorno;

            return View("Index", model);
        }

        [HttpGet]
        public async Task<IActionResult> Criar()
        {
            return View("CriarEditar", new PostViewModel());
        }

        [HttpPost]
        public async Task<IActionResult> Criar(PostViewModel model)
        {

            var retorno = new CommandResult<PostViewModel>();
            var command = _mapper.Map<PostCriarCommand>(model);
            retorno = await PostAsync<CommandResult<PostViewModel>>($"{_configuration["apis-endpoints:transparencia"]}api/Post", command);

            if (retorno == null)
            {
                ModelState.AddModelError("Consulta", "Não foi possível acessar o recurso solicitado.");
                retorno = new CommandResult<PostViewModel>();
            }

            if (retorno.Valido == false)
            {
                foreach (var item in retorno.Erros)
                {
                    ModelState.AddModelError(item.Key, item.Message);
                }
            }
            else
            {
                ShowMessage(EnumMessageType.success, retorno.Retorno.codigo, "Código da Postagem", true);
                return RedirectToAction("Index");
            }

            return View("CriarEditar", model);
        }

        [HttpGet("Transparencia/{controller}/{action}/{codigo}")]
        public async Task<IActionResult> Alterar(string codigo)
        {

            var retorno = await GetAsync<PostViewModel>($"{_configuration["apis-endpoints:transparencia"]}api/Post/{codigo}");

            if (retorno == null)
            {
                ModelState.AddModelError("Consulta", "Não foi possível acessar o recurso solicitado.");
                retorno = new PostViewModel();
            }

            return View("CriarEditar", retorno);

        }

        [HttpPost]
        public async Task<IActionResult> Alterar(PostViewModel model)
        {
            var retorno = new CommandResult<PostViewModel>();
            var command = _mapper.Map<PostAtualizarCommand>(model);
            retorno = await PutAsync<CommandResult<PostViewModel>>($"{_configuration["apis-endpoints:transparencia"]}api/Post/{model.codigo}", command);

            if (retorno == null)
            {
                ModelState.AddModelError("Consulta", "Não foi possível acessar o recurso solicitado.");
                retorno = new CommandResult<PostViewModel>();
            }

            if (retorno.Valido == false)
            {
                foreach (var item in retorno.Erros)
                {
                    ModelState.AddModelError(item.Key, item.Message);
                }
            }
            else
            {
                ShowMessage(EnumMessageType.success, "Postagem alterada com sucesso!");
                return RedirectToAction("Index");
            }

            return View("CriarEditar", model);
        }

        [AllowAnonymous]
        [HttpGet("Transparencia/{controller}/{action}/feed.{format}"), FormatFilter]
        public async Task<IActionResult> Feeds()
        {
            
            var feed = new rss();
            feed.channel = new rssChannel();
            feed.channel.title = "Divulgação e Transparência";
            feed.channel.description = "Divulgação e Transparência";
           
            var retorno = await GetAsync<List<PostViewModel>>($"{_configuration["apis-endpoints:transparencia"]}api/post?publico=true");
            if (retorno == null)
            {
                ModelState.AddModelError("Consulta", "Não foi possível consulta o recurso solicitado.");
                retorno = new List<PostViewModel>();
            }

            //List<feedEntry> entries = new List<feedEntry>();
            var items = new List<rssChannelItem>();
            foreach (var item in retorno)
            {
                items.Add(new rssChannelItem()
                {
                    title = item.titulo,
                    creator = "Editor",
                    description = item.descricao,
                    category= new string[] { "Transparencia" },
                    pubDate = item.modificadoEm.ToString(),
                    content = new content() { medium=item.conteudo, url= $"http://localhost:44322/Transparencia/Post/visualizar?codigo={item.codigo}" },
                    guid = new rssChannelItemGuid() { isPermaLink=true, Value="" },
                    link = $"http://localhost:44322/Transparencia/Post/view?codigo={item.codigo}"
                });
            }

            feed.channel.item = items.ToArray();

            return Ok(feed);
        }

        [AllowAnonymous]
        public async Task<IActionResult> Listar()
        {
            var retorno = await GetAsync<List<PostViewModel>>($"{_configuration["apis-endpoints:transparencia"]}api/Post?publico=true");

            if (retorno == null)
            {
                ModelState.AddModelError("Consulta", "Não foi possível acessar o recurso solicitado.");
                retorno = new List<PostViewModel>();
            }

            return View(retorno);
        }

        [AllowAnonymous]
        [HttpGet("Transparencia/{controller}/{action}/{codigo}")]
        public async Task<IActionResult> Visualizar(string codigo)
        {
            var retorno = await GetAsync<PostViewModel>($"{_configuration["apis-endpoints:transparencia"]}api/Post/{codigo}");

            if (retorno == null)
            {
                ModelState.AddModelError("Consulta", "Não foi possível acessar o recurso solicitado.");
                retorno = new PostViewModel();
            }

            return View(retorno);
        }
    }
}