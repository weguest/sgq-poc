﻿using System;
using System.Collections.Generic;
using System.Net.Http;

namespace SGQ.Presentation.Site.Areas.Transparencia.Models
{
    public class PostViewModel
    {
        public string codigo { get; set; }
        public string titulo { get; set; }
        public string descricao { get; set; }
        public string conteudo { get; set; } = "";
        public bool publico { get; set; } = false;
        public string estadoRegistro { get; set; }
        public DateTime? modificadoEm { get; set; }
    }

    public class IndexPostViewModel
    {
        public FiltroPostViewModel Filtro { get; set; } = new FiltroPostViewModel();
        public List<PostViewModel> Lista { get; set; } = new List<PostViewModel>();
    }

    public class FiltroPostViewModel
    {
        public string codigo { get; set; }
        public string titulo { get; set; }
        public string descricao { get; set; }
        public string conteudo { get; set; }
        public bool publico { get; set; } = false;
        public string estadoRegistro { get; set; } = "Ativo";

        public string UrlEncodedContent()
        {
            var keyValues = new List<KeyValuePair<string, string>>();
            keyValues.Add(new KeyValuePair<string, string>("codigo", codigo));
            keyValues.Add(new KeyValuePair<string, string>("titulo", titulo));
            keyValues.Add(new KeyValuePair<string, string>("descricao", descricao));
            keyValues.Add(new KeyValuePair<string, string>("conteudo", conteudo ));
            keyValues.Add(new KeyValuePair<string, string>("publico", publico.ToString()));

            return new FormUrlEncodedContent(keyValues).ReadAsStringAsync().Result;
        }
    }
}