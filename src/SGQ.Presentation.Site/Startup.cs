﻿using System.Linq;
using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SGQ.Crosscutting.Identity;
using SGQ.Incidentes.Domain.Incidente.Command;
using SGQ.Incidentes.Domain.NaoConformidade.Command;
using SGQ.Incidentes.Domain.NC.Command;
using SGQ.Incidentes.Domain.Problema.Command;
using SGQ.Presentation.Site.Areas.Incidentes.Models;
using SGQ.Presentation.Site.Areas.Processos.Models;
using SGQ.Presentation.Site.Areas.Transparencia.Models;
using SGQ.Processos.Domain.Atividade.Command;
using SGQ.Processos.Domain.PerguntaChecklist.Command;
using SGQ.Processos.Domain.Processo.Command;
using SGQ.Processos.Domain.RespostaChecklist.Command;
using SGQ.Transparencia.Domain.Post.Command;
using System.Net.Http.Headers;

namespace SGQ.Presentation.Site
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddMvc(options =>
            {
                options.FormatterMappings.SetMediaTypeMappingForFormat("xml", "application/xml");
                options.FormatterMappings.SetMediaTypeMappingForFormat("config", "application/xml");
                options.FormatterMappings.SetMediaTypeMappingForFormat("js", "application/json");

            }).SetCompatibilityVersion(CompatibilityVersion.Version_2_2).AddXmlSerializerFormatters();

            services.AddDistributedMemoryCache(); // Adds a default in-memory implementation of IDistributedCache
            services.AddSession();

            services.RegisterIdentityServiceUsingCookies(Configuration);

            //Automapper
            var config = new AutoMapper.MapperConfiguration(cfg =>
            {
                cfg.CreateMap<IncidenteViewModel, IncidenteCriarCommand>().ReverseMap();
                cfg.CreateMap<IncidenteViewModel, IncidenteAtualizarCommand>().ReverseMap();
                cfg.CreateMap<IncidenteViewModel, IncidenteExcluirCommand>().ReverseMap();
                cfg.CreateMap<IncidenteViewModel, IncidenteFecharCommand>().ReverseMap();

                cfg.CreateMap<NCViewModel, NaoConformidadeCriarCommand>().ReverseMap();
                cfg.CreateMap<NCViewModel, NaoConformidadeAtualizarCommand>().ReverseMap();
                cfg.CreateMap<NCViewModel, NaoConformidadeExcluirCommand>().ReverseMap();
                cfg.CreateMap<NCViewModel, NaoConformidadeFecharCommand>().ReverseMap();

                cfg.CreateMap<ProblemaViewModel, ProblemaCriarCommand>()
                    .ForMember(
                        dest => dest.Incidentes,
                        opt => opt.MapFrom
                        (
                            src => src.incidentes.Select(x => x.codigo).ToList()
                        ));

                cfg.CreateMap<ProblemaCriarCommand, ProblemaViewModel>()
                    .ForMember(dest => dest.incidentes, m => m.MapFrom(src => src.Incidentes.Select(x => new IncidenteViewModel(){ codigo = x}) ));

                cfg.CreateMap<ProblemaViewModel, ProblemaAtualizarCommand>().ReverseMap();
                cfg.CreateMap<ProblemaViewModel, ProblemaExcluirCommand>().ReverseMap();
                cfg.CreateMap<ProblemaViewModel, ProblemaResolverCommand>().ReverseMap();

                cfg.CreateMap<ProcessoViewModel, ProcessoCriarCommand>().ReverseMap();
                cfg.CreateMap<ProcessoViewModel, ProcessoAtualizarCommand>().ReverseMap();
                cfg.CreateMap<ProcessoViewModel, ProcessoExcluirCommand>().ReverseMap();

                cfg.CreateMap<AtividadeViewModel, AtividadeCriarCommand>().ReverseMap();
                cfg.CreateMap<AtividadeViewModel, AtividadeAtualizarCommand>().ReverseMap();
                cfg.CreateMap<AtividadeViewModel, AtividadeExcluirCommand>().ReverseMap();

                cfg.CreateMap<PerguntaViewModel, PerguntaChecklistCriarCommand>().ReverseMap();
                cfg.CreateMap<PerguntaViewModel, PerguntaChecklistAtualizarCommand>().ReverseMap();
                cfg.CreateMap<PerguntaViewModel, PerguntaChecklistExcluirCommand>().ReverseMap();

                cfg.CreateMap<RespostaViewModel, RespostaChecklistCriarCommand>().ReverseMap();
                cfg.CreateMap<RespostaViewModel, RespostaChecklistAtualizarCommand>().ReverseMap();
                cfg.CreateMap<RespostaViewModel, RespostaChecklistExcluirCommand>().ReverseMap();
                cfg.CreateMap<RespostaViewModel, RespostaFormularQuestionarioCommand>().ReverseMap();

                cfg.CreateMap<PostViewModel, PostCriarCommand>().ReverseMap();
                cfg.CreateMap<PostViewModel, PostAtualizarCommand>().ReverseMap();
                cfg.CreateMap<PostViewModel, PostExcluirCommand>().ReverseMap();

            });
            IMapper mapper = config.CreateMapper();
            services.AddSingleton(mapper);

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();

            app.UseSession();

            app.RegisterIdentityApplication(env);

            app.UseMvc(routes =>
            {
                routes.MapAreaRoute("incidentes_route", "Incidentes", "Incidentes/{controller=Incidente}/{action=Home}/{id?}");
                routes.MapAreaRoute("processos_route", "Processos", "Processos/{controller}/{action}/{id?}");
                routes.MapAreaRoute("seguranca_route", "Seguranca", "Seguranca/{controller}/{action}/{id?}");
                routes.MapAreaRoute("transparencia_route", "Transparencia", "Transparencia/{controller}/{action}/{id?}");

                routes.MapRoute(name: "default", template: "{controller=Home}/{action=Index}/{id?}");

            });
        }
    }
}
