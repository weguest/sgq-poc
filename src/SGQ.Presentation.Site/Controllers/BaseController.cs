﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc.Filters;
using projeto.identity.infra.identity.Services;
using SGQ.Crosscutting.Identity.Context;

namespace SGQ.Presentation.Site.Controllers
{
    public class BaseController : Controller
    {
        private readonly IUserService _userService;

        private static HttpClient Client = new HttpClient();

        public BaseController(IUserService userService)
        {
            _userService = userService;
        }

        public override void OnActionExecuted(ActionExecutedContext context)
        {
            if (!ModelState.IsValid)
            {
                var items = ModelState.Values.Where(x => x.Errors.Count > 0).Select(x => x.Errors.FirstOrDefault()?.ErrorMessage).ToList();

                foreach (var item in items)
                {
                    ShowMessage(EnumMessageType.danger, item);
                }

            }

            ViewBag.token = Token();

            base.OnActionExecuted(context);
        }

        protected string UsuarioLogadoId()
        {
            var userId = this.User.FindFirstValue(ClaimTypes.NameIdentifier);
            return userId;
        }

        protected string Token()
        {

            if (UsuarioLogadoId() is null) return null;

            var data = HttpContext.Session.GetString("token");

            if (data is null)
            {
                var usuario = _userService.FromCookie(UsuarioLogadoId());
                data = JsonConvert.SerializeObject(usuario);
                HttpContext.Session.SetString("token", data);
            }
            
            var tokenObj = JsonConvert.DeserializeObject<dynamic>(data);
            return tokenObj["Succeeded"].Value ? tokenObj.Data.token : null;
        }



        protected async Task<T> GetAsync<T>(string uri)
        {
            try
            {

                Client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                Client.DefaultRequestHeaders.Authorization =
                    new AuthenticationHeaderValue("Bearer", Token());

                using (HttpResponseMessage response = await Client.GetAsync(uri))
                {
                    //response.EnsureSuccessStatusCode();
                    string responseBody = await response.Content.ReadAsStringAsync();

                    return JsonConvert.DeserializeObject<T>(responseBody);
                }

            }
            catch (Exception ex)
            {
                //_logger.LogError(ex, ex.Message, uri);
            }

            return default(T);
        }

        protected async Task<T> PostAsync<T>(string uri, object parameters)
        {
            try
            {

                Client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                Client.DefaultRequestHeaders.Authorization =
                        new AuthenticationHeaderValue("Bearer", Token());

                using (HttpResponseMessage response = await Client.PostAsync(uri, new StringContent(JsonConvert.SerializeObject(parameters), Encoding.UTF8, "application/json")))
                {
                    //response.EnsureSuccessStatusCode();
                    string responseBody = await response.Content.ReadAsStringAsync();

                    return JsonConvert.DeserializeObject<T>(responseBody);
                }

            }
            catch (Exception ex)
            {
                //_logger.LogError(ex, ex.Message, uri);
            }

            return default(T);
        }

        protected async Task<T> DeleteAsync<T>(string uri)
        {
            try
            {

                Client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                Client.DefaultRequestHeaders.Authorization =
                        new AuthenticationHeaderValue("Bearer", Token());

                using (HttpResponseMessage response = await Client.DeleteAsync(uri))
                {
                    //response.EnsureSuccessStatusCode();
                    string responseBody = await response.Content.ReadAsStringAsync();

                    return JsonConvert.DeserializeObject<T>(responseBody);
                }

            }
            catch (Exception ex)
            {
                //_logger.LogError(ex, ex.Message, uri);
            }

            return default(T);
        }

        protected async Task<T> PutAsync<T>(string uri, object parameters)
        {
            try
            {

                Client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                Client.DefaultRequestHeaders.Authorization =
                        new AuthenticationHeaderValue("Bearer", Token());

                using (HttpResponseMessage response = await Client.PutAsync(uri, new StringContent(JsonConvert.SerializeObject(parameters), Encoding.UTF8, "application/json")))
                {
                    //response.EnsureSuccessStatusCode();
                    string responseBody = await response.Content.ReadAsStringAsync();
                    return JsonConvert.DeserializeObject<T>(responseBody);
                }

            }
            catch (Exception ex)
            {
                //_logger.LogError(ex, ex.Message, uri);
            }

            return default(T);
        }


        public void ShowMessage(EnumMessageType type, string message, string submessage = "", bool showonmodal = false)
        {

            var destino = showonmodal ? "ModalMessages" : "Messages";

            if (TempData.ContainsKey(destino))
            {
                //((List<dynamic>)TempData["Messages"]).Add( new { type=type.ToString(), message=message } );

                List<dynamic> messages = JsonConvert.DeserializeObject<List<dynamic>>(TempData[destino].ToString());
                messages.Add(new { type = type.ToString(), message = message, submessage = submessage });
                TempData[destino] = JsonConvert.SerializeObject(messages);

            }
            else
            {
                var m = new List<dynamic>();
                m.Add(new { type = type.ToString(), message = message, submessage = submessage });
                TempData[destino] = JsonConvert.SerializeObject(m);
            }
        }

    }

    public enum EnumMessageType
    {
        primary = 0,
        secondary = 1,
        success = 2,
        danger = 3,
        warning = 4,
        info = 5,
        light = 6,
        dark = 7
    }
}
