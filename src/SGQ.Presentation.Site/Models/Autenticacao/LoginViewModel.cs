﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SGQ.Presentation.Site.Models.Autenticacao
{
    public class LoginViewModel
    {
        [DisplayName("Login")]
        [Required(ErrorMessage = "O campo {0} é de preenchimento obrigatório.")]
        public string Login { get; set; }

        [DisplayName("Senha")]
        [Required(ErrorMessage = "O campo {0} é de preenchimento obrigatório.")]
        public string Password { get; set; }
    }
}
