﻿using MediatR;
using SGQ.Core.Domain.Command;

namespace SGQ.Transparencia.Domain.Post.Command
{
    public class PostExcluirCommand : IRequest<CommandResult<bool>>
    {
        public string Codigo { get; set; }
        public string ModificadoPor { get; set; }
    }
}