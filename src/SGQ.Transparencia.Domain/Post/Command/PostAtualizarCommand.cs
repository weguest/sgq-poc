﻿using System;
using MediatR;
using SGQ.Core.Domain.Command;
using SGQ.Core.Domain.Entity.Enum;
using SGQ.Transparencia.Domain.Post.Entity;

namespace SGQ.Transparencia.Domain.Post.Command
{
    public class PostAtualizarCommand : IRequest<CommandResult<PostEntity>>
    {
        public string Codigo { get; set; }
        public EnumEstadoRegistro EstadoRegistro { get; set; }
        public DateTime ModificadoEm { get; set; } = DateTime.Now;
        public string ModificadoPor { get; set; }
        public string Titulo { get; set; }
        public string Descricao { get; set; }
        public string Conteudo { get; set; }
        public bool? Public { get; set; } = false;
    }
}