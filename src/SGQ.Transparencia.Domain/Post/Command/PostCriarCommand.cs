﻿using MediatR;
using SGQ.Core.Domain.Command;
using SGQ.Transparencia.Domain.Post.Entity;

namespace SGQ.Transparencia.Domain.Post.Command
{
    public class PostCriarCommand : IRequest<CommandResult<PostEntity>>
    {
        public string CriadoPor { get; set; }
        public string Titulo { get; set; }
        public string Descricao { get; set; }
        public string Conteudo { get; set; }
        public bool? Publico { get; set; } = false;
    }
}