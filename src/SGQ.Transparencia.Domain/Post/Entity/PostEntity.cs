﻿using SGQ.Core.Domain.Entity;
using SGQ.Core.Domain.Entity.Enum;
using System;
using System.Collections.Generic;
using System.Text;

namespace SGQ.Transparencia.Domain.Post.Entity
{
    public class PostEntity : EntityBase
    {
        public string Titulo { get; set; }

        public string Descricao { get; set; }

        public string Conteudo { get; set; }

        public bool? Publico { get; set; }

        public PostEntity()
        {

        }

        public PostEntity( string titulo, string descricao, string conteudo, bool? publico, string criadoPor ) : base(criadoPor)
        {
            Codigo = GerarNumeroRandomico("POS", 12);

            Titulo = titulo;
            Descricao = descricao;
            Conteudo = conteudo;
            Publico = publico;

            if (string.IsNullOrWhiteSpace(titulo))
                Erros.Add(new Erro("titulo", "Você deve informar o titulo."));

            if (string.IsNullOrWhiteSpace(descricao))
                Erros.Add(new Erro("descricao", "Você deve informar uma descrição."));

            if (string.IsNullOrWhiteSpace(descricao))
                Erros.Add(new Erro("conteudo", "Você deve informar um conteúdo."));

        }

        public void Alterar( string codigo, string titulo, string descricao, string conteudo, bool? publico, string modificadopor, EnumEstadoRegistro estado)
        {
            base.Alterar(codigo, modificadopor, estado);

            Titulo = titulo;
            Descricao = descricao;
            Conteudo = conteudo;
            Publico = publico;


            if (string.IsNullOrWhiteSpace(titulo))
                this.Erros.Add(new Erro("titulo", "Você deve informar o titulo."));

            if (string.IsNullOrWhiteSpace(descricao))
                this.Erros.Add(new Erro("descricao", "Você deve informar uma descrição."));

            if (string.IsNullOrWhiteSpace(conteudo))
                this.Erros.Add(new Erro("conteudo", "Você deve informar o conteúdo."));

            if (string.IsNullOrWhiteSpace(descricao))
                this.Erros.Add(new Erro("processo", "Você deve informar o processo."));
        }

    }
}
