﻿using SGQ.Core.Domain.Query;

namespace SGQ.Transparencia.Domain.Post.Query
{
    public class PostListarQueryFilter : IQueryFilter<PostListarQueryDTO>
    {
        public string codigo { get; set; }
        public string titulo { get; set; }
        public string descricao { get; set; }
        public string conteudo { get; set; }
        public bool? publico { get; set; } = false;
    }
}