﻿using System.Threading.Tasks;
using MediatR;
using SGQ.Core.Infra.Repositories;
using SGQ.Incidentes.Domain.Incidente.Command;
using SGQ.Incidentes.Domain.Incidente.Events;
using SGQ.Incidentes.Domain.Problema.Command;

namespace SGQ.Incidentes.Application.Problema.EventHandler
{
    public class ProblemaEventHandler
    {
        private readonly IMediator _mediator;
        private readonly IUnitOfWork _uow;

        public ProblemaEventHandler(IMediator mediator, IUnitOfWork uow)
        {
            _mediator = mediator;
            _uow = uow;
            _uow.Open();
        }
        public Task Handler(IncidenteCriadoEvent @event)
        {
            var retorno = _mediator.Send(new ProblemaCriarCommand()
            {
                CriadoPor = @event.CriadoPor,
                Titulo = @event.Titulo,
                Descricao = @event.Descricao
            }).GetAwaiter().GetResult();

            _uow.Transaction.Commit();

            return Task.CompletedTask;
        }
    }
}