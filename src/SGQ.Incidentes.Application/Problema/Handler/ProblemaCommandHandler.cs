﻿using MediatR;
using SGQ.Core.Application.Handler;
using SGQ.Core.Domain.Command;
using SGQ.Crosscutting.Identity.Context;
using SGQ.Incidentes.Domain.Problema.Command;
using SGQ.Incidentes.Infra.Repositories;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using SGQ.Core.Domain.Entity;
using SGQ.Incidentes.Domain.Incidente.Entity;
using SGQ.Incidentes.Domain.Problema.Entity;

namespace SGQ.Incidentes.Application.Problema.Handler
{
    public class ProblemaCommandHandler : BaseCommandHandler,
        IRequestHandler<ProblemaCriarCommand, CommandResult<ProblemaEntity>>,
        IRequestHandler<ProblemaAtualizarCommand, CommandResult<ProblemaEntity>>,
        IRequestHandler<ProblemaExcluirCommand, CommandResult<bool>>,
        IRequestHandler<ProblemaResolverCommand, CommandResult<bool>>
    {
        private readonly IProblemaRepository _problemaRepository;
        private readonly IIncidentesRepository _incidentesRepository;

        public ProblemaCommandHandler(IUserService userService, IProblemaRepository problemaRepository, IIncidentesRepository incidentesRepository) : base(userService)
        {
            _problemaRepository = problemaRepository;
            _incidentesRepository = incidentesRepository;
        }

        public async Task<CommandResult<ProblemaEntity>> Handle(ProblemaCriarCommand request, CancellationToken cancellationToken)
        {
            var incidentes = new List<IncidenteEntity>();
            foreach (var codigo in request.Incidentes)
            {
                var incidente = await _incidentesRepository.ObterPorCodigo(codigo);
                if (incidente is null)
                    Erros.Add(new Erro("IncidenteEntity", $"O Codigo informado não existe { codigo }"));
                else
                    incidentes.Add(incidente);
            }

            var problema = new ProblemaEntity( request.Titulo, request.Descricao, incidentes, request.CriadoPor );

            //Verifica se usuario existe.
            UsuarioExiste(request.CriadoPor);

            //Adiciona erros da entidade.
            Erros.AddRange(problema.Erros);

            await _problemaRepository.AdicionarAsync(problema);

            if(Invalido)
                return new CommandResult<ProblemaEntity>()
                {
                    Valido = false,
                    Erros = Erros,
                    Mensagem = "Ocorreu um problema ao criar o registro.",
                    Retorno = default(ProblemaEntity)
                };

            return new CommandResult<ProblemaEntity>()
            {
                Valido = true,
                Erros = Erros,
                Mensagem = "Registro criado com sucesso.",
                Retorno = problema
            };

        }

        public async Task<CommandResult<ProblemaEntity>> Handle(ProblemaAtualizarCommand request, CancellationToken cancellationToken)
        {

            var problema = await _problemaRepository.ObterPorCodigo(request.Codigo);
            if (problema is null)
                Erros.Add(new Erro("ProblemaEntity", $"O ProblemaEntity informado não existe { request.Codigo }"));
            
            if(Invalido)
                return new CommandResult<ProblemaEntity>() { Valido = false, Erros = Erros, Mensagem = "Ocorreu um problema ao alterar o registro.", Retorno = default(ProblemaEntity) };

            var incidentes = new List<IncidenteEntity>();
            foreach (var codigo in request.Incidentes)
            {
                var incidente = await _incidentesRepository.ObterPorCodigo(codigo);

                if (incidente is null)
                    Erros.Add(new Erro("IncidenteEntity", $"O Codigo informado não existe { codigo }"));
                else
                    incidentes.Add(incidente);
            }

            
            problema.Alterar(request.Codigo, request.ModificadoPor, request.EstadoRegistro, request.Titulo, request.Descricao, incidentes);

            //Verifica se usuario existe.
            UsuarioExiste(request.ModificadoPor);

            //Adiciona erros da entidade.
            Erros.AddRange(problema.Erros);

            if (Invalido)
                return new CommandResult<ProblemaEntity>()
                {
                    Valido = false,
                    Erros = Erros,
                    Mensagem = "Ocorreu um problema ao criar o registro.",
                    Retorno = default(ProblemaEntity)
                };

            await _problemaRepository.AtualizarAsync(problema);

            return new CommandResult<ProblemaEntity>()
            {
                Valido = true,
                Erros = Erros,
                Mensagem = "Registro criado com sucesso.",
                Retorno = problema
            };
        }

        public async Task<CommandResult<bool>> Handle(ProblemaExcluirCommand request, CancellationToken cancellationToken)
        {
            var problema = await _problemaRepository.ObterPorCodigo(request.Codigo);

            if (problema is null)
                Erros.Add(new Erro("IncidenteEntity", $"O ProblemaEntity informado não existe { request.Codigo }"));
            
            if (Invalido)
                return new CommandResult<bool>() { Valido = false, Erros = Erros, Mensagem = "Ocorreu um problema ao alterar o registro.", Retorno = false };

            problema.Excluir(request.ModificadoPor);

            //Verifica se usuario existe.
            UsuarioExiste(request.ModificadoPor);

            //Adiciona erros da entidade.
            Erros.AddRange(problema.Erros);

            if (Invalido)
                return new CommandResult<bool>() { Valido = false, Erros = Erros, Mensagem = "Ocorreu um problema ao alterar o registro.", Retorno = false };

            var retorno = await  _problemaRepository.ExcluirAsync(request.Codigo);

            return 
                new CommandResult<bool>() { Valido = true, Erros = Erros, Mensagem = "Registro exclido com sucesso!", Retorno = retorno == 1 };

        }

        public async Task<CommandResult<bool>> Handle(ProblemaResolverCommand request, CancellationToken cancellationToken)
        {
            var problema = await _problemaRepository.ObterPorCodigo(request.Codigo);

            if (problema is null)
                Erros.Add(new Erro("Incidente", $"O ProblemaEntity informado não existe { request.Codigo }"));

            UsuarioExiste(request.ModificadoPor);

            problema?.Resolver(request.Solucao);

            Erros.AddRange(problema?.Erros);

            if (Invalido)
                return new CommandResult<bool>() { Valido = false, Erros = Erros, Mensagem = "Ocorreu um problema ao alterar o registro.", Retorno = false };
       
            var retorno = await _problemaRepository.AtualizarAsync(problema);

            return
                new CommandResult<bool>() { Valido = true, Erros = Erros, Mensagem = "Registro alterado com sucesso!", Retorno = retorno == 1 };
        }
    }
}
