﻿using MediatR;
using SGQ.Core.Application.Handler;
using SGQ.Core.Domain.Command;
using SGQ.Crosscutting.Identity.Context;
using SGQ.Incidentes.Domain.Incidente.Command;
using SGQ.Incidentes.Domain.Incidente.Entity;
using SGQ.Incidentes.Infra.Repositories;
using System.Threading;
using System.Threading.Tasks;
using SGQ.Core.Domain.Entity;

namespace SGQ.Incidentes.Application.Incidente.Handler
{
    public class IncidenteCommandHandler : BaseCommandHandler,
        IRequestHandler<IncidenteCriarCommand, CommandResult<IncidenteEntity>>,
        IRequestHandler<IncidenteAtualizarCommand, CommandResult<IncidenteEntity>>,
        IRequestHandler<IncidenteExcluirCommand, CommandResult<bool>>,
        IRequestHandler<IncidenteFecharCommand, CommandResult<bool>>
    {
        private readonly IMediator _mediator;
        private readonly IIncidentesRepository _incidenteRepository;

       
        public IncidenteCommandHandler(
            IUserService userService,
            IMediator mediator, 
            IIncidentesRepository incidenteRepository): base(userService)
        {
            _mediator = mediator;
            _incidenteRepository = incidenteRepository;
        }

        public async Task<CommandResult<IncidenteEntity>> Handle(IncidenteCriarCommand request, CancellationToken cancellationToken)
        {
            var incidente = new IncidenteEntity(
                request.CriadoPor, 
                request.Titulo,
                request.Descricao);
            
            //Verifica se usuario existe.
            UsuarioExiste( request.CriadoPor );
            
            //Adicionar error tratados na entidade.
            Erros.AddRange( incidente.Erros );

            if (Invalido)
            {
                return new CommandResult<IncidenteEntity>()
                {
                    Valido = this.Valido,
                    Erros = Erros,
                    Mensagem = "Ocorreu um problema ao cria o registro",
                    Retorno = null

                };
            };

            var r = await _incidenteRepository.AdicionarAsync(incidente);

            return new CommandResult<IncidenteEntity>()
            {
                Valido = this.Valido,
                Erros = Erros,
                Mensagem = "Registro criado com sucesso!",
                Retorno = incidente
            };

        }

        public async Task<CommandResult<IncidenteEntity>> Handle(IncidenteAtualizarCommand request, CancellationToken cancellationToken)
        {
            var incidente = await _incidenteRepository.ObterPorCodigo(request.Codigo);

            if (incidente is null)
                return new CommandResult<IncidenteEntity>() { Valido = false, Erros = Erros, Mensagem = $"O incidente de {request.Codigo} não existe.", Retorno = null };


            incidente.Alterar(
                request.Codigo, 
                request.ModificadoPor,
                request.EstadoRegistro,
                request.Titulo, 
                request.Descricao);

            //Verifica se usuario existe.
            UsuarioExiste(request.ModificadoPor);

            //Adicionar error tratados na entidade.
            Erros.AddRange(incidente.Erros);

            if (Invalido)
            {
                return new CommandResult<IncidenteEntity>()
                {
                    Valido = this.Valido,
                    Erros = Erros,
                    Mensagem = "Ocorreu um problema ao cria o registro",
                    Retorno = null
                };
            }

            //Criar usuário
            var r = await _incidenteRepository.AtualizarAsync(incidente);

            return new CommandResult<IncidenteEntity>()
            {
                Valido = this.Valido,
                Erros = Erros,
                Mensagem = "Registro criado com sucesso!",
                Retorno = incidente

            };
        }

        public async Task<CommandResult<bool>> Handle(IncidenteExcluirCommand request, CancellationToken cancellationToken)
        {
            var incidente = await _incidenteRepository.ObterPorCodigo(request.Codigo);
            
            if(incidente is null)
                return new CommandResult<bool>() { Valido = false, Erros = Erros, Mensagem = $"O incidente de {request.Codigo} não existe.", Retorno = false }; 
            
            //Verifica se usuario existe.
            UsuarioExiste(request.ModificadoPor);

            //Adicionar error tratados na entidade.
            Erros.AddRange(incidente.Erros);

            if (Invalido)
            {
                return new CommandResult<bool>()
                {
                    Valido = this.Valido,
                    Erros = Erros,
                    Mensagem = "Ocorreu um problema ao excluir o registro",
                    Retorno = false
                };
            }

            //Excluir incidente
            await _incidenteRepository.ExcluirAsync(incidente.Codigo);

            return new CommandResult<bool>()
            {
                Valido = this.Valido,
                Erros = Erros,
                Mensagem = "Registro excluido com sucesso!",
                Retorno = true
            };
        }

        public async Task<CommandResult<bool>> Handle(IncidenteFecharCommand request, CancellationToken cancellationToken)
        {
            var incidente = await _incidenteRepository.ObterPorCodigo(request.Codigo);
            
            incidente.Fechar(request.Solucao);

            //Verifica se usuario existe.
            UsuarioExiste(request.ModificadoPor);

            //Adicionar error tratados na entidade.
            Erros.AddRange(incidente.Erros);

            if (Invalido)
            {
                return new CommandResult<bool>()
                {
                    Valido = this.Valido,
                    Erros = Erros,
                    Mensagem = "Ocorreu um problema ao fechar o incidente",
                    Retorno = false
                };
            }

            //Criar usuário
            var r = await _incidenteRepository.AtualizarAsync(incidente);

            return new CommandResult<bool>()
            {
                Valido = this.Valido,
                Erros = Erros,
                Mensagem = "Registro fechado com sucesso!",
                Retorno = true

            };
        }
    }
}
