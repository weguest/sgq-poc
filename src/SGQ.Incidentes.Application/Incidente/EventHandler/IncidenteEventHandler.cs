﻿using System.Threading.Tasks;
using MediatR;
using SGQ.Core.Domain.Event;
using SGQ.Core.Infra.Repositories;
using SGQ.Incidentes.Domain.Incidente.Command;
using SGQ.Incidentes.Domain.Incidente.Events;
using SGQ.Incidentes.Domain.NC.Command;

namespace SGQ.Incidentes.Application.Incidente.EventHandler
{
    public class IncidenteEventHandler   : IEventHandler<IncidenteCriadoEvent>
    {

        private readonly IMediator _mediator;
        private readonly IUnitOfWork _uow;

        public IncidenteEventHandler(IMediator mediator, IUnitOfWork uow)
        {
            _mediator = mediator;
            _uow = uow;
            _uow.Open();
        }
        public Task Handler(IncidenteCriadoEvent @event)
        {
            var retorno = _mediator.Send(new IncidenteCriarCommand()
            {
                CriadoPor = @event.CriadoPor,
                Titulo = @event.Titulo,
                Descricao = @event.Descricao
            }).GetAwaiter().GetResult();

            _uow.Transaction.Commit();

            return Task.CompletedTask;
        }

    }
}
