﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using SGQ.Core.Application.Handler;
using SGQ.Core.Domain.Command;
using SGQ.Core.Domain.Entity;
using SGQ.Core.Domain.Event;
using SGQ.Crosscutting.Identity.Context;
using SGQ.Incidentes.Domain.NaoConformidade.Command;
using SGQ.Incidentes.Domain.NaoConformidade.Entity;
using SGQ.Incidentes.Domain.NaoConformidade.Events;
using SGQ.Incidentes.Domain.NC.Command;
using SGQ.Incidentes.Infra.Repositories;

namespace SGQ.Incidentes.Application.NaoConformidade.Handler
{
    public class NaoConformidadeCommandHandler : BaseCommandHandler,
        IRequestHandler<NaoConformidadeCriarCommand, CommandResult<NaoConformidadeEntity>>,
        IRequestHandler<NaoConformidadeAtualizarCommand, CommandResult<NaoConformidadeEntity>>,
        IRequestHandler<NaoConformidadeExcluirCommand, CommandResult<bool>>,
        IRequestHandler<NaoConformidadeFecharCommand, CommandResult<NaoConformidadeEntity>>,
        IRequestHandler<NaoConformidadeCriarDeRespostaCommand, CommandResult<NaoConformidadeEntity>>
    {
        private readonly IEventBus _bus;
        private readonly INaoConformidadeRepository _naoConformidadeRepository;

        public NaoConformidadeCommandHandler(
            IUserService userService,
            IEventBus bus,
            INaoConformidadeRepository naoConformidadeRepository) : base(userService)
        {
            _bus = bus;
            _naoConformidadeRepository = naoConformidadeRepository;
        }

        public async Task<CommandResult<NaoConformidadeEntity>> Handle(NaoConformidadeCriarCommand request, CancellationToken cancellationToken)
        {
            var nc = new NaoConformidadeEntity(
                request.Titulo, 
                request.Descricao, 
                request.Norma, 
                request.Processo,
                request.CriadoPor);

            //Verifica se usuario existe.
            UsuarioExiste(request.CriadoPor);

            //Adiciona erros da entidade.
            Erros.AddRange(nc.Erros);

            if(Invalido)
                return CommandResult<NaoConformidadeEntity>.Erro(Erros, "Houve um problema ao criar NC.");

            await _naoConformidadeRepository.AdicionarAsync(nc);

            //Publica na fila.
            _bus.Publish( new NaoConformidadeCriadaEvent( 
                codigo: nc.Codigo, 
                norma: nc.Norma, 
                processo: nc.Processo, 
                titulo:nc.Titulo, 
                descricao: nc.Descricao, 
                criadopor: request.CriadoPor));

            return new CommandResult<NaoConformidadeEntity>()
            {
                Valido = true,
                Erros = new List<Erro>(),
                Mensagem = "NC cadastrada com sucesso",
                Retorno =  nc
            };
            
        }

        public async Task<CommandResult<NaoConformidadeEntity>> Handle(NaoConformidadeAtualizarCommand request, CancellationToken cancellationToken)
        {

            var nc = await _naoConformidadeRepository.ObterPorCodigo(request.Codigo);

            if(nc is null)
                return CommandResult<NaoConformidadeEntity>.Erro(Erros, $"Nc de codigo {request.Codigo} não existe.");

            nc.Alterar(
                request.Codigo,
                request.Titulo,
                request.Descricao,
                request.Norma,
                request.Processo,
                request.ModificadoPor,
                request.EstadoRegistro
            );

            //Verifica se usuario existe.
            UsuarioExiste(request.ModificadoPor);

            //Adiciona erros da entidade.
            Erros.AddRange(nc.Erros);


            if (Invalido)
                return CommandResult<NaoConformidadeEntity>.Erro(Erros, "Houve um problema ao atualizar NC.");

            await _naoConformidadeRepository.AtualizarAsync(nc);

            return new CommandResult<NaoConformidadeEntity>()
            {
                Valido = true,
                Erros = new List<Erro>(),
                Mensagem = "NC cadastrada com sucesso",
                Retorno = nc
            };
        }

        public async Task<CommandResult<bool>> Handle(NaoConformidadeExcluirCommand request, CancellationToken cancellationToken)
        {
            var nc = await _naoConformidadeRepository.ObterPorCodigo(request.Codigo);

            if (nc is null)
                return CommandResult<bool>.Erro(Erros, $"Nc de codigo {request.Codigo} não existe.");

            await _naoConformidadeRepository.ExcluirAsync(request.Codigo);

            if (Invalido)
                return CommandResult<bool>.Erro(Erros, "Houve um problema ao atualizar NC.");

            return new CommandResult<bool>()
            {
                Valido = true,
                Erros = new List<Erro>(),
                Mensagem = "NC excluida com sucesso",
                Retorno = true
            };
        }

        public async Task<CommandResult<NaoConformidadeEntity>> Handle(NaoConformidadeCriarDeRespostaCommand request, CancellationToken cancellationToken)
        {
            //Só cria a NC se a resposta nao bater com o esperado.
            if (request.Resposta == request.Resposta_Esperada) return null;

            var nc = new NaoConformidadeEntity(
                $"NC ({request.Organismo}) {request.Codigo} / {request.Processo} / {DateTime.Now.Year}-{DateTime.Now.Month}-{DateTime.Now.Day}",
                $"Pergunta: {request.Pergunta} / Resposta:{request.Resposta} / Grupo:{request.Grupo} / Turno:{request.Turno}",
                request.Norma,
                request.Processo,
                request.ModificadoPor);

            //Verifica se usuario existe.
            UsuarioExiste(request.ModificadoPor);

            //Adiciona erros da entidade.
            Erros.AddRange(nc.Erros);

            if (Invalido)
                return CommandResult<NaoConformidadeEntity>.Erro(Erros, "Houve um problema ao criar NC.");

            await _naoConformidadeRepository.AdicionarAsync(nc);

            return new CommandResult<NaoConformidadeEntity>()
            {
                Valido = true,
                Erros = new List<Erro>(),
                Mensagem = "NC cadastrada com sucesso",
                Retorno = nc
            };
        }

        public async Task<CommandResult<NaoConformidadeEntity>> Handle(NaoConformidadeFecharCommand request, CancellationToken cancellationToken)
        {
            var nc = await _naoConformidadeRepository.ObterPorCodigo(request.Codigo);

            if (nc is null)
                return CommandResult<NaoConformidadeEntity>.Erro(Erros, $"Nc de codigo {request.Codigo} não existe.");

            nc.Fechar( request.ModificadoPor, request.AcoesCorretivas, request.AcoesPreventivas );

            //Verifica se usuario existe.
            UsuarioExiste(request.ModificadoPor);

            //Adiciona erros da entidade.
            Erros.AddRange(nc.Erros);


            if (Invalido)
                return CommandResult<NaoConformidadeEntity>.Erro(Erros, "Houve um problema ao atualizar NC.");

            await _naoConformidadeRepository.AtualizarAsync(nc);

            return new CommandResult<NaoConformidadeEntity>()
            {
                Valido = true,
                Erros = new List<Erro>(),
                Mensagem = "NC cadastrada com sucesso",
                Retorno = nc
            };
        }
    }
}
