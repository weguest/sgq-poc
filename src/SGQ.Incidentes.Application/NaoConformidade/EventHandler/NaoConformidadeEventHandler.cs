﻿using System.Threading.Tasks;
using MediatR;
using SGQ.Core.Domain.Event;
using SGQ.Core.Infra.Repositories;
using SGQ.Incidentes.Domain.NaoConformidade.Command;
using SGQ.Incidentes.Domain.NaoConformidade.Events;
using SGQ.Incidentes.Domain.NC.Command;

namespace SGQ.Incidentes.Application.NaoConformidade.EventHandler
{
    public class NaoConformidadeEventHandler : IEventHandler<RespostaChecklistRespondidaEvent>
    {
        private readonly IMediator _mediator;
        private readonly IUnitOfWork _uow;

        public NaoConformidadeEventHandler(IMediator mediator, IUnitOfWork uow)
        {
            _mediator = mediator;
            _uow = uow;
            
        }
        public Task Handler(RespostaChecklistRespondidaEvent @event)
        {
            _uow.Open();

            var command = new NaoConformidadeCriarDeRespostaCommand()
            {
                Codigo = @event.Codigo,
                Titulo = @event.Titulo,
                Descricao = @event.Descricao,
                Grupo = @event.Grupo,
                Turno = @event.Turno,
                ModificadoPor = @event.ModificadoPor,
                Norma = @event.Norma,
                Organismo = @event.Organismo,
                Processo = @event.Processo,
                Pergunta = @event.Pergunta,
                Resposta = @event.Resposta,
                Resposta_Esperada = @event.Resposta_Esperada
            };

            var retorno = _mediator.Send(command).GetAwaiter().GetResult();

            _uow.Transaction.Commit();
            
            return Task.CompletedTask;
        }
    }
}
